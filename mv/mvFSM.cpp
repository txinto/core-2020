/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "mvFSM.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['VentilationCycle' begin (DON'T REMOVE THIS LINE!)] */
/** ***************************************************
This state machine simply models the insufflation-exsufflation loop.
If a stop request is commanded, the current cycle is finished, before finishing (going to final state).
*************************************************** */
/* set initial state */
STATE_T VentilationCyclestate = ID_VENTILATIONCYCLE_INITIAL;
STATE_T VentilationCycle(  )
{

    switch( VentilationCyclestate )
    {
        /* State ID: ID_VENTILATIONCYCLE_INITIAL */
        case ID_VENTILATIONCYCLE_INITIAL:
        {
            if( edre.mvStop == true )
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::vmDisable' begin] */
                edre.vmEnable = false;
                /* ['<global>::vmDisable' end] */
                /* ['<global>::mdStopReq' begin] */
                edre.mdStop = true;
                edre.mdEnable = false;
                edre.mdHoming = false;
                /* ['<global>::mdStopReq' end] */
                VentilationCyclestate = ID_VENTILATIONCYCLE_STOPPED;
            }
            else
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::vmExsufflationReq' begin] */
                edre.vmMode = edre.mvExsufflationMode;
                edre.vmEnable = true;
                /* ['<global>::vmExsufflationReq' end] */
                VentilationCyclestate = ID_VENTILATIONCYCLE_EXSUFFLATION;
            }
            return ID_VENTILATIONCYCLE_INITIAL;
        }
        /* State ID: ID_VENTILATIONCYCLE_EXSUFFLATION */
        case ID_VENTILATIONCYCLE_EXSUFFLATION:
        {
            if( edre.mvStop == true )
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::vmDisable' begin] */
                edre.vmEnable = false;
                /* ['<global>::vmDisable' end] */
                /* ['<global>::mdStopReq' begin] */
                edre.mdStop = true;
                edre.mdEnable = false;
                edre.mdHoming = false;
                /* ['<global>::mdStopReq' end] */
                VentilationCyclestate = ID_VENTILATIONCYCLE_STOPPED;
            }
            else if( edre.vmActionFinished )
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                VentilationCyclestate = ID_VENTILATIONCYCLE_RECRUITACTIVE_;
            }
            return ID_VENTILATIONCYCLE_EXSUFFLATION;
        }
        /* State ID: ID_VENTILATIONCYCLE_STOPPED */
        case ID_VENTILATIONCYCLE_STOPPED:
        {
            VentilationCyclestate = ID_VENTILATIONCYCLE_INITIAL;
            return ID_VENTILATIONCYCLE_STOPPED;
        }
        /* State ID: ID_VENTILATIONCYCLE_RECRUITACTIVE_ */
        case ID_VENTILATIONCYCLE_RECRUITACTIVE_:
        {
            if( edre.mvRecruit )
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::resetRecruitTimer' begin] */
                edre.RecruitmentElap = 0L;
                /* ['<global>::resetRecruitTimer' end] */
                /* ['<global>::vmRecruitReq' begin] */
                edre.vmMode = edre.mvRecruitMode;
                edre.vmEnable = true;
                /* ['<global>::vmRecruitReq' end] */
                VentilationCyclestate = ID_VENTILATIONCYCLE_RECRUITING;
            }
            else
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::vmInsufflationReq' begin] */
                edre.vmMode = edre.mvInsufflationMode;
                edre.vmEnable = true;
                /* ['<global>::vmInsufflationReq' end] */
                VentilationCyclestate = ID_VENTILATIONCYCLE_INSUFFLATION;
            }
            return ID_VENTILATIONCYCLE_RECRUITACTIVE_;
        }
        /* State ID: ID_VENTILATIONCYCLE_RECRUITING */
        case ID_VENTILATIONCYCLE_RECRUITING:
        {
            if( edre.RecruitmentElap > edre.RecruitmentTimer )
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                VentilationCyclestate = ID_VENTILATIONCYCLE_EXSUFFLATION;
            }
            else if( edre.mvStop == true )
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::vmDisable' begin] */
                edre.vmEnable = false;
                /* ['<global>::vmDisable' end] */
                /* ['<global>::mdStopReq' begin] */
                edre.mdStop = true;
                edre.mdEnable = false;
                edre.mdHoming = false;
                /* ['<global>::mdStopReq' end] */
                VentilationCyclestate = ID_VENTILATIONCYCLE_STOPPED;
            }
            else
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::incrRecruitTimer' begin] */
                edre.RecruitmentElap++;
                /* ['<global>::incrRecruitTimer' end] */
            }
            return ID_VENTILATIONCYCLE_RECRUITING;
        }
        /* State ID: ID_VENTILATIONCYCLE_INSUFFLATION */
        case ID_VENTILATIONCYCLE_INSUFFLATION:
        {
            if( edre.mvStop == true )
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::vmDisable' begin] */
                edre.vmEnable = false;
                /* ['<global>::vmDisable' end] */
                /* ['<global>::mdStopReq' begin] */
                edre.mdStop = true;
                edre.mdEnable = false;
                edre.mdHoming = false;
                /* ['<global>::mdStopReq' end] */
                VentilationCyclestate = ID_VENTILATIONCYCLE_STOPPED;
            }
            else if( (edre.mvEnable == false) && (edre.vmActionFinished) )
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::vmDisable' begin] */
                edre.vmEnable = false;
                /* ['<global>::vmDisable' end] */
                VentilationCyclestate = ID_VENTILATIONCYCLE_DISABLED;
            }
            else if( edre.vmActionFinished )
            {
                /* Transition ID: ID_VENTILATIONCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::vmExsufflationReq' begin] */
                edre.vmMode = edre.mvExsufflationMode;
                edre.vmEnable = true;
                /* ['<global>::vmExsufflationReq' end] */
                VentilationCyclestate = ID_VENTILATIONCYCLE_EXSUFFLATION;
            }
            return ID_VENTILATIONCYCLE_INSUFFLATION;
        }
        /* State ID: ID_VENTILATIONCYCLE_DISABLED */
        case ID_VENTILATIONCYCLE_DISABLED:
        {
            VentilationCyclestate = ID_VENTILATIONCYCLE_INITIAL;
            return ID_VENTILATIONCYCLE_DISABLED;
        }
    }
}
/* ['VentilationCycle' end (DON'T REMOVE THIS LINE!)] */

/* ['VentilationSession' begin (DON'T REMOVE THIS LINE!)] */
/** ***************************************************
VentilationSession models the normal working cycle of the ventilator.  At the beginning of each session (when the machine is connected to a new patient, or when the machine has been stopped for any reason) the machine shall execute a homing operation for reference, and just after the homing, it shall begin the ventilation cycle loop.  The ventilation cycle loop is modeled in a sub-machine called VentilationCycle.  If an stop is requested, this machine will wait for VentilationCycle to end, and then will return the control to the parent machine (MechVentilation).
*************************************************** */
/* set initial state */
STATE_T VentilationSessionstate = ID_VENTILATIONSESSION_INITIAL;
STATE_T VentilationSession(  )
{

    switch( VentilationSessionstate )
    {
        /* State ID: ID_VENTILATIONSESSION_INITIAL */
        case ID_VENTILATIONSESSION_INITIAL:
        {
            /* Transition ID: ID_VENTILATIONSESSION_TRANSITION_CONNECTION */
            /* Actions: */
            /* ['<global>::mdHomingReq' begin] */
            edre.mdHoming = true;
            edre.mdEnable = true;
            /* ['<global>::mdHomingReq' end] */
            VentilationSessionstate = ID_VENTILATIONSESSION_HOMING;
            return ID_VENTILATIONSESSION_INITIAL;
        }
        /* State ID: ID_VENTILATIONSESSION_HOMING */
        case ID_VENTILATIONSESSION_HOMING:
        {
            if( edre.mdHomingAchieved )
            {
                /* Transition ID: ID_VENTILATIONSESSION_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::mdDisable' begin] */
                edre.mdHoming = false;
                edre.mdEnable = false;
                /* ['<global>::mdDisable' end] */
                VentilationSessionstate = ID_VENTILATIONSESSION_VENTILATIONCYCLE;
            }
            else if( edre.mvStop == true )
            {
                /* Transition ID: ID_VENTILATIONSESSION_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::mdStopReq' begin] */
                edre.mdStop = true;
                edre.mdEnable = false;
                edre.mdHoming = false;
                /* ['<global>::mdStopReq' end] */
                VentilationSessionstate = ID_VENTILATIONSESSION_STOPPED;
            }
            return ID_VENTILATIONSESSION_HOMING;
        }
        /* State ID: ID_VENTILATIONSESSION_VENTILATIONCYCLE */
        case ID_VENTILATIONSESSION_VENTILATIONCYCLE:
        {
            /* call substate function */
            STATE_T ventilationcycle_retval = VentilationCycle(  );
            if( ventilationcycle_retval < 0 )
            {
                /* Transition ID: ID_VENTILATIONSESSION_TRANSITION_CONNECTION */
                VentilationSessionstate = ID_VENTILATIONSESSION_SESSIONEND;
            }
            return ID_VENTILATIONSESSION_VENTILATIONCYCLE;
        }
        /* State ID: ID_VENTILATIONSESSION_SESSIONEND */
        case ID_VENTILATIONSESSION_SESSIONEND:
        {
            VentilationSessionstate = ID_VENTILATIONSESSION_INITIAL;
            return ID_VENTILATIONSESSION_SESSIONEND;
        }
        /* State ID: ID_VENTILATIONSESSION_STOPPED */
        case ID_VENTILATIONSESSION_STOPPED:
        {
            VentilationSessionstate = ID_VENTILATIONSESSION_INITIAL;
            return ID_VENTILATIONSESSION_STOPPED;
        }
    }
}
/* ['VentilationSession' end (DON'T REMOVE THIS LINE!)] */

/* ['MechVentilation' begin (DON'T REMOVE THIS LINE!)] */
/** ***************************************************
Mechanical ventilation state machine.  Includes the VentilationSession and VentilationCycle.  Includes a Stop and a Ready state for responding to failures or finish the VentilationSession.
*************************************************** */
/* set initial state */
STATE_T MechVentilationstate = ID_MECHVENTILATION_INITIAL;
STATE_T MechVentilation(  )
{

    switch( MechVentilationstate )
    {
        /* State ID: ID_MECHVENTILATION_INITIAL */
        case ID_MECHVENTILATION_INITIAL:
        {
            /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
            MechVentilationstate = ID_MECHVENTILATION_MVSTOPPRESENT;
            return ID_MECHVENTILATION_INITIAL;
        }
        /* State ID: ID_MECHVENTILATION_MVSTOPPRESENT */
        case ID_MECHVENTILATION_MVSTOPPRESENT:
        {
            if( edre.mvStop == true )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::mdStopReq' begin] */
                edre.mdStop = true;
                edre.mdEnable = false;
                edre.mdHoming = false;
                /* ['<global>::mdStopReq' end] */
                MechVentilationstate = ID_MECHVENTILATION_STOP;
            }
            else
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                MechVentilationstate = ID_MECHVENTILATION_READY;
            }
            return ID_MECHVENTILATION_MVSTOPPRESENT;
        }
        /* State ID: ID_MECHVENTILATION_READY */
        case ID_MECHVENTILATION_READY:
        {
            if( edre.mvStop == true )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::mdStopReq' begin] */
                edre.mdStop = true;
                edre.mdEnable = false;
                edre.mdHoming = false;
                /* ['<global>::mdStopReq' end] */
                MechVentilationstate = ID_MECHVENTILATION_STOP;
            }
            else if( edre.mvEnable )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                MechVentilationstate = ID_MECHVENTILATION_VENTILATIONSESSION;
            }
            else if( false )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                MechVentilationstate = ID_MECHVENTILATION_UNREACHABLEFINAL;
            }
            return ID_MECHVENTILATION_READY;
        }
        /* State ID: ID_MECHVENTILATION_VENTILATIONSESSION */
        case ID_MECHVENTILATION_VENTILATIONSESSION:
        {
            /* call substate function */
            STATE_T ventilationsession_retval = VentilationSession(  );
            if( (ventilationsession_retval < 0) )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                MechVentilationstate = ID_MECHVENTILATION_MVSTOPPRESENT;
            }
            return ID_MECHVENTILATION_VENTILATIONSESSION;
        }
        /* State ID: ID_MECHVENTILATION_STOP */
        case ID_MECHVENTILATION_STOP:
        {
            if( (!edre.mvStop && edre.mvRehab) )
            {
                /* Transition ID: ID_MECHVENTILATION_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::mdRehabReq' begin] */
                edre.mdStop = false;
                edre.mdRehab = false;
                /* ['<global>::mdRehabReq' end] */
                MechVentilationstate = ID_MECHVENTILATION_READY;
            }
            return ID_MECHVENTILATION_STOP;
        }
        /* State ID: ID_MECHVENTILATION_UNREACHABLEFINAL */
        case ID_MECHVENTILATION_UNREACHABLEFINAL:
        {
            MechVentilationstate = ID_MECHVENTILATION_INITIAL;
            return ID_MECHVENTILATION_UNREACHABLEFINAL;
        }
    }
}
/* ['MechVentilation' end (DON'T REMOVE THIS LINE!)] */
