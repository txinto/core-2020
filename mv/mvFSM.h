/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "mvFSM_CI.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['VentilationCycle' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_VENTILATIONCYCLE_INITIAL 0
#define ID_VENTILATIONCYCLE_RECRUITING 1
#define ID_VENTILATIONCYCLE_INSUFFLATION 2
#define ID_VENTILATIONCYCLE_EXSUFFLATION 3
#define ID_VENTILATIONCYCLE_RECRUITACTIVE_ 4
#define ID_VENTILATIONCYCLE_STOPPED -5
#define ID_VENTILATIONCYCLE_DISABLED -6

STATE_T VentilationCycle(  );
/* ['VentilationCycle' end (DON'T REMOVE THIS LINE!)] */

/* ['VentilationSession' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_VENTILATIONSESSION_INITIAL 7
#define ID_VENTILATIONSESSION_HOMING 8
#define ID_VENTILATIONSESSION_VENTILATIONCYCLE 9
#define ID_VENTILATIONSESSION_SESSIONEND -10
#define ID_VENTILATIONSESSION_STOPPED -11

STATE_T VentilationSession(  );
/* ['VentilationSession' end (DON'T REMOVE THIS LINE!)] */

/* ['MechVentilation' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_MECHVENTILATION_INITIAL 12
#define ID_MECHVENTILATION_READY 13
#define ID_MECHVENTILATION_STOP 14
#define ID_MECHVENTILATION_VENTILATIONSESSION 15
#define ID_MECHVENTILATION_MVSTOPPRESENT 16
#define ID_MECHVENTILATION_UNREACHABLEFINAL -17

STATE_T MechVentilation(  );
/* ['MechVentilation' end (DON'T REMOVE THIS LINE!)] */
