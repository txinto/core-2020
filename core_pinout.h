/* ---- This file describes all the pin assignments of the microcontroller --- */

#ifndef _CORE_PINOUT_H
#define _CORE_PINOUT_H

#include <Arduino.h>

#define CFG_PWM_RESOLUTION (10)
#define PIN_PeepValve_FREQ 100

#ifdef ARDUINO_ESP8266_WEMOS_D1MINI
//#error "TODO: Definir pines para el ESP8266_WEMOS_D1MINI"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_ESP8266_ESP01
#error "TODO: Definir pines para el ARDUINO_ESP8266_ESP01"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_ESP8266_NODEMCU
#error "TODO: Definir pines para el ARDUINO_ESP8266_NODEMCU"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_ESP32_DEV
#error "TODO: Definir pines para la ARDUINO_AVR_NANO"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_TEENSY31
//#error "TODO: Definir pines para la TEENSY31"
#define PIN_statusLED LED_BUILTIN
#define PIN_ResetRelay 0
#define PIN_ServoPend 1
#define PIN_TurnAxisHwHi 2
#define PIN_ServoAlarm 3
#define PIN_BeaconLightRed 4
#define PIN_BeaconLightOrange 5
#define PIN_TurnAxisPulse 6
#define PIN_TurnAxisDir 7
#define PIN_TurnAxisEnable 8
#define PIN_BeaconLightGreen 9
#define PIN_EmergencyStop 10
#define PIN_Buzzer 11
#define PIN_TurnAxisHwLow 12
#define PIN_PeepValve 14
#define PIN_AtmosfericPressure A0
#define PIN_DiferentialPressure A1
#define PIN_LCD_DB7 -1
#define PIN_LCD_DB6 -1
#define PIN_LCD_DB5 -1
#define PIN_LCD_DB4 -1
#define PIN_LCD_DB3 -1
#define PIN_LCD_DB2 -1
#define PIN_LCD_DB1 -1
#define PIN_LCD_DB0 -1
#elif ARDUINO_TEENSY35
//#error "TODO: Definir pines para la TEENSY35"
#define PIN_statusLED LED_BUILTIN
#define PIN_ResetRelay 0
#define PIN_ServoPend 1
#define PIN_TurnAxisHwHi 2
#define PIN_ServoAlarm 3
#define PIN_BeaconLightRed 4
#define PIN_BeaconLightOrange 5
#define PIN_TurnAxisPulse 6
#define PIN_TurnAxisDir 7
#define PIN_TurnAxisEnable 8
#define PIN_BeaconLightGreen 9
#define PIN_EmergencyStop 10
#define PIN_Buzzer 11
#define PIN_TurnAxisHwLow 12
#define PIN_PeepValve 16
#define PIN_AtmosfericPressure A0
#define PIN_DiferentialPressure A1
#define PIN_LCD_DB7 17
#define PIN_LCD_DB6 18
#define PIN_LCD_DB5 19
#define PIN_LCD_DB4 20
#define PIN_LCD_DB3 21
#define PIN_LCD_DB2 22
#define PIN_LCD_DB1 23
#define PIN_LCD_DB0 24

#elif ARDUINO_AVR_NANO
#error "TODO: Definir pines para la ARDUINO_AVR_NANO"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_AVR_MEGA2560
//#error "TODO: Definir pines para la ARDUINO_AVR_MEGA2560"
#define PIN_statusLED LED_BUILTIN
#define PIN_TurnAxisHwHi 2
#define PIN_ServoAlarm 3
#define PIN_BeaconLightRed 4
#define PIN_BeaconLightOrange 5
#define PIN_TurnAxisPulse 6
#define PIN_TurnAxisDir 7
#define PIN_TurnAxisEnable 8
#define PIN_BeaconLightGreen 9
#define PIN_EmergencyStop 10
#define PIN_Buzzer 11
#define PIN_TurnAxisHwLow 13
#define PIN_ServoPend 14
#define PIN_ResetRelay 25
#define PIN_PeepValve 39
#define PIN_AtmosfericPressure A0
#define PIN_DiferentialPressure A1
#define PIN_LCD_DB7 30
#define PIN_LCD_DB6 31
#define PIN_LCD_DB5 32
#define PIN_LCD_DB4 33
#define PIN_LCD_DB3 34
#define PIN_LCD_DB2 35
#define PIN_LCD_DB1 36
#define PIN_LCD_DB0 37
#elif ARDUINO_AVR_UNO
//#error "TODO: Definir pines para la ARDUINO_AVR_MEGA2560"
#define PIN_statusLED LED_BUILTIN
#define PIN_ResetRelay 0
#define PIN_ServoPend 1
#define PIN_TurnAxisHwHi 2
#define PIN_ServoAlarm 3
#define PIN_BeaconLightRed 4
#define PIN_BeaconLightOrange 5
#define PIN_TurnAxisPulse 6
#define PIN_TurnAxisDir 7
#define PIN_TurnAxisEnable 8
#define PIN_BeaconLightGreen 9
#define PIN_EmergencyStop 10
#define PIN_Buzzer 11
#define PIN_TurnAxisHwLow 12
#define PIN_PeepValve 14
#define PIN_AtmosfericPressure A0
#define PIN_DiferentialPressure A1
#define PIN_LCD_DB7 -1
#define PIN_LCD_DB6 -1
#define PIN_LCD_DB5 -1
#define PIN_LCD_DB4 -1
#define PIN_LCD_DB3 -1
#define PIN_LCD_DB2 -1
#define PIN_LCD_DB1 -1
#define PIN_LCD_DB0 -1
#else
#error "No microcontroller defined"
#endif

#endif // _PRJ_PINOUT_H