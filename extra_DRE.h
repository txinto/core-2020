#include "core_types.h"

typedef enum {
    VM_MODE_IDLE,
    VM_MODE_EXSUFFLATION,
    VM_MODE_INSUFFLATION,
    VM_MODE_RECRUIT
} t_vmMode;

typedef enum {
    MC_MODE_IDLE,
    MC_MODE_PRESSURE_CTRL,
    MC_MODE_VOLUME_CTRL
} t_mcMode;

typedef enum {
    MD_MODE_IDLE,
    MD_MODE_SPEED_ABSOLUTE,
    MD_MODE_SPEED_RELATIVE,
    MD_MODE_POS_ABSOLUTE,
    MD_MODE_POS_RELATIVE
} t_mdMode;

typedef struct {
    /**************** MV: MechVentilation ***************/
    /* Contains VentilationSession(vs) and VentilationCycle(vc) */
    /* MV Parameters */

    uint32_t RecruitmentTimer;
    /* MV Commands */
    bool mvStop;
    bool mvEnable;
    bool mvRehab;
    bool mvRecruit;
    /* MV Responses */

    /* Internal variables */
    uint32_t RecruitmentElap;


    /*********** VC: VentilationManager ***********/
    /* VM Parameters */
    uint16_t PeakSetpoint;
    uint16_t PeepSetpoint;
    uint16_t RecruitmentSetpoint;
    t_vmMode mvExsufflationMode;
    t_vmMode mvInsufflationMode;
    t_vmMode mvRecruitMode;
    /* VM Commands */
    t_vmMode vmMode;
    bool vmStop;
    bool vmEnable;
    bool vmRehab;
     /* MV Responses */   
    bool vmActionFinished;

    /*********** MC: MotorControl ***********/
    /* MC Parameters */
    uint16_t mcSetpoint;
    t_mcMode mcMode;
    /* MC Commands */
    bool mcStop;
    bool mcEnable;
    bool mcRehab;
     /* MC Responses */   
    bool mcSetPointAchieved;

    /*********** MC: MotorDriving ***********/
    /* MC Parameters */
    uint16_t mdSetpoint;
    t_mdMode mdMode;
    /* MC Commands */
    bool mdStop;
    bool mdEnable;
    bool mdRehab;
    bool mdHoming;
     /* MV Responses */   
    bool mdSetPointAchieved;
    bool mdHomingAchieved;

#include "s1/S1DRE.h"
#include "vmg/VentilationManagerDRE.h"

}t_extra_DRE;

