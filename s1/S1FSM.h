/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "S1FSM_CI.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['Cycle' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_CYCLE_INITIAL 0
#define ID_CYCLE_STEP1 1
#define ID_CYCLE_STEP2 2
#define ID_CYCLE_DISABLED -3
#define ID_CYCLE_STOPPED -4

STATE_T Cycle(  );
/* ['Cycle' end (DON'T REMOVE THIS LINE!)] */

/* ['DegradedCycle' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_DEGRADEDCYCLE_INITIAL 5
#define ID_DEGRADEDCYCLE_DEGSTEP1 6
#define ID_DEGRADEDCYCLE_DEGSTEP2 7
#define ID_DEGRADEDCYCLE_DISABLED -8
#define ID_DEGRADEDCYCLE_STOPPED -9

STATE_T DegradedCycle(  );
/* ['DegradedCycle' end (DON'T REMOVE THIS LINE!)] */

/* ['Run' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_RUN_INITIAL 10
#define ID_RUN_SETUP 11
#define ID_RUN_DEGRADEDSETUP 12
#define ID_RUN_DEGRADEDCYCLE 13
#define ID_RUN_CYCLE 14
#define ID_RUN_MODECHOICE 15
#define ID_RUN_STOPPRESENT 16
#define ID_RUN_DISABLED -17
#define ID_RUN_STOPPED -18

STATE_T Run(  );
/* ['Run' end (DON'T REMOVE THIS LINE!)] */

/* ['S1' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_S1_INITIAL 19
#define ID_S1_READY 20
#define ID_S1_STOP 21
#define ID_S1_RUN 22
#define ID_S1_S1STOPPRESENT 23
#define ID_S1_UNREACHABLEFINAL -24

STATE_T S1(  );
/* ['S1' end (DON'T REMOVE THIS LINE!)] */
