/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "S1FSM.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['Cycle' begin (DON'T REMOVE THIS LINE!)] */
/** ***************************************************
This state machine simply models the insufflation-exsufflation loop.
If a stop request is commanded, the current cycle is finished, before finishing (going to final state).
*************************************************** */
/* set initial state */
STATE_T Cyclestate = ID_CYCLE_INITIAL;
STATE_T Cycle(  )
{

    switch( Cyclestate )
    {
        /* State ID: ID_CYCLE_INITIAL */
        case ID_CYCLE_INITIAL:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doCycleStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Cycle.
                // ..
                /* ['<global>::doCycleStop' end] */
                Cyclestate = ID_CYCLE_STOPPED;
            }
            else
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doStep1' begin] */
                edre.s1Step1Done = false;
                // Acciones que lanzan Step1
                // ...
                /* ['<global>::doStep1' end] */
                Cyclestate = ID_CYCLE_STEP1;
            }
            return ID_CYCLE_INITIAL;
        }
        /* State ID: ID_CYCLE_STOPPED */
        case ID_CYCLE_STOPPED:
        {
            Cyclestate = ID_CYCLE_INITIAL;
            return ID_CYCLE_STOPPED;
        }
        /* State ID: ID_CYCLE_STEP1 */
        case ID_CYCLE_STEP1:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doCycleStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Cycle.
                // ..
                /* ['<global>::doCycleStop' end] */
                Cyclestate = ID_CYCLE_STOPPED;
            }
            else if( edre.s1Step1Done )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doStep2' begin] */
                edre.s1Step2Done = false;
                // Acciones que lanzan Step2
                // ...
                /* ['<global>::doStep2' end] */
                Cyclestate = ID_CYCLE_STEP2;
            }
            return ID_CYCLE_STEP1;
        }
        /* State ID: ID_CYCLE_STEP2 */
        case ID_CYCLE_STEP2:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doCycleStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Cycle.
                // ..
                /* ['<global>::doCycleStop' end] */
                Cyclestate = ID_CYCLE_STOPPED;
            }
            else if( (edre.s1Enable == false) && (edre.s1Step2Done) )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doCycleDisable' begin] */
                // Acciones de deshabilitaci_n a nivel de subm_quina Cycle.
                // ..
                /* ['<global>::doCycleDisable' end] */
                Cyclestate = ID_CYCLE_DISABLED;
            }
            else if( edre.s1Step2Done )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doStep1' begin] */
                edre.s1Step1Done = false;
                // Acciones que lanzan Step1
                // ...
                /* ['<global>::doStep1' end] */
                Cyclestate = ID_CYCLE_STEP1;
            }
            return ID_CYCLE_STEP2;
        }
        /* State ID: ID_CYCLE_DISABLED */
        case ID_CYCLE_DISABLED:
        {
            Cyclestate = ID_CYCLE_INITIAL;
            return ID_CYCLE_DISABLED;
        }
    }
}
/* ['Cycle' end (DON'T REMOVE THIS LINE!)] */

/* ['DegradedCycle' begin (DON'T REMOVE THIS LINE!)] */
/** ***************************************************
This state machine simply models the insufflation-exsufflation loop.
If a stop request is commanded, the current cycle is finished, before finishing (going to final state).
*************************************************** */
/* set initial state */
STATE_T DegradedCyclestate = ID_DEGRADEDCYCLE_INITIAL;
STATE_T DegradedCycle(  )
{

    switch( DegradedCyclestate )
    {
        /* State ID: ID_DEGRADEDCYCLE_INITIAL */
        case ID_DEGRADEDCYCLE_INITIAL:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_DEGRADEDCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDegCycleStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina DegradedCycle.
                // ..
                /* ['<global>::doDegCycleStop' end] */
                DegradedCyclestate = ID_DEGRADEDCYCLE_STOPPED;
            }
            else
            {
                /* Transition ID: ID_DEGRADEDCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDegStep1' begin] */
                edre.s1DegStep1Done = false;
                // Acciones que lanzan DegStep1
                // ...
                /* ['<global>::doDegStep1' end] */
                DegradedCyclestate = ID_DEGRADEDCYCLE_DEGSTEP1;
            }
            return ID_DEGRADEDCYCLE_INITIAL;
        }
        /* State ID: ID_DEGRADEDCYCLE_STOPPED */
        case ID_DEGRADEDCYCLE_STOPPED:
        {
            DegradedCyclestate = ID_DEGRADEDCYCLE_INITIAL;
            return ID_DEGRADEDCYCLE_STOPPED;
        }
        /* State ID: ID_DEGRADEDCYCLE_DEGSTEP1 */
        case ID_DEGRADEDCYCLE_DEGSTEP1:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_DEGRADEDCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDegCycleStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina DegradedCycle.
                // ..
                /* ['<global>::doDegCycleStop' end] */
                DegradedCyclestate = ID_DEGRADEDCYCLE_STOPPED;
            }
            else if( edre.s1DegStep1Done )
            {
                /* Transition ID: ID_DEGRADEDCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDegStep2' begin] */
                edre.s1DegStep2Done = false;
                // Acciones que lanzan DegStep2
                // ...
                /* ['<global>::doDegStep2' end] */
                DegradedCyclestate = ID_DEGRADEDCYCLE_DEGSTEP2;
            }
            return ID_DEGRADEDCYCLE_DEGSTEP1;
        }
        /* State ID: ID_DEGRADEDCYCLE_DEGSTEP2 */
        case ID_DEGRADEDCYCLE_DEGSTEP2:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_DEGRADEDCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDegCycleStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina DegradedCycle.
                // ..
                /* ['<global>::doDegCycleStop' end] */
                DegradedCyclestate = ID_DEGRADEDCYCLE_STOPPED;
            }
            else if( (edre.s1Enable == false) && (edre.s1DegStep2Done) )
            {
                /* Transition ID: ID_DEGRADEDCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDegCycleDisable' begin] */
                // Acciones de deshabilitaci_n a nivel de subm_quina DegCycle.
                // ..
                /* ['<global>::doDegCycleDisable' end] */
                DegradedCyclestate = ID_DEGRADEDCYCLE_DISABLED;
            }
            else if( edre.s1DegStep2Done )
            {
                /* Transition ID: ID_DEGRADEDCYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDegStep1' begin] */
                edre.s1DegStep1Done = false;
                // Acciones que lanzan DegStep1
                // ...
                /* ['<global>::doDegStep1' end] */
                DegradedCyclestate = ID_DEGRADEDCYCLE_DEGSTEP1;
            }
            return ID_DEGRADEDCYCLE_DEGSTEP2;
        }
        /* State ID: ID_DEGRADEDCYCLE_DISABLED */
        case ID_DEGRADEDCYCLE_DISABLED:
        {
            DegradedCyclestate = ID_DEGRADEDCYCLE_INITIAL;
            return ID_DEGRADEDCYCLE_DISABLED;
        }
    }
}
/* ['DegradedCycle' end (DON'T REMOVE THIS LINE!)] */

/* ['Run' begin (DON'T REMOVE THIS LINE!)] */
/** ***************************************************
VentilationSession models the normal working cycle of the ventilator.  At the beginning of each session (when the machine is connected to a new patient, or when the machine has been stopped for any reason) the machine shall execute a homing operation for reference, and just after the homing, it shall begin the ventilation cycle loop.  The ventilation cycle loop is modeled in a sub-machine called VentilationCycle.  If an stop is requested, this machine will wait for VentilationCycle to end, and then will return the control to the parent machine (MechVentilation).
*************************************************** */
/* set initial state */
STATE_T Runstate = ID_RUN_INITIAL;
STATE_T Run(  )
{

    switch( Runstate )
    {
        /* State ID: ID_RUN_INITIAL */
        case ID_RUN_INITIAL:
        {
            /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
            Runstate = ID_RUN_MODECHOICE;
            return ID_RUN_INITIAL;
        }
        /* State ID: ID_RUN_MODECHOICE */
        case ID_RUN_MODECHOICE:
        {
            if( (edre.s1Mode == CTE_s1_MODE_DEGRADED) )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDegSetup' begin] */
                edre.s1CurrentMode = CTE_s1_MODE_DEGRADED;
                edre.s1SetupDone = false;
                // Acciones de preparaci_n de S1
                // que deban ejecutarse antes de comandar
                // el ciclo de trabajo del modo degradado
                // ...
                /* ['<global>::doDegSetup' end] */
                Runstate = ID_RUN_DEGRADEDSETUP;
            }
            else
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doSetup' begin] */
                edre.s1CurrentMode = CTE_s1_MODE_NORMAL;
                edre.s1SetupDone = false;
                // Acciones de preparaci_n de S1
                // que deban ejecutarse antes de comandar
                // el ciclo de trabajo
                // ...
                /* ['<global>::doSetup' end] */
                Runstate = ID_RUN_SETUP;
            }
            return ID_RUN_MODECHOICE;
        }
        /* State ID: ID_RUN_SETUP */
        case ID_RUN_SETUP:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doRunStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Run.
                // ..
                /* ['<global>::doRunStop' end] */
                /* ['<global>::doNormalStop' begin] */
                edre.s1CurrentMode = CTE_s1_MODE_UNKNOWN;
                // Acciones para parar abruptamente el modo Normal.
                // Estas acciones se sumar_n al doRunStop, que ser_ ejecutado despu_s.
                // ..
                /* ['<global>::doNormalStop' end] */
                Runstate = ID_RUN_STOPPED;
            }
            else if( (edre.s1Enable == false) && (edre.s1SetupDone)  )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doRunDisable' begin] */
                // Acciones de deshabilitaci_n a nivel de subm_quina Run.
                // ..
                /* ['<global>::doRunDisable' end] */
                /* ['<global>::doNormalDisable' begin] */
                edre.s1CurrentMode = CTE_s1_MODE_UNKNOWN;
                // Acciones para deshabilitar el modo Normal.
                // Estas acciones se sumar_n al doRunDisable, que ser_ ejecutado despu_s.
                // ...
                /* ['<global>::doNormalDisable' end] */
                Runstate = ID_RUN_DISABLED;
            }
            else if( edre.s1SetupDone )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::launchCycle' begin] */
                // Acciones de lanzamiento del ciclo de trabajo
                // ...
                /* ['<global>::launchCycle' end] */
                Runstate = ID_RUN_CYCLE;
            }
            return ID_RUN_SETUP;
        }
        /* State ID: ID_RUN_CYCLE */
        case ID_RUN_CYCLE:
        {
            /* call substate function */
            STATE_T cycle_retval = Cycle(  );
            if( cycle_retval < 0 )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                Runstate = ID_RUN_STOPPRESENT;
            }
            else if( (edre.s1Mode != edre.s1CurrentMode) )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::closeNormalMode' begin] */
                // Acciones de cierre del modo normal
                // ...
                /* ['<global>::closeNormalMode' end] */
                Runstate = ID_RUN_MODECHOICE;
            }
            return ID_RUN_CYCLE;
        }
        /* State ID: ID_RUN_STOPPRESENT */
        case ID_RUN_STOPPRESENT:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doRunStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Run.
                // ..
                /* ['<global>::doRunStop' end] */
                Runstate = ID_RUN_STOPPED;
            }
            else
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doRunDisable' begin] */
                // Acciones de deshabilitaci_n a nivel de subm_quina Run.
                // ..
                /* ['<global>::doRunDisable' end] */
                Runstate = ID_RUN_DISABLED;
            }
            return ID_RUN_STOPPRESENT;
        }
        /* State ID: ID_RUN_DISABLED */
        case ID_RUN_DISABLED:
        {
            Runstate = ID_RUN_INITIAL;
            return ID_RUN_DISABLED;
        }
        /* State ID: ID_RUN_STOPPED */
        case ID_RUN_STOPPED:
        {
            Runstate = ID_RUN_INITIAL;
            return ID_RUN_STOPPED;
        }
        /* State ID: ID_RUN_DEGRADEDSETUP */
        case ID_RUN_DEGRADEDSETUP:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDegStop' begin] */
                edre.s1CurrentMode = CTE_s1_MODE_UNKNOWN;
                // Acciones para parar abruptamente el modo Degradado.
                // Estas acciones se sumar_n al doRunStop, que ser_ ejecutado despu_s.
                // ...
                /* ['<global>::doDegStop' end] */
                /* ['<global>::doRunStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Run.
                // ..
                /* ['<global>::doRunStop' end] */
                Runstate = ID_RUN_STOPPED;
            }
            else if( (edre.s1Enable == false) && (edre.s1SetupDone)  )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDegDisable' begin] */
                edre.s1CurrentMode = CTE_s1_MODE_UNKNOWN;
                // Acciones para deshabilitar el modo Degradado.
                // Estas acciones se sumar_n al doRunDisable, que ser_ ejecutado despu_s.
                // ...
                /* ['<global>::doDegDisable' end] */
                /* ['<global>::doRunDisable' begin] */
                // Acciones de deshabilitaci_n a nivel de subm_quina Run.
                // ..
                /* ['<global>::doRunDisable' end] */
                Runstate = ID_RUN_DISABLED;
            }
            else if( edre.s1SetupDone )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::launchDegCycle' begin] */
                // Acciones de lanzamiento ciclo de trabajo (subm_quina Cycle)
                // ...
                /* ['<global>::launchDegCycle' end] */
                Runstate = ID_RUN_DEGRADEDCYCLE;
            }
            return ID_RUN_DEGRADEDSETUP;
        }
        /* State ID: ID_RUN_DEGRADEDCYCLE */
        case ID_RUN_DEGRADEDCYCLE:
        {
            /* call substate function */
            STATE_T degradedcycle_retval = DegradedCycle(  );
            if( (edre.s1Mode != edre.s1CurrentMode) )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::closeDegMode' begin] */
                // Acciones de cierre del modo Degraded
                // ...
                /* ['<global>::closeDegMode' end] */
                Runstate = ID_RUN_MODECHOICE;
            }
            else if( degradedcycle_retval < 0 )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                Runstate = ID_RUN_STOPPRESENT;
            }
            return ID_RUN_DEGRADEDCYCLE;
        }
    }
}
/* ['Run' end (DON'T REMOVE THIS LINE!)] */

/* ['S1' begin (DON'T REMOVE THIS LINE!)] */
/** ***************************************************
FSM principal del m_dulo S1.
Contiene el manejo del los estos Ready, Stop y Run.
*************************************************** */
/* set initial state */
STATE_T S1state = ID_S1_INITIAL;
STATE_T S1(  )
{

    switch( S1state )
    {
        /* State ID: ID_S1_INITIAL */
        case ID_S1_INITIAL:
        {
            /* Transition ID: ID_S1_TRANSITION_CONNECTION */
            S1state = ID_S1_S1STOPPRESENT;
            return ID_S1_INITIAL;
        }
        /* State ID: ID_S1_S1STOPPRESENT */
        case ID_S1_S1STOPPRESENT:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_S1_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doStop' begin] */
                // Acciones de Parada abrupta de S1
                // ...
                /* ['<global>::doStop' end] */
                S1state = ID_S1_STOP;
            }
            else
            {
                /* Transition ID: ID_S1_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDisable' begin] */
                // Operaciones de deshabilitaci_n de S1
                // ...
                /* ['<global>::doDisable' end] */
                S1state = ID_S1_READY;
            }
            return ID_S1_S1STOPPRESENT;
        }
        /* State ID: ID_S1_STOP */
        case ID_S1_STOP:
        {
            if( (!edre.s1Stop && edre.s1Rehab) )
            {
                /* Transition ID: ID_S1_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doRehab' begin] */
                // Acciones de rearme de S1
                // ...
                /* ['<global>::doRehab' end] */
                /* ['<global>::doDisable' begin] */
                // Operaciones de deshabilitaci_n de S1
                // ...
                /* ['<global>::doDisable' end] */
                S1state = ID_S1_READY;
            }
            return ID_S1_STOP;
        }
        /* State ID: ID_S1_READY */
        case ID_S1_READY:
        {
            if( edre.s1Stop == true )
            {
                /* Transition ID: ID_S1_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doStop' begin] */
                // Acciones de Parada abrupta de S1
                // ...
                /* ['<global>::doStop' end] */
                S1state = ID_S1_STOP;
            }
            else if( false )
            {
                /* Transition ID: ID_S1_TRANSITION_CONNECTION */
                S1state = ID_S1_UNREACHABLEFINAL;
            }
            else if( edre.s1Enable )
            {
                /* Transition ID: ID_S1_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::launchRun' begin] */
                // Acciones de lanzamiento de la subm_quina Run
                // ...
                /* ['<global>::launchRun' end] */
                S1state = ID_S1_RUN;
            }
            return ID_S1_READY;
        }
        /* State ID: ID_S1_UNREACHABLEFINAL */
        case ID_S1_UNREACHABLEFINAL:
        {
            S1state = ID_S1_INITIAL;
            return ID_S1_UNREACHABLEFINAL;
        }
        /* State ID: ID_S1_RUN */
        case ID_S1_RUN:
        {
            /* call substate function */
            STATE_T run_retval = Run(  );
            if( (run_retval < 0) )
            {
                /* Transition ID: ID_S1_TRANSITION_CONNECTION */
                S1state = ID_S1_S1STOPPRESENT;
            }
            return ID_S1_RUN;
        }
    }
}
/* ['S1' end (DON'T REMOVE THIS LINE!)] */
