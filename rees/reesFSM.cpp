/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "reesFSM.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['DbgLEDBlink' begin (DON'T REMOVE THIS LINE!)] */
void DbgLEDBlink(  )
{
    /* set initial state */
    static STATE_T state = ID_DBGLEDBLINK_INIT;

    switch( state )
    {
        /* State ID: ID_DBGLEDBLINK_INIT */
        case ID_DBGLEDBLINK_INIT:
        {
            /* Transition ID: ID_DBGLEDBLINK_ALWAYS */
            /* Actions: */
            /* ['<global>::blinkingTimerReset' begin] */
            dre.dbgLedBlinkTimer = 0;
            /* ['<global>::blinkingTimerReset' end] */
            /* ['<global>::ledOn' begin] */
            dre.DBG_LED = true;
            /* ['<global>::ledOn' end] */
            state = ID_DBGLEDBLINK_ON;
            break;
        }
        /* State ID: ID_DBGLEDBLINK_ON */
        case ID_DBGLEDBLINK_ON:
        {
            if( dre.dbgLedBlinkTimer >= CFG_DBGLED_BLINK_TIMER_DUTY )
            {
                /* Transition ID: ID_DBGLEDBLINK_DUTYEND */
                /* Actions: */
                /* ['<global>::blinkingTimerIncrement' begin] */
                dre.dbgLedBlinkTimer++;
                /* ['<global>::blinkingTimerIncrement' end] */
                /* ['<global>::ledOff' begin] */
                dre.DBG_LED = false;
                /* ['<global>::ledOff' end] */
                state = ID_DBGLEDBLINK_OFF;
            }
            else
            {
                /* Transition ID: ID_DBGLEDBLINK_TIMERINCREMENT */
                /* Actions: */
                /* ['<global>::blinkingTimerIncrement' begin] */
                dre.dbgLedBlinkTimer++;
                /* ['<global>::blinkingTimerIncrement' end] */
            }
            break;
        }
        /* State ID: ID_DBGLEDBLINK_OFF */
        case ID_DBGLEDBLINK_OFF:
        {
            if( dre.dbgLedBlinkTimer >= CFG_DBGLED_BLINK_TIMER_PERIOD )
            {
                /* Transition ID: ID_DBGLEDBLINK_PERIODEND */
                /* Actions: */
                /* ['<global>::blinkingTimerReset' begin] */
                dre.dbgLedBlinkTimer = 0;
                /* ['<global>::blinkingTimerReset' end] */
                /* ['<global>::ledOn' begin] */
                dre.DBG_LED = true;
                /* ['<global>::ledOn' end] */
                state = ID_DBGLEDBLINK_ON;
            }
            else
            {
                /* Transition ID: ID_DBGLEDBLINK_TIMERINCREMENT */
                /* Actions: */
                /* ['<global>::blinkingTimerIncrement' begin] */
                dre.dbgLedBlinkTimer++;
                /* ['<global>::blinkingTimerIncrement' end] */
            }
            break;
        }
    }
}
/* ['DbgLEDBlink' end (DON'T REMOVE THIS LINE!)] */

/* ['StatusLED' begin (DON'T REMOVE THIS LINE!)] */
void StatusLED(  )
{
    /* set initial state */
    static STATE_T state = ID_STATUSLED_INIT;

    switch( state )
    {
        /* State ID: ID_STATUSLED_INIT */
        case ID_STATUSLED_INIT:
        {
            /* Transition ID: ID_STATUSLED_ALWAYS */
            /* Actions: */
            /* ['<global>::stLedOn' begin] */
            dre.statusLED = true;
            /* ['<global>::stLedOn' end] */
            state = ID_STATUSLED_READY;
            break;
        }
        /* State ID: ID_STATUSLED_READY */
        case ID_STATUSLED_READY:
        {
            break;
        }
    }
}
/* ['StatusLED' end (DON'T REMOVE THIS LINE!)] */
