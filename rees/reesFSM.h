/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "reesFSM_CI.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['StatusLED' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_STATUSLED_INIT 3
#define ID_STATUSLED_READY 4

void StatusLED(  );
/* ['StatusLED' end (DON'T REMOVE THIS LINE!)] */

/* ['DbgLEDBlink' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_DBGLEDBLINK_INIT 0
#define ID_DBGLEDBLINK_OFF 1
#define ID_DBGLEDBLINK_ON 2

void DbgLEDBlink(  );
/* ['DbgLEDBlink' end (DON'T REMOVE THIS LINE!)] */
