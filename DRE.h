#ifndef __DRE_H
#define __DRE_H

#include "core_cfg.h"
#include "core_types.h"


typedef struct {
// LED -- Does not need declaration STATUS_LED;
BOOL statusLED;
t_dbgledblinktimer dbgLedBlinkTimer;
t_dbg_led DBG_LED;
// Bus -- Does not need declaration SDA;
// Bus -- Does not need declaration SCL;
BOOL ResetRelay;
BOOL LCD_DB7;
BOOL LCD_DB6;
BOOL LCD_DB5;
BOOL LCD_DB4;
BOOL LCD_DB3;
BOOL LCD_DB2;
BOOL LCD_DB1;
BOOL LCD_DB0;
// Bus -- Does not need declaration BME_CS2;
// Bus -- Does not need declaration BME_MISO;
// Bus -- Does not need declaration BME_MOSI;
// Bus -- Does not need declaration BME_SCK;
// Bus -- Does not need declaration BME_CS1;
// Power -- Does not need declaration _5V;
// Power -- Does not need declaration GND;
t_int_fs INT_FS;
t_int_fc INT_FC;
t_int_hs INT_HS;
t_int_hc INT_HC;
t_systemdate_w SystemDate_W;
t_systemtime_w SystemTime_W;
t_peakfactor PeakFactor;
t_plateaufactor PlateauFactor;
t_peepfactor PeepFactor;
t_frequencyfactor FrequencyFactor;
t_tidalvolumenfactor TidalVolumenFactor;
t_minimumvolumefactor MinimumVolumeFactor;
t_triggerfactor TriggerFactor;
t_batteryleveltrigger BatteryLevelTrigger;
t_patienttype PatientType;
t_patientweight PatientWeight;
t_patientheight PatientHeight;
t_tidalvolume TidalVolume;
t_pidmode PIDMode;
t_pidstate PIDState;
t_piderrorbits PIDErrorBits;
t_alarms Alarms;
t_status Status;
t_power_startmode POWER_StartMode;
t_power_stopmode POWER_StopMode;
t_home_mode HOME_Mode;
BOOL SetWriteData;
BOOL SetTime;
BOOL Trigger;
BOOL PeakPBPlus;
BOOL PeakPBMinus;
BOOL PeepPBPlus;
BOOL PeepPBMinus;
BOOL FrPBPlus;
BOOL FrPBMinus;
BOOL RecruitmentOn;
BOOL RecruitmentOff;
BOOL RecruitmentMem;
BOOL RecruitmentRunning;
BOOL RecruitmentEnding;
BOOL RecruitmentEnd;
BOOL ErrorAck;
BOOL Reset;
BOOL PIDError;
BOOL PeakDisplayPreAlarmHI;
BOOL PeakDisplayAlarmHI;
BOOL PeakDisplayPreAlarmLOW;
BOOL PeakDisplayAlarmLOW;
BOOL PeakInterlockPreAlarmHI;
BOOL PeakInterlockAlarmHI;
BOOL PeakInterlockPreAlarmLOW;
BOOL PeakInterlockAlarmLOW;
BOOL PlateauDisplayPreAlarmHI;
BOOL PlateauDisplayAlarmHI;
BOOL PlateauDisplayPreAlarmLOW;
BOOL PlateauDisplayAlarmLOW;
BOOL PlateauInterlockPreAlarmHI;
BOOL PlateauInterlockAlarmHI;
BOOL PlateauInterlockPreAlarmLOW;
BOOL PlateauInterlockAlarmLOW;
BOOL PeepDisplayPreAlarmHI;
BOOL PeepDisplayAlarmHI;
BOOL PeepDisplayPreAlarmLOW;
BOOL PeepDisplayAlarmLOW;
BOOL PeepInterlockPreAlarmHI;
BOOL PeepInterlockAlarmHI;
BOOL PeepInterlockPreAlarmLOW;
BOOL PeepInterlockAlarmLOW;
BOOL FrequencyDisplayPreAlarmHI;
BOOL FrequencyDisplayAlarmHI;
BOOL FrequencyDisplayPreAlarmLOW;
BOOL FrequencyDisplayAlarmLOW;
BOOL FrequencyInterlockPreAlarmHI;
BOOL FrequencyInterlockAlarmHI;
BOOL FrequencyInterlockPreAlarmLOW;
BOOL FrequencyInterlockAlarmLOW;
BOOL TidalVolumeDisplayPreAlarmHI;
BOOL TidalVolumeDisplayAlarmHI;
BOOL TidalVolumeDisplayPreAlarmLOW;
BOOL TidalVolumeDisplayAlarmLOW;
BOOL TidalVolumeInterlockPreAlarmHI;
BOOL TidalVolumeInterlockAlarmHI;
BOOL TidalVolumeInterlockPreAlarmLOW;
BOOL TidalVolumeInterlockAlarmLOW;
BOOL MinuteVolumeDisplayPreAlarmHI;
BOOL MinuteVolumeDisplayAlarmHI;
BOOL MinuteVolumeDisplayPreAlarmLOW;
BOOL MinuteVolumeDisplayAlarmLOW;
BOOL MinuteVolumeInterlockPreAlarmHI;
BOOL MinuteVolumeInterlockAlarmHI;
BOOL MinuteVolumeInterlockPreAlarmLOW;
BOOL MinuteVolumeInterlockAlarmLOW;
BOOL TriggerDisplayPreAlarmHI;
BOOL TriggerDisplayAlarmHI;
BOOL TriggerDisplayPreAlarmLOW;
BOOL TriggerDisplayAlarmLOW;
BOOL TriggerInterlockPreAlarmHI;
BOOL TriggerInterlockAlarmHI;
BOOL TriggerInterlockPreAlarmLOW;
BOOL TriggerInterlockAlarmLOW;
BOOL BatteryDisplayPreAlarmHI;
BOOL BatteryDisplayAlarmHI;
BOOL BatteryDisplayPreAlarmLOW;
BOOL BatteryDisplayAlarmLOW;
BOOL BatteryInterlockPreAlarmHI;
BOOL BatteryInterlockAlarmHI;
BOOL BatteryInterlockPreAlarmLOW;
BOOL BatteryInterlockAlarmLOW;
BOOL Enable;
BOOL Busy;
BOOL POWER_Enable;
BOOL POWER_Status;
BOOL POWER_Error;
BOOL RESET_Execute;
BOOL RESET_Done;
BOOL RESET_Error;
BOOL HOME_Execute;
BOOL HOME_Done;
BOOL HOME_Error;
BOOL HALT_Execute;
BOOL HALT_Done;
BOOL HALT_Error;
BOOL ABSOLUTE_Execute;
BOOL ABSOLUTE_Done;
BOOL ABSOLUTE_Error;
BOOL RELATIVE_Execute;
BOOL RELATIVE_Done;
BOOL RELATIVE_Error;
BOOL Velocity_Execute;
BOOL Velocity_Current;
BOOL Velocity_invelocity;
BOOL Velocity_Error;
BOOL JOG_Forward;
BOOL JOG_Backward;
BOOL JOG_Invelocity;
BOOL JOG_Error;
BOOL Permissions;
BOOL Operation_ON;
BOOL Emergency;
BOOL GeneralAck;
BOOL MotorAck;
BOOL ProcessAck;
BOOL GeneralReset;
BOOL MotorReset;
BOOL ProcessReset;
UI_16 PeakValue;
UI_16 PlateauValue;
UI_16 PeepValue;
UI_16 FrequencyValue;
UI_16 TidalVolumeValue;
UI_16 MinumValue;
UI_16 DrivingPreassure;
UI_16 BatteryVoltage;
UI_16 BatteryCapacity;
UI_16 Pressure1;
UI_16 Pressure2;
UI_16 VolumeAccumulated;
UI_16 Flow;
UI_16 SystemDateRetVal;
t_datetime SystemDateOut;
UI_16 LocalDateRetVal;
t_datetime LocalDateOut;
t_datetime WriteDateTime;
t_datetime WriteDateOut;
t_datetime StatuSetTime;
UI_16 PeakMax;
UI_16 PeakSetpoint;
UI_16 PeakMin;
UI_16 PlateauMax;
UI_16 PlateauSetpoint;
UI_16 PlateauMin;
UI_16 PeepMax;
UI_16 PeepSetpoint;
UI_16 PeepMin;
UI_16 FrequencyMax;
UI_16 FrequencySetpoint;
UI_16 FrequencyMin;
UI_16 TidalVolumenMax;
UI_16 TidalVolumenSetpoint;
UI_16 TidalVolumenMin;
UI_16 MinimumVolumeMax;
UI_16 MinimumVolumeSetpoint;
UI_16 MinimumVolumeMin;
UI_16 TriggerSetpoint;
UI_16 RecruitmentTimer;
UI_16 RecruitmentElap;
t_datetime TimeStart;
t_datetime TimeEnd;
UI_16 FlowSetpoint;
FL_16 PIDOutReal;
UI_16 PIDOutInt;
UI_16 PIDOutPWM;
UI_16 PIDOutMotor;
UI_16 PeakSetpointPreAlarmHI;
UI_16 PeakSetpointAlarmHI;
UI_16 PeakSetpointPreAlarmLOW;
UI_16 PeakSetpointAlarmLOW;
UI_16 PeakSetpointHysteresisHI;
UI_16 PeakSetpointHysteresisLOW;
UI_16 PlateauSetpointPreAlarmHI;
UI_16 PlateauSetpointAlarmHI;
UI_16 PlateauSetpointPreAlarmLOW;
UI_16 PlateaukSetpointAlarmLOW;
UI_16 PlateaukSetpointHysteresisHI;
UI_16 PlateaukSetpointHysteresisLOW;
UI_16 PeepSetpointPreAlarmHI;
UI_16 PeepSetpointAlarmHI;
UI_16 PeepSetpointPreAlarmLOW;
UI_16 PeepSetpointAlarmLOW;
UI_16 PeepSetpointHysteresisHI;
UI_16 PeepSetpointHysteresisLOW;
UI_16 FrequencySetpointPreAlarmHI;
UI_16 FrequencySetpointAlarmHI;
UI_16 FrequencySetpointPreAlarmLOW;
UI_16 FrequencySetpointAlarmLOW;
UI_16 FrequencySetpointHysteresisHI;
UI_16 FrequencySetpointHysteresisLOW;
UI_16 TidalVolumeSetpointPreAlarmHI;
UI_16 TidalVolumeSetpointAlarmHI;
UI_16 TidalVolumeSetpointPreAlarmLOW;
UI_16 TidalVolumeSetpointAlarmLOW;
UI_16 TidalVolumeHystersisHI;
UI_16 TidalVolumeHystersisLOW;
UI_16 MinuteVolumeSetpointPreAlarmHI;
UI_16 MinuteVolumeSetpointAlarmHI;
UI_16 MinuteVolumeSetpointPreAlarmLOW;
UI_16 MinuteVolumeSetpointAlarmLOW;
UI_16 MinuteVolumeHysteresisHI;
UI_16 MinuteVolumeHysteresisLOW;
UI_16 TriggeVolumeSetpointPreAlarmHI;
UI_16 TriggerVolumeSetpointAlarmHI;
UI_16 TriggerVolumeSetpointPreAlarmLOW;
UI_16 TriggerVolumeSetpointAlarmLOW;
UI_16 TriggerVolumeHysteresisHI;
UI_16 TriggerVolumeHysteresisLOW;
UI_16 BatterySetpointPreAlarmHI;
UI_16 BatterySetpointAlarmHI;
UI_16 BatterySetpointPreAlarmLOW;
UI_16 BatterySetpointAlarmLOW;
UI_16 BatterySetpointHysteresisHI;
UI_16 BatterySetpointHysteresisLOW;
UI_16 Frecuency;
UI_16 Duty_Cycle;
UI_16 HOME_Position;
UI_16 ABSOLUTE_Position;
UI_16 ABSOLUTE_Velocity;
UI_16 RELATIVE_Distance;
UI_16 RELATIVE_Velocity;
UI_16 Velocity_Velocity;
UI_16 JOG_Velocity;
BOOL EmergencyStop;
BOOL TurnAxisHwHi;
BOOL BeaconLightRed;
BOOL BeaconLightOrange;
BOOL TurnAxisPulse;
BOOL TurnAxisDir;
BOOL TurnAxisEnable;
BOOL BeaconLightGreen;
BOOL Buzzer;
t_pwm PeepValve;
BOOL TurnAxisHwLow;
BOOL ServoPend;
BOOL ServoAlarm;
uint16_t AtmosfericPressure;
uint16_t DiferentialPressure;
// Bus -- Does not need declaration SerTX;
// Bus -- Does not need declaration SerRX;
// TBD -- Does not need declaration EmgcyButton;
UI_8 ApiVersion;
t_machineuuid MachineUuid;
UI_16 FirmwareVersion;
UI_16 Uptime15mCounter;
UI_8 MutedAlarmSeconds;
UI_16 CycleRpmLast30s;
t_cycleindications CycleIndications;
UI_16 CycleDuration;
t_ventilationflags VentilationFlags;
UI_16 VolumeSetting;
UI_16 RpmSetting;
UI_8 RampDegreesSetting;
UI_8 EiRatioSetting;
UI_8 RecruitTimer;
t_incomingframe IncomingFrame;
t_outgoingframe OutgoingFrame;
// Display -- Does not need declaration Display;
// Display -- Does not need declaration DisplayPlus;
// Keyboard -- Does not need declaration DisplayButtons;
UI_16 FlowPublic;
BOOL Power_EN;
BOOL Power_Axis;
BOOL Power_ENO;
BOOL Reset_EN;
BOOL Reset_Axis;
BOOL Reset_ENO;
BOOL Home_EN;
BOOL Home_Axis;
BOOL Home_ENO;
BOOL Halt_EN;
BOOL Halt_Axis;
BOOL Halt_ENO;
BOOL Absolute_EN;
BOOL Absolute_Axis;
BOOL Absolute_ENO;
BOOL Relative_EN;
BOOL Relative_Axis;
BOOL Relative_ENO;
BOOL Velocity_EN;
BOOL Velocity_Axis;
BOOL Velocity_ENO;
BOOL Jog_EN;
BOOL Jog_Axis;
BOOL Jog_ENO;
BOOL EN;
BOOL EN0;
UI_32 CycleStartTime;

} t_dre;

typedef struct {
// (null) No diag variables for STATUS_LED
BOOL enable_statusLED;
BOOL statusLED;
BOOL enable_dbgLedBlinkTimer;
t_dbgledblinktimer dbgLedBlinkTimer;
BOOL enable_DBG_LED;
t_dbg_led DBG_LED;
// (null) No diag variables for SDA
// (null) No diag variables for SCL
BOOL enable_ResetRelay;
BOOL ResetRelay;
BOOL enable_LCD_DB7;
BOOL LCD_DB7;
BOOL enable_LCD_DB6;
BOOL LCD_DB6;
BOOL enable_LCD_DB5;
BOOL LCD_DB5;
BOOL enable_LCD_DB4;
BOOL LCD_DB4;
BOOL enable_LCD_DB3;
BOOL LCD_DB3;
BOOL enable_LCD_DB2;
BOOL LCD_DB2;
BOOL enable_LCD_DB1;
BOOL LCD_DB1;
BOOL enable_LCD_DB0;
BOOL LCD_DB0;
// (null) No diag variables for BME_CS2
// (null) No diag variables for BME_MISO
// (null) No diag variables for BME_MOSI
// (null) No diag variables for BME_SCK
// (null) No diag variables for BME_CS1
// (null) No diag variables for _5V
// (null) No diag variables for GND
BOOL enable_INT_FS;
t_int_fs INT_FS;
BOOL enable_INT_FC;
t_int_fc INT_FC;
BOOL enable_INT_HS;
t_int_hs INT_HS;
BOOL enable_INT_HC;
t_int_hc INT_HC;
BOOL enable_SystemDate_W;
t_systemdate_w SystemDate_W;
BOOL enable_SystemTime_W;
t_systemtime_w SystemTime_W;
BOOL enable_PeakFactor;
t_peakfactor PeakFactor;
BOOL enable_PlateauFactor;
t_plateaufactor PlateauFactor;
BOOL enable_PeepFactor;
t_peepfactor PeepFactor;
BOOL enable_FrequencyFactor;
t_frequencyfactor FrequencyFactor;
BOOL enable_TidalVolumenFactor;
t_tidalvolumenfactor TidalVolumenFactor;
BOOL enable_MinimumVolumeFactor;
t_minimumvolumefactor MinimumVolumeFactor;
BOOL enable_TriggerFactor;
t_triggerfactor TriggerFactor;
BOOL enable_BatteryLevelTrigger;
t_batteryleveltrigger BatteryLevelTrigger;
BOOL enable_PatientType;
t_patienttype PatientType;
BOOL enable_PatientWeight;
t_patientweight PatientWeight;
BOOL enable_PatientHeight;
t_patientheight PatientHeight;
BOOL enable_TidalVolume;
t_tidalvolume TidalVolume;
BOOL enable_PIDMode;
t_pidmode PIDMode;
BOOL enable_PIDState;
t_pidstate PIDState;
BOOL enable_PIDErrorBits;
t_piderrorbits PIDErrorBits;
BOOL enable_Alarms;
t_alarms Alarms;
BOOL enable_Status;
t_status Status;
BOOL enable_POWER_StartMode;
t_power_startmode POWER_StartMode;
BOOL enable_POWER_StopMode;
t_power_stopmode POWER_StopMode;
BOOL enable_HOME_Mode;
t_home_mode HOME_Mode;
BOOL enable_SetWriteData;
BOOL SetWriteData;
BOOL enable_SetTime;
BOOL SetTime;
BOOL enable_Trigger;
BOOL Trigger;
BOOL enable_PeakPBPlus;
BOOL PeakPBPlus;
BOOL enable_PeakPBMinus;
BOOL PeakPBMinus;
BOOL enable_PeepPBPlus;
BOOL PeepPBPlus;
BOOL enable_PeepPBMinus;
BOOL PeepPBMinus;
BOOL enable_FrPBPlus;
BOOL FrPBPlus;
BOOL enable_FrPBMinus;
BOOL FrPBMinus;
BOOL enable_RecruitmentOn;
BOOL RecruitmentOn;
BOOL enable_RecruitmentOff;
BOOL RecruitmentOff;
BOOL enable_RecruitmentMem;
BOOL RecruitmentMem;
BOOL enable_RecruitmentRunning;
BOOL RecruitmentRunning;
BOOL enable_RecruitmentEnding;
BOOL RecruitmentEnding;
BOOL enable_RecruitmentEnd;
BOOL RecruitmentEnd;
BOOL enable_ErrorAck;
BOOL ErrorAck;
BOOL enable_Reset;
BOOL Reset;
BOOL enable_PIDError;
BOOL PIDError;
BOOL enable_PeakDisplayPreAlarmHI;
BOOL PeakDisplayPreAlarmHI;
BOOL enable_PeakDisplayAlarmHI;
BOOL PeakDisplayAlarmHI;
BOOL enable_PeakDisplayPreAlarmLOW;
BOOL PeakDisplayPreAlarmLOW;
BOOL enable_PeakDisplayAlarmLOW;
BOOL PeakDisplayAlarmLOW;
BOOL enable_PeakInterlockPreAlarmHI;
BOOL PeakInterlockPreAlarmHI;
BOOL enable_PeakInterlockAlarmHI;
BOOL PeakInterlockAlarmHI;
BOOL enable_PeakInterlockPreAlarmLOW;
BOOL PeakInterlockPreAlarmLOW;
BOOL enable_PeakInterlockAlarmLOW;
BOOL PeakInterlockAlarmLOW;
BOOL enable_PlateauDisplayPreAlarmHI;
BOOL PlateauDisplayPreAlarmHI;
BOOL enable_PlateauDisplayAlarmHI;
BOOL PlateauDisplayAlarmHI;
BOOL enable_PlateauDisplayPreAlarmLOW;
BOOL PlateauDisplayPreAlarmLOW;
BOOL enable_PlateauDisplayAlarmLOW;
BOOL PlateauDisplayAlarmLOW;
BOOL enable_PlateauInterlockPreAlarmHI;
BOOL PlateauInterlockPreAlarmHI;
BOOL enable_PlateauInterlockAlarmHI;
BOOL PlateauInterlockAlarmHI;
BOOL enable_PlateauInterlockPreAlarmLOW;
BOOL PlateauInterlockPreAlarmLOW;
BOOL enable_PlateauInterlockAlarmLOW;
BOOL PlateauInterlockAlarmLOW;
BOOL enable_PeepDisplayPreAlarmHI;
BOOL PeepDisplayPreAlarmHI;
BOOL enable_PeepDisplayAlarmHI;
BOOL PeepDisplayAlarmHI;
BOOL enable_PeepDisplayPreAlarmLOW;
BOOL PeepDisplayPreAlarmLOW;
BOOL enable_PeepDisplayAlarmLOW;
BOOL PeepDisplayAlarmLOW;
BOOL enable_PeepInterlockPreAlarmHI;
BOOL PeepInterlockPreAlarmHI;
BOOL enable_PeepInterlockAlarmHI;
BOOL PeepInterlockAlarmHI;
BOOL enable_PeepInterlockPreAlarmLOW;
BOOL PeepInterlockPreAlarmLOW;
BOOL enable_PeepInterlockAlarmLOW;
BOOL PeepInterlockAlarmLOW;
BOOL enable_FrequencyDisplayPreAlarmHI;
BOOL FrequencyDisplayPreAlarmHI;
BOOL enable_FrequencyDisplayAlarmHI;
BOOL FrequencyDisplayAlarmHI;
BOOL enable_FrequencyDisplayPreAlarmLOW;
BOOL FrequencyDisplayPreAlarmLOW;
BOOL enable_FrequencyDisplayAlarmLOW;
BOOL FrequencyDisplayAlarmLOW;
BOOL enable_FrequencyInterlockPreAlarmHI;
BOOL FrequencyInterlockPreAlarmHI;
BOOL enable_FrequencyInterlockAlarmHI;
BOOL FrequencyInterlockAlarmHI;
BOOL enable_FrequencyInterlockPreAlarmLOW;
BOOL FrequencyInterlockPreAlarmLOW;
BOOL enable_FrequencyInterlockAlarmLOW;
BOOL FrequencyInterlockAlarmLOW;
BOOL enable_TidalVolumeDisplayPreAlarmHI;
BOOL TidalVolumeDisplayPreAlarmHI;
BOOL enable_TidalVolumeDisplayAlarmHI;
BOOL TidalVolumeDisplayAlarmHI;
BOOL enable_TidalVolumeDisplayPreAlarmLOW;
BOOL TidalVolumeDisplayPreAlarmLOW;
BOOL enable_TidalVolumeDisplayAlarmLOW;
BOOL TidalVolumeDisplayAlarmLOW;
BOOL enable_TidalVolumeInterlockPreAlarmHI;
BOOL TidalVolumeInterlockPreAlarmHI;
BOOL enable_TidalVolumeInterlockAlarmHI;
BOOL TidalVolumeInterlockAlarmHI;
BOOL enable_TidalVolumeInterlockPreAlarmLOW;
BOOL TidalVolumeInterlockPreAlarmLOW;
BOOL enable_TidalVolumeInterlockAlarmLOW;
BOOL TidalVolumeInterlockAlarmLOW;
BOOL enable_MinuteVolumeDisplayPreAlarmHI;
BOOL MinuteVolumeDisplayPreAlarmHI;
BOOL enable_MinuteVolumeDisplayAlarmHI;
BOOL MinuteVolumeDisplayAlarmHI;
BOOL enable_MinuteVolumeDisplayPreAlarmLOW;
BOOL MinuteVolumeDisplayPreAlarmLOW;
BOOL enable_MinuteVolumeDisplayAlarmLOW;
BOOL MinuteVolumeDisplayAlarmLOW;
BOOL enable_MinuteVolumeInterlockPreAlarmHI;
BOOL MinuteVolumeInterlockPreAlarmHI;
BOOL enable_MinuteVolumeInterlockAlarmHI;
BOOL MinuteVolumeInterlockAlarmHI;
BOOL enable_MinuteVolumeInterlockPreAlarmLOW;
BOOL MinuteVolumeInterlockPreAlarmLOW;
BOOL enable_MinuteVolumeInterlockAlarmLOW;
BOOL MinuteVolumeInterlockAlarmLOW;
BOOL enable_TriggerDisplayPreAlarmHI;
BOOL TriggerDisplayPreAlarmHI;
BOOL enable_TriggerDisplayAlarmHI;
BOOL TriggerDisplayAlarmHI;
BOOL enable_TriggerDisplayPreAlarmLOW;
BOOL TriggerDisplayPreAlarmLOW;
BOOL enable_TriggerDisplayAlarmLOW;
BOOL TriggerDisplayAlarmLOW;
BOOL enable_TriggerInterlockPreAlarmHI;
BOOL TriggerInterlockPreAlarmHI;
BOOL enable_TriggerInterlockAlarmHI;
BOOL TriggerInterlockAlarmHI;
BOOL enable_TriggerInterlockPreAlarmLOW;
BOOL TriggerInterlockPreAlarmLOW;
BOOL enable_TriggerInterlockAlarmLOW;
BOOL TriggerInterlockAlarmLOW;
BOOL enable_BatteryDisplayPreAlarmHI;
BOOL BatteryDisplayPreAlarmHI;
BOOL enable_BatteryDisplayAlarmHI;
BOOL BatteryDisplayAlarmHI;
BOOL enable_BatteryDisplayPreAlarmLOW;
BOOL BatteryDisplayPreAlarmLOW;
BOOL enable_BatteryDisplayAlarmLOW;
BOOL BatteryDisplayAlarmLOW;
BOOL enable_BatteryInterlockPreAlarmHI;
BOOL BatteryInterlockPreAlarmHI;
BOOL enable_BatteryInterlockAlarmHI;
BOOL BatteryInterlockAlarmHI;
BOOL enable_BatteryInterlockPreAlarmLOW;
BOOL BatteryInterlockPreAlarmLOW;
BOOL enable_BatteryInterlockAlarmLOW;
BOOL BatteryInterlockAlarmLOW;
BOOL enable_Enable;
BOOL Enable;
BOOL enable_Busy;
BOOL Busy;
BOOL enable_POWER_Enable;
BOOL POWER_Enable;
BOOL enable_POWER_Status;
BOOL POWER_Status;
BOOL enable_POWER_Error;
BOOL POWER_Error;
BOOL enable_RESET_Execute;
BOOL RESET_Execute;
BOOL enable_RESET_Done;
BOOL RESET_Done;
BOOL enable_RESET_Error;
BOOL RESET_Error;
BOOL enable_HOME_Execute;
BOOL HOME_Execute;
BOOL enable_HOME_Done;
BOOL HOME_Done;
BOOL enable_HOME_Error;
BOOL HOME_Error;
BOOL enable_HALT_Execute;
BOOL HALT_Execute;
BOOL enable_HALT_Done;
BOOL HALT_Done;
BOOL enable_HALT_Error;
BOOL HALT_Error;
BOOL enable_ABSOLUTE_Execute;
BOOL ABSOLUTE_Execute;
BOOL enable_ABSOLUTE_Done;
BOOL ABSOLUTE_Done;
BOOL enable_ABSOLUTE_Error;
BOOL ABSOLUTE_Error;
BOOL enable_RELATIVE_Execute;
BOOL RELATIVE_Execute;
BOOL enable_RELATIVE_Done;
BOOL RELATIVE_Done;
BOOL enable_RELATIVE_Error;
BOOL RELATIVE_Error;
BOOL enable_Velocity_Execute;
BOOL Velocity_Execute;
BOOL enable_Velocity_Current;
BOOL Velocity_Current;
BOOL enable_Velocity_invelocity;
BOOL Velocity_invelocity;
BOOL enable_Velocity_Error;
BOOL Velocity_Error;
BOOL enable_JOG_Forward;
BOOL JOG_Forward;
BOOL enable_JOG_Backward;
BOOL JOG_Backward;
BOOL enable_JOG_Invelocity;
BOOL JOG_Invelocity;
BOOL enable_JOG_Error;
BOOL JOG_Error;
BOOL enable_Permissions;
BOOL Permissions;
BOOL enable_Operation_ON;
BOOL Operation_ON;
BOOL enable_Emergency;
BOOL Emergency;
BOOL enable_GeneralAck;
BOOL GeneralAck;
BOOL enable_MotorAck;
BOOL MotorAck;
BOOL enable_ProcessAck;
BOOL ProcessAck;
BOOL enable_GeneralReset;
BOOL GeneralReset;
BOOL enable_MotorReset;
BOOL MotorReset;
BOOL enable_ProcessReset;
BOOL ProcessReset;
BOOL enable_PeakValue;
UI_16 PeakValue;
BOOL enable_PlateauValue;
UI_16 PlateauValue;
BOOL enable_PeepValue;
UI_16 PeepValue;
BOOL enable_FrequencyValue;
UI_16 FrequencyValue;
BOOL enable_TidalVolumeValue;
UI_16 TidalVolumeValue;
BOOL enable_MinumValue;
UI_16 MinumValue;
BOOL enable_DrivingPreassure;
UI_16 DrivingPreassure;
BOOL enable_BatteryVoltage;
UI_16 BatteryVoltage;
BOOL enable_BatteryCapacity;
UI_16 BatteryCapacity;
BOOL enable_Pressure1;
UI_16 Pressure1;
BOOL enable_Pressure2;
UI_16 Pressure2;
BOOL enable_VolumeAccumulated;
UI_16 VolumeAccumulated;
BOOL enable_Flow;
UI_16 Flow;
BOOL enable_SystemDateRetVal;
UI_16 SystemDateRetVal;
BOOL enable_SystemDateOut;
t_datetime SystemDateOut;
BOOL enable_LocalDateRetVal;
UI_16 LocalDateRetVal;
BOOL enable_LocalDateOut;
t_datetime LocalDateOut;
BOOL enable_WriteDateTime;
t_datetime WriteDateTime;
BOOL enable_WriteDateOut;
t_datetime WriteDateOut;
BOOL enable_StatuSetTime;
t_datetime StatuSetTime;
BOOL enable_PeakMax;
UI_16 PeakMax;
BOOL enable_PeakSetpoint;
UI_16 PeakSetpoint;
BOOL enable_PeakMin;
UI_16 PeakMin;
BOOL enable_PlateauMax;
UI_16 PlateauMax;
BOOL enable_PlateauSetpoint;
UI_16 PlateauSetpoint;
BOOL enable_PlateauMin;
UI_16 PlateauMin;
BOOL enable_PeepMax;
UI_16 PeepMax;
BOOL enable_PeepSetpoint;
UI_16 PeepSetpoint;
BOOL enable_PeepMin;
UI_16 PeepMin;
BOOL enable_FrequencyMax;
UI_16 FrequencyMax;
BOOL enable_FrequencySetpoint;
UI_16 FrequencySetpoint;
BOOL enable_FrequencyMin;
UI_16 FrequencyMin;
BOOL enable_TidalVolumenMax;
UI_16 TidalVolumenMax;
BOOL enable_TidalVolumenSetpoint;
UI_16 TidalVolumenSetpoint;
BOOL enable_TidalVolumenMin;
UI_16 TidalVolumenMin;
BOOL enable_MinimumVolumeMax;
UI_16 MinimumVolumeMax;
BOOL enable_MinimumVolumeSetpoint;
UI_16 MinimumVolumeSetpoint;
BOOL enable_MinimumVolumeMin;
UI_16 MinimumVolumeMin;
BOOL enable_TriggerSetpoint;
UI_16 TriggerSetpoint;
BOOL enable_RecruitmentTimer;
UI_16 RecruitmentTimer;
BOOL enable_RecruitmentElap;
UI_16 RecruitmentElap;
BOOL enable_TimeStart;
t_datetime TimeStart;
BOOL enable_TimeEnd;
t_datetime TimeEnd;
BOOL enable_FlowSetpoint;
UI_16 FlowSetpoint;
BOOL enable_PIDOutReal;
FL_16 PIDOutReal;
BOOL enable_PIDOutInt;
UI_16 PIDOutInt;
BOOL enable_PIDOutPWM;
UI_16 PIDOutPWM;
BOOL enable_PIDOutMotor;
UI_16 PIDOutMotor;
BOOL enable_PeakSetpointPreAlarmHI;
UI_16 PeakSetpointPreAlarmHI;
BOOL enable_PeakSetpointAlarmHI;
UI_16 PeakSetpointAlarmHI;
BOOL enable_PeakSetpointPreAlarmLOW;
UI_16 PeakSetpointPreAlarmLOW;
BOOL enable_PeakSetpointAlarmLOW;
UI_16 PeakSetpointAlarmLOW;
BOOL enable_PeakSetpointHysteresisHI;
UI_16 PeakSetpointHysteresisHI;
BOOL enable_PeakSetpointHysteresisLOW;
UI_16 PeakSetpointHysteresisLOW;
BOOL enable_PlateauSetpointPreAlarmHI;
UI_16 PlateauSetpointPreAlarmHI;
BOOL enable_PlateauSetpointAlarmHI;
UI_16 PlateauSetpointAlarmHI;
BOOL enable_PlateauSetpointPreAlarmLOW;
UI_16 PlateauSetpointPreAlarmLOW;
BOOL enable_PlateaukSetpointAlarmLOW;
UI_16 PlateaukSetpointAlarmLOW;
BOOL enable_PlateaukSetpointHysteresisHI;
UI_16 PlateaukSetpointHysteresisHI;
BOOL enable_PlateaukSetpointHysteresisLOW;
UI_16 PlateaukSetpointHysteresisLOW;
BOOL enable_PeepSetpointPreAlarmHI;
UI_16 PeepSetpointPreAlarmHI;
BOOL enable_PeepSetpointAlarmHI;
UI_16 PeepSetpointAlarmHI;
BOOL enable_PeepSetpointPreAlarmLOW;
UI_16 PeepSetpointPreAlarmLOW;
BOOL enable_PeepSetpointAlarmLOW;
UI_16 PeepSetpointAlarmLOW;
BOOL enable_PeepSetpointHysteresisHI;
UI_16 PeepSetpointHysteresisHI;
BOOL enable_PeepSetpointHysteresisLOW;
UI_16 PeepSetpointHysteresisLOW;
BOOL enable_FrequencySetpointPreAlarmHI;
UI_16 FrequencySetpointPreAlarmHI;
BOOL enable_FrequencySetpointAlarmHI;
UI_16 FrequencySetpointAlarmHI;
BOOL enable_FrequencySetpointPreAlarmLOW;
UI_16 FrequencySetpointPreAlarmLOW;
BOOL enable_FrequencySetpointAlarmLOW;
UI_16 FrequencySetpointAlarmLOW;
BOOL enable_FrequencySetpointHysteresisHI;
UI_16 FrequencySetpointHysteresisHI;
BOOL enable_FrequencySetpointHysteresisLOW;
UI_16 FrequencySetpointHysteresisLOW;
BOOL enable_TidalVolumeSetpointPreAlarmHI;
UI_16 TidalVolumeSetpointPreAlarmHI;
BOOL enable_TidalVolumeSetpointAlarmHI;
UI_16 TidalVolumeSetpointAlarmHI;
BOOL enable_TidalVolumeSetpointPreAlarmLOW;
UI_16 TidalVolumeSetpointPreAlarmLOW;
BOOL enable_TidalVolumeSetpointAlarmLOW;
UI_16 TidalVolumeSetpointAlarmLOW;
BOOL enable_TidalVolumeHystersisHI;
UI_16 TidalVolumeHystersisHI;
BOOL enable_TidalVolumeHystersisLOW;
UI_16 TidalVolumeHystersisLOW;
BOOL enable_MinuteVolumeSetpointPreAlarmHI;
UI_16 MinuteVolumeSetpointPreAlarmHI;
BOOL enable_MinuteVolumeSetpointAlarmHI;
UI_16 MinuteVolumeSetpointAlarmHI;
BOOL enable_MinuteVolumeSetpointPreAlarmLOW;
UI_16 MinuteVolumeSetpointPreAlarmLOW;
BOOL enable_MinuteVolumeSetpointAlarmLOW;
UI_16 MinuteVolumeSetpointAlarmLOW;
BOOL enable_MinuteVolumeHysteresisHI;
UI_16 MinuteVolumeHysteresisHI;
BOOL enable_MinuteVolumeHysteresisLOW;
UI_16 MinuteVolumeHysteresisLOW;
BOOL enable_TriggeVolumeSetpointPreAlarmHI;
UI_16 TriggeVolumeSetpointPreAlarmHI;
BOOL enable_TriggerVolumeSetpointAlarmHI;
UI_16 TriggerVolumeSetpointAlarmHI;
BOOL enable_TriggerVolumeSetpointPreAlarmLOW;
UI_16 TriggerVolumeSetpointPreAlarmLOW;
BOOL enable_TriggerVolumeSetpointAlarmLOW;
UI_16 TriggerVolumeSetpointAlarmLOW;
BOOL enable_TriggerVolumeHysteresisHI;
UI_16 TriggerVolumeHysteresisHI;
BOOL enable_TriggerVolumeHysteresisLOW;
UI_16 TriggerVolumeHysteresisLOW;
BOOL enable_BatterySetpointPreAlarmHI;
UI_16 BatterySetpointPreAlarmHI;
BOOL enable_BatterySetpointAlarmHI;
UI_16 BatterySetpointAlarmHI;
BOOL enable_BatterySetpointPreAlarmLOW;
UI_16 BatterySetpointPreAlarmLOW;
BOOL enable_BatterySetpointAlarmLOW;
UI_16 BatterySetpointAlarmLOW;
BOOL enable_BatterySetpointHysteresisHI;
UI_16 BatterySetpointHysteresisHI;
BOOL enable_BatterySetpointHysteresisLOW;
UI_16 BatterySetpointHysteresisLOW;
BOOL enable_Frecuency;
UI_16 Frecuency;
BOOL enable_Duty_Cycle;
UI_16 Duty_Cycle;
BOOL enable_HOME_Position;
UI_16 HOME_Position;
BOOL enable_ABSOLUTE_Position;
UI_16 ABSOLUTE_Position;
BOOL enable_ABSOLUTE_Velocity;
UI_16 ABSOLUTE_Velocity;
BOOL enable_RELATIVE_Distance;
UI_16 RELATIVE_Distance;
BOOL enable_RELATIVE_Velocity;
UI_16 RELATIVE_Velocity;
BOOL enable_Velocity_Velocity;
UI_16 Velocity_Velocity;
BOOL enable_JOG_Velocity;
UI_16 JOG_Velocity;
BOOL enable_EmergencyStop;
BOOL EmergencyStop;
BOOL enable_TurnAxisHwHi;
BOOL TurnAxisHwHi;
BOOL enable_BeaconLightRed;
BOOL BeaconLightRed;
BOOL enable_BeaconLightOrange;
BOOL BeaconLightOrange;
BOOL enable_TurnAxisPulse;
BOOL TurnAxisPulse;
BOOL enable_TurnAxisDir;
BOOL TurnAxisDir;
BOOL enable_TurnAxisEnable;
BOOL TurnAxisEnable;
BOOL enable_BeaconLightGreen;
BOOL BeaconLightGreen;
BOOL enable_Buzzer;
BOOL Buzzer;
BOOL enable_PeepValve;
t_pwm PeepValve;
BOOL enable_TurnAxisHwLow;
BOOL TurnAxisHwLow;
BOOL enable_ServoPend;
BOOL ServoPend;
BOOL enable_ServoAlarm;
BOOL ServoAlarm;
BOOL enable_AtmosfericPressure;
uint16_t AtmosfericPressure;
BOOL enable_DiferentialPressure;
uint16_t DiferentialPressure;
// (null) No diag variables for SerTX
// (null) No diag variables for SerRX
// (null) No diag variables for EmgcyButton
BOOL enable_ApiVersion;
UI_8 ApiVersion;
BOOL enable_MachineUuid;
t_machineuuid MachineUuid;
BOOL enable_FirmwareVersion;
UI_16 FirmwareVersion;
BOOL enable_Uptime15mCounter;
UI_16 Uptime15mCounter;
BOOL enable_MutedAlarmSeconds;
UI_8 MutedAlarmSeconds;
BOOL enable_CycleRpmLast30s;
UI_16 CycleRpmLast30s;
BOOL enable_CycleIndications;
t_cycleindications CycleIndications;
BOOL enable_CycleDuration;
UI_16 CycleDuration;
BOOL enable_VentilationFlags;
t_ventilationflags VentilationFlags;
BOOL enable_VolumeSetting;
UI_16 VolumeSetting;
BOOL enable_RpmSetting;
UI_16 RpmSetting;
BOOL enable_RampDegreesSetting;
UI_8 RampDegreesSetting;
BOOL enable_EiRatioSetting;
UI_8 EiRatioSetting;
BOOL enable_RecruitTimer;
UI_8 RecruitTimer;
BOOL enable_IncomingFrame;
t_incomingframe IncomingFrame;
BOOL enable_OutgoingFrame;
t_outgoingframe OutgoingFrame;
// (null) No diag variables for Display
// (null) No diag variables for DisplayPlus
// (null) No diag variables for DisplayButtons
BOOL enable_FlowPublic;
UI_16 FlowPublic;
BOOL enable_Power_EN;
BOOL Power_EN;
BOOL enable_Power_Axis;
BOOL Power_Axis;
BOOL enable_Power_ENO;
BOOL Power_ENO;
BOOL enable_Reset_EN;
BOOL Reset_EN;
BOOL enable_Reset_Axis;
BOOL Reset_Axis;
BOOL enable_Reset_ENO;
BOOL Reset_ENO;
BOOL enable_Home_EN;
BOOL Home_EN;
BOOL enable_Home_Axis;
BOOL Home_Axis;
BOOL enable_Home_ENO;
BOOL Home_ENO;
BOOL enable_Halt_EN;
BOOL Halt_EN;
BOOL enable_Halt_Axis;
BOOL Halt_Axis;
BOOL enable_Halt_ENO;
BOOL Halt_ENO;
BOOL enable_Absolute_EN;
BOOL Absolute_EN;
BOOL enable_Absolute_Axis;
BOOL Absolute_Axis;
BOOL enable_Absolute_ENO;
BOOL Absolute_ENO;
BOOL enable_Relative_EN;
BOOL Relative_EN;
BOOL enable_Relative_Axis;
BOOL Relative_Axis;
BOOL enable_Relative_ENO;
BOOL Relative_ENO;
BOOL enable_Velocity_EN;
BOOL Velocity_EN;
BOOL enable_Velocity_Axis;
BOOL Velocity_Axis;
BOOL enable_Velocity_ENO;
BOOL Velocity_ENO;
BOOL enable_Jog_EN;
BOOL Jog_EN;
BOOL enable_Jog_Axis;
BOOL Jog_Axis;
BOOL enable_Jog_ENO;
BOOL Jog_ENO;
BOOL enable_EN;
BOOL EN;
BOOL enable_EN0;
BOOL EN0;
BOOL enable_CycleStartTime;
UI_32 CycleStartTime;

} t_diag;

// Initialization functions

  
// STATUS_LED flow acquisition
// (setup input disabled for LED type);
// STATUS_LED flow synthesis
// (output disabled for LED type);
  
// statusLED flow acquisition
// (setup input disabled for DO type);
// statusLED flow synthesis
void setup_statusLED_output(void);
  
// dbgLedBlinkTimer flow acquisition
// (setup input disabled for Variable type);
// dbgLedBlinkTimer flow synthesis
// (output disabled for Variable type);
  
// DBG_LED flow acquisition
// (setup input disabled for Variable type);
// DBG_LED flow synthesis
// (output disabled for Variable type);
  
// SDA flow acquisition
// (setup input disabled for Bus type);
// SDA flow synthesis
// (output disabled for Bus type);
  
// SCL flow acquisition
// (setup input disabled for Bus type);
// SCL flow synthesis
// (output disabled for Bus type);
  
// ResetRelay flow acquisition
// (setup input disabled for DO type);
// ResetRelay flow synthesis
void setup_ResetRelay_output(void);
  
// LCD_DB7 flow acquisition
// (setup input disabled for DO type);
// LCD_DB7 flow synthesis
void setup_LCD_DB7_output(void);
  
// LCD_DB6 flow acquisition
// (setup input disabled for DO type);
// LCD_DB6 flow synthesis
void setup_LCD_DB6_output(void);
  
// LCD_DB5 flow acquisition
// (setup input disabled for DO type);
// LCD_DB5 flow synthesis
void setup_LCD_DB5_output(void);
  
// LCD_DB4 flow acquisition
// (setup input disabled for DO type);
// LCD_DB4 flow synthesis
void setup_LCD_DB4_output(void);
  
// LCD_DB3 flow acquisition
// (setup input disabled for DO type);
// LCD_DB3 flow synthesis
void setup_LCD_DB3_output(void);
  
// LCD_DB2 flow acquisition
// (setup input disabled for DO type);
// LCD_DB2 flow synthesis
void setup_LCD_DB2_output(void);
  
// LCD_DB1 flow acquisition
// (setup input disabled for DO type);
// LCD_DB1 flow synthesis
void setup_LCD_DB1_output(void);
  
// LCD_DB0 flow acquisition
// (setup input disabled for DO type);
// LCD_DB0 flow synthesis
void setup_LCD_DB0_output(void);
  
// BME_CS2 flow acquisition
// (setup input disabled for Bus type);
// BME_CS2 flow synthesis
// (output disabled for Bus type);
  
// BME_MISO flow acquisition
// (setup input disabled for Bus type);
// BME_MISO flow synthesis
// (output disabled for Bus type);
  
// BME_MOSI flow acquisition
// (setup input disabled for Bus type);
// BME_MOSI flow synthesis
// (output disabled for Bus type);
  
// BME_SCK flow acquisition
// (setup input disabled for Bus type);
// BME_SCK flow synthesis
// (output disabled for Bus type);
  
// BME_CS1 flow acquisition
// (setup input disabled for Bus type);
// BME_CS1 flow synthesis
// (output disabled for Bus type);
  
// _5V flow acquisition
// (setup input disabled for Power type);
// _5V flow synthesis
// (output disabled for Power type);
  
// GND flow acquisition
// (setup input disabled for Power type);
// GND flow synthesis
// (output disabled for Power type);
  
// INT_FS flow acquisition
// (setup input disabled for Variable type);
// INT_FS flow synthesis
// (output disabled for Variable type);
  
// INT_FC flow acquisition
// (setup input disabled for Variable type);
// INT_FC flow synthesis
// (output disabled for Variable type);
  
// INT_HS flow acquisition
// (setup input disabled for Variable type);
// INT_HS flow synthesis
// (output disabled for Variable type);
  
// INT_HC flow acquisition
// (setup input disabled for Variable type);
// INT_HC flow synthesis
// (output disabled for Variable type);
  
// SystemDate_W flow acquisition
// (setup input disabled for Variable type);
// SystemDate_W flow synthesis
// (output disabled for Variable type);
  
// SystemTime_W flow acquisition
// (setup input disabled for Variable type);
// SystemTime_W flow synthesis
// (output disabled for Variable type);
  
// PeakFactor flow acquisition
// (setup input disabled for Variable type);
// PeakFactor flow synthesis
// (output disabled for Variable type);
  
// PlateauFactor flow acquisition
// (setup input disabled for Variable type);
// PlateauFactor flow synthesis
// (output disabled for Variable type);
  
// PeepFactor flow acquisition
// (setup input disabled for Variable type);
// PeepFactor flow synthesis
// (output disabled for Variable type);
  
// FrequencyFactor flow acquisition
// (setup input disabled for Variable type);
// FrequencyFactor flow synthesis
// (output disabled for Variable type);
  
// TidalVolumenFactor flow acquisition
// (setup input disabled for Variable type);
// TidalVolumenFactor flow synthesis
// (output disabled for Variable type);
  
// MinimumVolumeFactor flow acquisition
// (setup input disabled for Variable type);
// MinimumVolumeFactor flow synthesis
// (output disabled for Variable type);
  
// TriggerFactor flow acquisition
// (setup input disabled for Variable type);
// TriggerFactor flow synthesis
// (output disabled for Variable type);
  
// BatteryLevelTrigger flow acquisition
// (setup input disabled for Variable type);
// BatteryLevelTrigger flow synthesis
// (output disabled for Variable type);
  
// PatientType flow acquisition
// (setup input disabled for Variable type);
// PatientType flow synthesis
// (output disabled for Variable type);
  
// PatientWeight flow acquisition
// (setup input disabled for Variable type);
// PatientWeight flow synthesis
// (output disabled for Variable type);
  
// PatientHeight flow acquisition
// (setup input disabled for Variable type);
// PatientHeight flow synthesis
// (output disabled for Variable type);
  
// TidalVolume flow acquisition
// (setup input disabled for Variable type);
// TidalVolume flow synthesis
// (output disabled for Variable type);
  
// PIDMode flow acquisition
// (setup input disabled for Variable type);
// PIDMode flow synthesis
// (output disabled for Variable type);
  
// PIDState flow acquisition
// (setup input disabled for Variable type);
// PIDState flow synthesis
// (output disabled for Variable type);
  
// PIDErrorBits flow acquisition
// (setup input disabled for Variable type);
// PIDErrorBits flow synthesis
// (output disabled for Variable type);
  
// Alarms flow acquisition
// (setup input disabled for Variable type);
// Alarms flow synthesis
// (output disabled for Variable type);
  
// Status flow acquisition
// (setup input disabled for Variable type);
// Status flow synthesis
// (output disabled for Variable type);
  
// POWER_StartMode flow acquisition
// (setup input disabled for Variable type);
// POWER_StartMode flow synthesis
// (output disabled for Variable type);
  
// POWER_StopMode flow acquisition
// (setup input disabled for Variable type);
// POWER_StopMode flow synthesis
// (output disabled for Variable type);
  
// HOME_Mode flow acquisition
// (setup input disabled for Variable type);
// HOME_Mode flow synthesis
// (output disabled for Variable type);
  
// SetWriteData flow acquisition
// (setup input disabled for BOOL type);
// SetWriteData flow synthesis
// (output disabled for BOOL type);
  
// SetTime flow acquisition
// (setup input disabled for BOOL type);
// SetTime flow synthesis
// (output disabled for BOOL type);
  
// Trigger flow acquisition
// (setup input disabled for BOOL type);
// Trigger flow synthesis
// (output disabled for BOOL type);
  
// PeakPBPlus flow acquisition
// (setup input disabled for BOOL type);
// PeakPBPlus flow synthesis
// (output disabled for BOOL type);
  
// PeakPBMinus flow acquisition
// (setup input disabled for BOOL type);
// PeakPBMinus flow synthesis
// (output disabled for BOOL type);
  
// PeepPBPlus flow acquisition
// (setup input disabled for BOOL type);
// PeepPBPlus flow synthesis
// (output disabled for BOOL type);
  
// PeepPBMinus flow acquisition
// (setup input disabled for BOOL type);
// PeepPBMinus flow synthesis
// (output disabled for BOOL type);
  
// FrPBPlus flow acquisition
// (setup input disabled for BOOL type);
// FrPBPlus flow synthesis
// (output disabled for BOOL type);
  
// FrPBMinus flow acquisition
// (setup input disabled for BOOL type);
// FrPBMinus flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentOn flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentOn flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentOff flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentOff flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentMem flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentMem flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentRunning flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentRunning flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentEnding flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentEnding flow synthesis
// (output disabled for BOOL type);
  
// RecruitmentEnd flow acquisition
// (setup input disabled for BOOL type);
// RecruitmentEnd flow synthesis
// (output disabled for BOOL type);
  
// ErrorAck flow acquisition
// (setup input disabled for BOOL type);
// ErrorAck flow synthesis
// (output disabled for BOOL type);
  
// Reset flow acquisition
// (setup input disabled for BOOL type);
// Reset flow synthesis
// (output disabled for BOOL type);
  
// PIDError flow acquisition
// (setup input disabled for BOOL type);
// PIDError flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeakDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeakDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeakDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeakDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeakDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeakInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeakInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeakInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeakInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeakInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PlateauDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PlateauDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PlateauDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PlateauDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PlateauInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PlateauInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PlateauInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PlateauInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PlateauInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeepDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeepDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeepDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeepDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeepInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// PeepInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeepInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// PeepInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// PeepInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// FrequencyDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// FrequencyDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// FrequencyDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// FrequencyDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// FrequencyInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// FrequencyInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// FrequencyInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// FrequencyInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// FrequencyInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TidalVolumeInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TidalVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// MinuteVolumeInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// MinuteVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TriggerDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TriggerDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TriggerDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TriggerDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TriggerInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// TriggerInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TriggerInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// TriggerInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// TriggerInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// BatteryDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// BatteryDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// BatteryDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// BatteryDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// BatteryInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type);
// BatteryInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// BatteryInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// BatteryInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type);
// BatteryInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
  
// Enable flow acquisition
// (setup input disabled for BOOL type);
// Enable flow synthesis
// (output disabled for BOOL type);
  
// Busy flow acquisition
// (setup input disabled for BOOL type);
// Busy flow synthesis
// (output disabled for BOOL type);
  
// POWER_Enable flow acquisition
// (setup input disabled for BOOL type);
// POWER_Enable flow synthesis
// (output disabled for BOOL type);
  
// POWER_Status flow acquisition
// (setup input disabled for BOOL type);
// POWER_Status flow synthesis
// (output disabled for BOOL type);
  
// POWER_Error flow acquisition
// (setup input disabled for BOOL type);
// POWER_Error flow synthesis
// (output disabled for BOOL type);
  
// RESET_Execute flow acquisition
// (setup input disabled for BOOL type);
// RESET_Execute flow synthesis
// (output disabled for BOOL type);
  
// RESET_Done flow acquisition
// (setup input disabled for BOOL type);
// RESET_Done flow synthesis
// (output disabled for BOOL type);
  
// RESET_Error flow acquisition
// (setup input disabled for BOOL type);
// RESET_Error flow synthesis
// (output disabled for BOOL type);
  
// HOME_Execute flow acquisition
// (setup input disabled for BOOL type);
// HOME_Execute flow synthesis
// (output disabled for BOOL type);
  
// HOME_Done flow acquisition
// (setup input disabled for BOOL type);
// HOME_Done flow synthesis
// (output disabled for BOOL type);
  
// HOME_Error flow acquisition
// (setup input disabled for BOOL type);
// HOME_Error flow synthesis
// (output disabled for BOOL type);
  
// HALT_Execute flow acquisition
// (setup input disabled for BOOL type);
// HALT_Execute flow synthesis
// (output disabled for BOOL type);
  
// HALT_Done flow acquisition
// (setup input disabled for BOOL type);
// HALT_Done flow synthesis
// (output disabled for BOOL type);
  
// HALT_Error flow acquisition
// (setup input disabled for BOOL type);
// HALT_Error flow synthesis
// (output disabled for BOOL type);
  
// ABSOLUTE_Execute flow acquisition
// (setup input disabled for BOOL type);
// ABSOLUTE_Execute flow synthesis
// (output disabled for BOOL type);
  
// ABSOLUTE_Done flow acquisition
// (setup input disabled for BOOL type);
// ABSOLUTE_Done flow synthesis
// (output disabled for BOOL type);
  
// ABSOLUTE_Error flow acquisition
// (setup input disabled for BOOL type);
// ABSOLUTE_Error flow synthesis
// (output disabled for BOOL type);
  
// RELATIVE_Execute flow acquisition
// (setup input disabled for BOOL type);
// RELATIVE_Execute flow synthesis
// (output disabled for BOOL type);
  
// RELATIVE_Done flow acquisition
// (setup input disabled for BOOL type);
// RELATIVE_Done flow synthesis
// (output disabled for BOOL type);
  
// RELATIVE_Error flow acquisition
// (setup input disabled for BOOL type);
// RELATIVE_Error flow synthesis
// (output disabled for BOOL type);
  
// Velocity_Execute flow acquisition
// (setup input disabled for BOOL type);
// Velocity_Execute flow synthesis
// (output disabled for BOOL type);
  
// Velocity_Current flow acquisition
// (setup input disabled for BOOL type);
// Velocity_Current flow synthesis
// (output disabled for BOOL type);
  
// Velocity_invelocity flow acquisition
// (setup input disabled for BOOL type);
// Velocity_invelocity flow synthesis
// (output disabled for BOOL type);
  
// Velocity_Error flow acquisition
// (setup input disabled for BOOL type);
// Velocity_Error flow synthesis
// (output disabled for BOOL type);
  
// JOG_Forward flow acquisition
// (setup input disabled for BOOL type);
// JOG_Forward flow synthesis
// (output disabled for BOOL type);
  
// JOG_Backward flow acquisition
// (setup input disabled for BOOL type);
// JOG_Backward flow synthesis
// (output disabled for BOOL type);
  
// JOG_Invelocity flow acquisition
// (setup input disabled for BOOL type);
// JOG_Invelocity flow synthesis
// (output disabled for BOOL type);
  
// JOG_Error flow acquisition
// (setup input disabled for BOOL type);
// JOG_Error flow synthesis
// (output disabled for BOOL type);
  
// Permissions flow acquisition
// (setup input disabled for BOOL type);
// Permissions flow synthesis
// (output disabled for BOOL type);
  
// Operation_ON flow acquisition
// (setup input disabled for BOOL type);
// Operation_ON flow synthesis
// (output disabled for BOOL type);
  
// Emergency flow acquisition
// (setup input disabled for BOOL type);
// Emergency flow synthesis
// (output disabled for BOOL type);
  
// GeneralAck flow acquisition
// (setup input disabled for BOOL type);
// GeneralAck flow synthesis
// (output disabled for BOOL type);
  
// MotorAck flow acquisition
// (setup input disabled for BOOL type);
// MotorAck flow synthesis
// (output disabled for BOOL type);
  
// ProcessAck flow acquisition
// (setup input disabled for BOOL type);
// ProcessAck flow synthesis
// (output disabled for BOOL type);
  
// GeneralReset flow acquisition
// (setup input disabled for BOOL type);
// GeneralReset flow synthesis
// (output disabled for BOOL type);
  
// MotorReset flow acquisition
// (setup input disabled for BOOL type);
// MotorReset flow synthesis
// (output disabled for BOOL type);
  
// ProcessReset flow acquisition
// (setup input disabled for BOOL type);
// ProcessReset flow synthesis
// (output disabled for BOOL type);
  
// PeakValue flow acquisition
// (setup input disabled for UI_16 type);
// PeakValue flow synthesis
// (output disabled for UI_16 type);
  
// PlateauValue flow acquisition
// (setup input disabled for UI_16 type);
// PlateauValue flow synthesis
// (output disabled for UI_16 type);
  
// PeepValue flow acquisition
// (setup input disabled for UI_16 type);
// PeepValue flow synthesis
// (output disabled for UI_16 type);
  
// FrequencyValue flow acquisition
// (setup input disabled for UI_16 type);
// FrequencyValue flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeValue flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeValue flow synthesis
// (output disabled for UI_16 type);
  
// MinumValue flow acquisition
// (setup input disabled for UI_16 type);
// MinumValue flow synthesis
// (output disabled for UI_16 type);
  
// DrivingPreassure flow acquisition
// (setup input disabled for UI_16 type);
// DrivingPreassure flow synthesis
// (output disabled for UI_16 type);
  
// BatteryVoltage flow acquisition
// (setup input disabled for UI_16 type);
// BatteryVoltage flow synthesis
// (output disabled for UI_16 type);
  
// BatteryCapacity flow acquisition
// (setup input disabled for UI_16 type);
// BatteryCapacity flow synthesis
// (output disabled for UI_16 type);
  
// Pressure1 flow acquisition
// (setup input disabled for UI_16 type);
// Pressure1 flow synthesis
// (output disabled for UI_16 type);
  
// Pressure2 flow acquisition
// (setup input disabled for UI_16 type);
// Pressure2 flow synthesis
// (output disabled for UI_16 type);
  
// VolumeAccumulated flow acquisition
// (setup input disabled for UI_16 type);
// VolumeAccumulated flow synthesis
// (output disabled for UI_16 type);
  
// Flow flow acquisition
// (setup input disabled for UI_16 type);
// Flow flow synthesis
// (output disabled for UI_16 type);
  
// SystemDateRetVal flow acquisition
// (setup input disabled for UI_16 type);
// SystemDateRetVal flow synthesis
// (output disabled for UI_16 type);
  
// SystemDateOut flow acquisition
// (setup input disabled for DateTime type);
// SystemDateOut flow synthesis
// (output disabled for DateTime type);
  
// LocalDateRetVal flow acquisition
// (setup input disabled for UI_16 type);
// LocalDateRetVal flow synthesis
// (output disabled for UI_16 type);
  
// LocalDateOut flow acquisition
// (setup input disabled for DateTime type);
// LocalDateOut flow synthesis
// (output disabled for DateTime type);
  
// WriteDateTime flow acquisition
// (setup input disabled for DateTime type);
// WriteDateTime flow synthesis
// (output disabled for DateTime type);
  
// WriteDateOut flow acquisition
// (setup input disabled for DateTime type);
// WriteDateOut flow synthesis
// (output disabled for DateTime type);
  
// StatuSetTime flow acquisition
// (setup input disabled for DateTime type);
// StatuSetTime flow synthesis
// (output disabled for DateTime type);
  
// PeakMax flow acquisition
// (setup input disabled for UI_16 type);
// PeakMax flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PeakMin flow acquisition
// (setup input disabled for UI_16 type);
// PeakMin flow synthesis
// (output disabled for UI_16 type);
  
// PlateauMax flow acquisition
// (setup input disabled for UI_16 type);
// PlateauMax flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// PlateauSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PlateauMin flow acquisition
// (setup input disabled for UI_16 type);
// PlateauMin flow synthesis
// (output disabled for UI_16 type);
  
// PeepMax flow acquisition
// (setup input disabled for UI_16 type);
// PeepMax flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PeepMin flow acquisition
// (setup input disabled for UI_16 type);
// PeepMin flow synthesis
// (output disabled for UI_16 type);
  
// FrequencyMax flow acquisition
// (setup input disabled for UI_16 type);
// FrequencyMax flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpoint flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpoint flow synthesis
// (output disabled for UI_16 type);
  
// FrequencyMin flow acquisition
// (setup input disabled for UI_16 type);
// FrequencyMin flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumenMax flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumenMax flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumenSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumenSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumenMin flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumenMin flow synthesis
// (output disabled for UI_16 type);
  
// MinimumVolumeMax flow acquisition
// (setup input disabled for UI_16 type);
// MinimumVolumeMax flow synthesis
// (output disabled for UI_16 type);
  
// MinimumVolumeSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// MinimumVolumeSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// MinimumVolumeMin flow acquisition
// (setup input disabled for UI_16 type);
// MinimumVolumeMin flow synthesis
// (output disabled for UI_16 type);
  
// TriggerSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// TriggerSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// RecruitmentTimer flow acquisition
// (setup input disabled for TimeUI_16 type);
// RecruitmentTimer flow synthesis
// (output disabled for TimeUI_16 type);
  
// RecruitmentElap flow acquisition
// (setup input disabled for TimeUI_16 type);
// RecruitmentElap flow synthesis
// (output disabled for TimeUI_16 type);
  
// TimeStart flow acquisition
// (setup input disabled for DateTime type);
// TimeStart flow synthesis
// (output disabled for DateTime type);
  
// TimeEnd flow acquisition
// (setup input disabled for DateTime type);
// TimeEnd flow synthesis
// (output disabled for DateTime type);
  
// FlowSetpoint flow acquisition
// (setup input disabled for UI_16 type);
// FlowSetpoint flow synthesis
// (output disabled for UI_16 type);
  
// PIDOutReal flow acquisition
// (setup input disabled for FL_16 type);
// PIDOutReal flow synthesis
// (output disabled for FL_16 type);
  
// PIDOutInt flow acquisition
// (setup input disabled for UI_16 type);
// PIDOutInt flow synthesis
// (output disabled for UI_16 type);
  
// PIDOutPWM flow acquisition
// (setup input disabled for UI_16 type);
// PIDOutPWM flow synthesis
// (output disabled for UI_16 type);
  
// PIDOutMotor flow acquisition
// (setup input disabled for UI_16 type);
// PIDOutMotor flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// PeakSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeakSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PlateauSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PlateauSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PlateauSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PlateauSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PlateaukSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PlateaukSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PlateaukSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// PlateaukSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// PlateaukSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// PlateaukSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// PeepSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// PeepSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// FrequencySetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// FrequencySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeHystersisHI flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeHystersisHI flow synthesis
// (output disabled for UI_16 type);
  
// TidalVolumeHystersisLOW flow acquisition
// (setup input disabled for UI_16 type);
// TidalVolumeHystersisLOW flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// MinuteVolumeHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// MinuteVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// TriggeVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// TriggeVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// TriggerVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// TriggerVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// TriggerVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// TriggerVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// TriggerVolumeHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// TriggerVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
  
// BatterySetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type);
// BatterySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
  
// Frecuency flow acquisition
// (setup input disabled for UI_16 type);
// Frecuency flow synthesis
// (output disabled for UI_16 type);
  
// Duty_Cycle flow acquisition
// (setup input disabled for UI_16 type);
// Duty_Cycle flow synthesis
// (output disabled for UI_16 type);
  
// HOME_Position flow acquisition
// (setup input disabled for UI_16 type);
// HOME_Position flow synthesis
// (output disabled for UI_16 type);
  
// ABSOLUTE_Position flow acquisition
// (setup input disabled for UI_16 type);
// ABSOLUTE_Position flow synthesis
// (output disabled for UI_16 type);
  
// ABSOLUTE_Velocity flow acquisition
// (setup input disabled for UI_16 type);
// ABSOLUTE_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// RELATIVE_Distance flow acquisition
// (setup input disabled for UI_16 type);
// RELATIVE_Distance flow synthesis
// (output disabled for UI_16 type);
  
// RELATIVE_Velocity flow acquisition
// (setup input disabled for UI_16 type);
// RELATIVE_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// Velocity_Velocity flow acquisition
// (setup input disabled for UI_16 type);
// Velocity_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// JOG_Velocity flow acquisition
// (setup input disabled for UI_16 type);
// JOG_Velocity flow synthesis
// (output disabled for UI_16 type);
  
// EmergencyStop flow acquisition
void setup_EmergencyStop(void);
// EmergencyStop flow synthesis
// (output disabled for DI type);
  
// TurnAxisHwHi flow acquisition
void setup_TurnAxisHwHi(void);
// TurnAxisHwHi flow synthesis
// (output disabled for DI_pu type);
  
// BeaconLightRed flow acquisition
// (setup input disabled for DO type);
// BeaconLightRed flow synthesis
void setup_BeaconLightRed_output(void);
  
// BeaconLightOrange flow acquisition
// (setup input disabled for DO type);
// BeaconLightOrange flow synthesis
void setup_BeaconLightOrange_output(void);
  
// TurnAxisPulse flow acquisition
// (setup input disabled for DO type);
// TurnAxisPulse flow synthesis
void setup_TurnAxisPulse_output(void);
  
// TurnAxisDir flow acquisition
// (setup input disabled for DO type);
// TurnAxisDir flow synthesis
void setup_TurnAxisDir_output(void);
  
// TurnAxisEnable flow acquisition
// (setup input disabled for DO type);
// TurnAxisEnable flow synthesis
void setup_TurnAxisEnable_output(void);
  
// BeaconLightGreen flow acquisition
// (setup input disabled for DO type);
// BeaconLightGreen flow synthesis
void setup_BeaconLightGreen_output(void);
  
// Buzzer flow acquisition
// (setup input disabled for DO type);
// Buzzer flow synthesis
void setup_Buzzer_output(void);
  
// PeepValve flow acquisition
// (setup input disabled for PWM type);
// PeepValve flow synthesis
void setup_PeepValve(void);
  
// TurnAxisHwLow flow acquisition
void setup_TurnAxisHwLow(void);
// TurnAxisHwLow flow synthesis
// (output disabled for DI_pu type);
  
// ServoPend flow acquisition
void setup_ServoPend(void);
// ServoPend flow synthesis
// (output disabled for DI type);
  
// ServoAlarm flow acquisition
void setup_ServoAlarm(void);
// ServoAlarm flow synthesis
// (output disabled for DI_pu type);
  
// AtmosfericPressure flow acquisition
void setup_AtmosfericPressure(void);
// AtmosfericPressure flow synthesis
// (output disabled for ADC type);
  
// DiferentialPressure flow acquisition
void setup_DiferentialPressure(void);
// DiferentialPressure flow synthesis
// (output disabled for ADC type);
  
// SerTX flow acquisition
// (setup input disabled for Bus type);
// SerTX flow synthesis
// (output disabled for Bus type);
  
// SerRX flow acquisition
// (setup input disabled for Bus type);
// SerRX flow synthesis
// (output disabled for Bus type);
  
// EmgcyButton flow acquisition
// (setup input disabled for TBD type);
// EmgcyButton flow synthesis
// (output disabled for TBD type);
  
// ApiVersion flow acquisition
// (setup input disabled for UI_8 type);
// ApiVersion flow synthesis
// (output disabled for UI_8 type);
  
// MachineUuid flow acquisition
// (setup input disabled for Variable type);
// MachineUuid flow synthesis
// (output disabled for Variable type);
  
// FirmwareVersion flow acquisition
// (setup input disabled for UI_16 type);
// FirmwareVersion flow synthesis
// (output disabled for UI_16 type);
  
// Uptime15mCounter flow acquisition
// (setup input disabled for UI_16 type);
// Uptime15mCounter flow synthesis
// (output disabled for UI_16 type);
  
// MutedAlarmSeconds flow acquisition
// (setup input disabled for UI_8 type);
// MutedAlarmSeconds flow synthesis
// (output disabled for UI_8 type);
  
// CycleRpmLast30s flow acquisition
// (setup input disabled for UI_16 type);
// CycleRpmLast30s flow synthesis
// (output disabled for UI_16 type);
  
// CycleIndications flow acquisition
// (setup input disabled for Variable type);
// CycleIndications flow synthesis
// (output disabled for Variable type);
  
// CycleDuration flow acquisition
// (setup input disabled for TimeUI_16 type);
// CycleDuration flow synthesis
// (output disabled for TimeUI_16 type);
  
// VentilationFlags flow acquisition
// (setup input disabled for Variable type);
// VentilationFlags flow synthesis
// (output disabled for Variable type);
  
// VolumeSetting flow acquisition
// (setup input disabled for UI_16 type);
// VolumeSetting flow synthesis
// (output disabled for UI_16 type);
  
// RpmSetting flow acquisition
// (setup input disabled for UI_16 type);
// RpmSetting flow synthesis
// (output disabled for UI_16 type);
  
// RampDegreesSetting flow acquisition
// (setup input disabled for UI_8 type);
// RampDegreesSetting flow synthesis
// (output disabled for UI_8 type);
  
// EiRatioSetting flow acquisition
// (setup input disabled for UI_8 type);
// EiRatioSetting flow synthesis
// (output disabled for UI_8 type);
  
// RecruitTimer flow acquisition
// (setup input disabled for UI_8 type);
// RecruitTimer flow synthesis
// (output disabled for UI_8 type);
  
// IncomingFrame flow acquisition
// (setup input disabled for Variable type);
// IncomingFrame flow synthesis
// (output disabled for Variable type);
  
// OutgoingFrame flow acquisition
// (setup input disabled for Variable type);
// OutgoingFrame flow synthesis
// (output disabled for Variable type);
  
// Display flow acquisition
// (setup input disabled for Display type);
// Display flow synthesis
// (output disabled for Display type);
  
// DisplayPlus flow acquisition
// (setup input disabled for Display type);
// DisplayPlus flow synthesis
// (output disabled for Display type);
  
// DisplayButtons flow acquisition
// (setup input disabled for Keyboard type);
// DisplayButtons flow synthesis
// (output disabled for Keyboard type);
  
// FlowPublic flow acquisition
// (setup input disabled for UI_16 type);
// FlowPublic flow synthesis
// (output disabled for UI_16 type);
  
// Power_EN flow acquisition
// (setup input disabled for BOOL type);
// Power_EN flow synthesis
// (output disabled for BOOL type);
  
// Power_Axis flow acquisition
// (setup input disabled for BOOL type);
// Power_Axis flow synthesis
// (output disabled for BOOL type);
  
// Power_ENO flow acquisition
// (setup input disabled for BOOL type);
// Power_ENO flow synthesis
// (output disabled for BOOL type);
  
// Reset_EN flow acquisition
// (setup input disabled for BOOL type);
// Reset_EN flow synthesis
// (output disabled for BOOL type);
  
// Reset_Axis flow acquisition
// (setup input disabled for BOOL type);
// Reset_Axis flow synthesis
// (output disabled for BOOL type);
  
// Reset_ENO flow acquisition
// (setup input disabled for BOOL type);
// Reset_ENO flow synthesis
// (output disabled for BOOL type);
  
// Home_EN flow acquisition
// (setup input disabled for BOOL type);
// Home_EN flow synthesis
// (output disabled for BOOL type);
  
// Home_Axis flow acquisition
// (setup input disabled for BOOL type);
// Home_Axis flow synthesis
// (output disabled for BOOL type);
  
// Home_ENO flow acquisition
// (setup input disabled for BOOL type);
// Home_ENO flow synthesis
// (output disabled for BOOL type);
  
// Halt_EN flow acquisition
// (setup input disabled for BOOL type);
// Halt_EN flow synthesis
// (output disabled for BOOL type);
  
// Halt_Axis flow acquisition
// (setup input disabled for BOOL type);
// Halt_Axis flow synthesis
// (output disabled for BOOL type);
  
// Halt_ENO flow acquisition
// (setup input disabled for BOOL type);
// Halt_ENO flow synthesis
// (output disabled for BOOL type);
  
// Absolute_EN flow acquisition
// (setup input disabled for BOOL type);
// Absolute_EN flow synthesis
// (output disabled for BOOL type);
  
// Absolute_Axis flow acquisition
// (setup input disabled for BOOL type);
// Absolute_Axis flow synthesis
// (output disabled for BOOL type);
  
// Absolute_ENO flow acquisition
// (setup input disabled for BOOL type);
// Absolute_ENO flow synthesis
// (output disabled for BOOL type);
  
// Relative_EN flow acquisition
// (setup input disabled for BOOL type);
// Relative_EN flow synthesis
// (output disabled for BOOL type);
  
// Relative_Axis flow acquisition
// (setup input disabled for BOOL type);
// Relative_Axis flow synthesis
// (output disabled for BOOL type);
  
// Relative_ENO flow acquisition
// (setup input disabled for BOOL type);
// Relative_ENO flow synthesis
// (output disabled for BOOL type);
  
// Velocity_EN flow acquisition
// (setup input disabled for BOOL type);
// Velocity_EN flow synthesis
// (output disabled for BOOL type);
  
// Velocity_Axis flow acquisition
// (setup input disabled for BOOL type);
// Velocity_Axis flow synthesis
// (output disabled for BOOL type);
  
// Velocity_ENO flow acquisition
// (setup input disabled for BOOL type);
// Velocity_ENO flow synthesis
// (output disabled for BOOL type);
  
// Jog_EN flow acquisition
// (setup input disabled for BOOL type);
// Jog_EN flow synthesis
// (output disabled for BOOL type);
  
// Jog_Axis flow acquisition
// (setup input disabled for BOOL type);
// Jog_Axis flow synthesis
// (output disabled for BOOL type);
  
// Jog_ENO flow acquisition
// (setup input disabled for BOOL type);
// Jog_ENO flow synthesis
// (output disabled for BOOL type);
  
// EN flow acquisition
// (setup input disabled for BOOL type);
// EN flow synthesis
// (output disabled for BOOL type);
  
// EN0 flow acquisition
// (setup input disabled for BOOL type);
// EN0 flow synthesis
// (output disabled for BOOL type);
  
// CycleStartTime flow acquisition

// CycleStartTime flow synthesis


// Input / Output functions

  
// STATUS_LED flow acquisition
// (input disabled for LED type);
// STATUS_LED flow synthesis
// (output disabled for LED type);
// STATUS_LED getter
// (getter disabled for LED type);
// STATUS_LED setter
// (setter disabled for LED type);
  
// statusLED flow acquisition
// (input disabled for DO type);
// statusLED flow synthesis
void synthesize_statusLED(void);
// statusLED getter
// (getter disabled for DO type);
// statusLED setter

  
// dbgLedBlinkTimer flow acquisition
// (input disabled for Variable type);
// dbgLedBlinkTimer flow synthesis
// (output disabled for Variable type);
// dbgLedBlinkTimer getter
t_dbgledblinktimer get_dbgLedBlinkTimer(void);
// dbgLedBlinkTimer setter
// (setter disabled for Variable type);
  
// DBG_LED flow acquisition
// (input disabled for Variable type);
// DBG_LED flow synthesis
// (output disabled for Variable type);
// DBG_LED getter
t_dbg_led get_DBG_LED(void);
// DBG_LED setter
// (setter disabled for Variable type);
  
// SDA flow acquisition
// (input disabled for Bus type);
// SDA flow synthesis
// (output disabled for Bus type);
// SDA getter
// (getter disabled for Bus type);
// SDA setter
// (setter disabled for Bus type);
  
// SCL flow acquisition
// (input disabled for Bus type);
// SCL flow synthesis
// (output disabled for Bus type);
// SCL getter
// (getter disabled for Bus type);
// SCL setter
// (setter disabled for Bus type);
  
// ResetRelay flow acquisition
// (input disabled for DO type);
// ResetRelay flow synthesis
void synthesize_ResetRelay(void);
// ResetRelay getter
// (getter disabled for DO type);
// ResetRelay setter

  
// LCD_DB7 flow acquisition
// (input disabled for DO type);
// LCD_DB7 flow synthesis
void synthesize_LCD_DB7(void);
// LCD_DB7 getter
// (getter disabled for DO type);
// LCD_DB7 setter

  
// LCD_DB6 flow acquisition
// (input disabled for DO type);
// LCD_DB6 flow synthesis
void synthesize_LCD_DB6(void);
// LCD_DB6 getter
// (getter disabled for DO type);
// LCD_DB6 setter

  
// LCD_DB5 flow acquisition
// (input disabled for DO type);
// LCD_DB5 flow synthesis
void synthesize_LCD_DB5(void);
// LCD_DB5 getter
// (getter disabled for DO type);
// LCD_DB5 setter

  
// LCD_DB4 flow acquisition
// (input disabled for DO type);
// LCD_DB4 flow synthesis
void synthesize_LCD_DB4(void);
// LCD_DB4 getter
// (getter disabled for DO type);
// LCD_DB4 setter

  
// LCD_DB3 flow acquisition
// (input disabled for DO type);
// LCD_DB3 flow synthesis
void synthesize_LCD_DB3(void);
// LCD_DB3 getter
// (getter disabled for DO type);
// LCD_DB3 setter

  
// LCD_DB2 flow acquisition
// (input disabled for DO type);
// LCD_DB2 flow synthesis
void synthesize_LCD_DB2(void);
// LCD_DB2 getter
// (getter disabled for DO type);
// LCD_DB2 setter

  
// LCD_DB1 flow acquisition
// (input disabled for DO type);
// LCD_DB1 flow synthesis
void synthesize_LCD_DB1(void);
// LCD_DB1 getter
// (getter disabled for DO type);
// LCD_DB1 setter

  
// LCD_DB0 flow acquisition
// (input disabled for DO type);
// LCD_DB0 flow synthesis
void synthesize_LCD_DB0(void);
// LCD_DB0 getter
// (getter disabled for DO type);
// LCD_DB0 setter

  
// BME_CS2 flow acquisition
// (input disabled for Bus type);
// BME_CS2 flow synthesis
// (output disabled for Bus type);
// BME_CS2 getter
// (getter disabled for Bus type);
// BME_CS2 setter
// (setter disabled for Bus type);
  
// BME_MISO flow acquisition
// (input disabled for Bus type);
// BME_MISO flow synthesis
// (output disabled for Bus type);
// BME_MISO getter
// (getter disabled for Bus type);
// BME_MISO setter
// (setter disabled for Bus type);
  
// BME_MOSI flow acquisition
// (input disabled for Bus type);
// BME_MOSI flow synthesis
// (output disabled for Bus type);
// BME_MOSI getter
// (getter disabled for Bus type);
// BME_MOSI setter
// (setter disabled for Bus type);
  
// BME_SCK flow acquisition
// (input disabled for Bus type);
// BME_SCK flow synthesis
// (output disabled for Bus type);
// BME_SCK getter
// (getter disabled for Bus type);
// BME_SCK setter
// (setter disabled for Bus type);
  
// BME_CS1 flow acquisition
// (input disabled for Bus type);
// BME_CS1 flow synthesis
// (output disabled for Bus type);
// BME_CS1 getter
// (getter disabled for Bus type);
// BME_CS1 setter
// (setter disabled for Bus type);
  
// _5V flow acquisition
// (input disabled for Power type);
// _5V flow synthesis
// (output disabled for Power type);
// _5V getter
// (getter disabled for Power type);
// _5V setter
// (setter disabled for Power type);
  
// GND flow acquisition
// (input disabled for Power type);
// GND flow synthesis
// (output disabled for Power type);
// GND getter
// (getter disabled for Power type);
// GND setter
// (setter disabled for Power type);
  
// INT_FS flow acquisition
// (input disabled for Variable type);
// INT_FS flow synthesis
// (output disabled for Variable type);
// INT_FS getter
t_int_fs get_INT_FS(void);
// INT_FS setter
// (setter disabled for Variable type);
  
// INT_FC flow acquisition
// (input disabled for Variable type);
// INT_FC flow synthesis
// (output disabled for Variable type);
// INT_FC getter
t_int_fc get_INT_FC(void);
// INT_FC setter
// (setter disabled for Variable type);
  
// INT_HS flow acquisition
// (input disabled for Variable type);
// INT_HS flow synthesis
// (output disabled for Variable type);
// INT_HS getter
t_int_hs get_INT_HS(void);
// INT_HS setter
// (setter disabled for Variable type);
  
// INT_HC flow acquisition
// (input disabled for Variable type);
// INT_HC flow synthesis
// (output disabled for Variable type);
// INT_HC getter
t_int_hc get_INT_HC(void);
// INT_HC setter
// (setter disabled for Variable type);
  
// SystemDate_W flow acquisition
// (input disabled for Variable type);
// SystemDate_W flow synthesis
// (output disabled for Variable type);
// SystemDate_W getter
t_systemdate_w get_SystemDate_W(void);
// SystemDate_W setter
// (setter disabled for Variable type);
  
// SystemTime_W flow acquisition
// (input disabled for Variable type);
// SystemTime_W flow synthesis
// (output disabled for Variable type);
// SystemTime_W getter
t_systemtime_w get_SystemTime_W(void);
// SystemTime_W setter
// (setter disabled for Variable type);
  
// PeakFactor flow acquisition
// (input disabled for Variable type);
// PeakFactor flow synthesis
// (output disabled for Variable type);
// PeakFactor getter
t_peakfactor get_PeakFactor(void);
// PeakFactor setter
// (setter disabled for Variable type);
  
// PlateauFactor flow acquisition
// (input disabled for Variable type);
// PlateauFactor flow synthesis
// (output disabled for Variable type);
// PlateauFactor getter
t_plateaufactor get_PlateauFactor(void);
// PlateauFactor setter
// (setter disabled for Variable type);
  
// PeepFactor flow acquisition
// (input disabled for Variable type);
// PeepFactor flow synthesis
// (output disabled for Variable type);
// PeepFactor getter
t_peepfactor get_PeepFactor(void);
// PeepFactor setter
// (setter disabled for Variable type);
  
// FrequencyFactor flow acquisition
// (input disabled for Variable type);
// FrequencyFactor flow synthesis
// (output disabled for Variable type);
// FrequencyFactor getter
t_frequencyfactor get_FrequencyFactor(void);
// FrequencyFactor setter
// (setter disabled for Variable type);
  
// TidalVolumenFactor flow acquisition
// (input disabled for Variable type);
// TidalVolumenFactor flow synthesis
// (output disabled for Variable type);
// TidalVolumenFactor getter
t_tidalvolumenfactor get_TidalVolumenFactor(void);
// TidalVolumenFactor setter
// (setter disabled for Variable type);
  
// MinimumVolumeFactor flow acquisition
// (input disabled for Variable type);
// MinimumVolumeFactor flow synthesis
// (output disabled for Variable type);
// MinimumVolumeFactor getter
t_minimumvolumefactor get_MinimumVolumeFactor(void);
// MinimumVolumeFactor setter
// (setter disabled for Variable type);
  
// TriggerFactor flow acquisition
// (input disabled for Variable type);
// TriggerFactor flow synthesis
// (output disabled for Variable type);
// TriggerFactor getter
t_triggerfactor get_TriggerFactor(void);
// TriggerFactor setter
// (setter disabled for Variable type);
  
// BatteryLevelTrigger flow acquisition
// (input disabled for Variable type);
// BatteryLevelTrigger flow synthesis
// (output disabled for Variable type);
// BatteryLevelTrigger getter
t_batteryleveltrigger get_BatteryLevelTrigger(void);
// BatteryLevelTrigger setter
// (setter disabled for Variable type);
  
// PatientType flow acquisition
// (input disabled for Variable type);
// PatientType flow synthesis
// (output disabled for Variable type);
// PatientType getter
t_patienttype get_PatientType(void);
// PatientType setter
// (setter disabled for Variable type);
  
// PatientWeight flow acquisition
// (input disabled for Variable type);
// PatientWeight flow synthesis
// (output disabled for Variable type);
// PatientWeight getter
t_patientweight get_PatientWeight(void);
// PatientWeight setter
// (setter disabled for Variable type);
  
// PatientHeight flow acquisition
// (input disabled for Variable type);
// PatientHeight flow synthesis
// (output disabled for Variable type);
// PatientHeight getter
t_patientheight get_PatientHeight(void);
// PatientHeight setter
// (setter disabled for Variable type);
  
// TidalVolume flow acquisition
// (input disabled for Variable type);
// TidalVolume flow synthesis
// (output disabled for Variable type);
// TidalVolume getter
t_tidalvolume get_TidalVolume(void);
// TidalVolume setter
// (setter disabled for Variable type);
  
// PIDMode flow acquisition
// (input disabled for Variable type);
// PIDMode flow synthesis
// (output disabled for Variable type);
// PIDMode getter
t_pidmode get_PIDMode(void);
// PIDMode setter
// (setter disabled for Variable type);
  
// PIDState flow acquisition
// (input disabled for Variable type);
// PIDState flow synthesis
// (output disabled for Variable type);
// PIDState getter
t_pidstate get_PIDState(void);
// PIDState setter
// (setter disabled for Variable type);
  
// PIDErrorBits flow acquisition
// (input disabled for Variable type);
// PIDErrorBits flow synthesis
// (output disabled for Variable type);
// PIDErrorBits getter
t_piderrorbits get_PIDErrorBits(void);
// PIDErrorBits setter
// (setter disabled for Variable type);
  
// Alarms flow acquisition
// (input disabled for Variable type);
// Alarms flow synthesis
// (output disabled for Variable type);
// Alarms getter
t_alarms get_Alarms(void);
// Alarms setter
// (setter disabled for Variable type);
  
// Status flow acquisition
// (input disabled for Variable type);
// Status flow synthesis
// (output disabled for Variable type);
// Status getter
t_status get_Status(void);
// Status setter
// (setter disabled for Variable type);
  
// POWER_StartMode flow acquisition
// (input disabled for Variable type);
// POWER_StartMode flow synthesis
// (output disabled for Variable type);
// POWER_StartMode getter
t_power_startmode get_POWER_StartMode(void);
// POWER_StartMode setter
// (setter disabled for Variable type);
  
// POWER_StopMode flow acquisition
// (input disabled for Variable type);
// POWER_StopMode flow synthesis
// (output disabled for Variable type);
// POWER_StopMode getter
t_power_stopmode get_POWER_StopMode(void);
// POWER_StopMode setter
// (setter disabled for Variable type);
  
// HOME_Mode flow acquisition
// (input disabled for Variable type);
// HOME_Mode flow synthesis
// (output disabled for Variable type);
// HOME_Mode getter
t_home_mode get_HOME_Mode(void);
// HOME_Mode setter
// (setter disabled for Variable type);
  
// SetWriteData flow acquisition
// (input disabled for BOOL type);
// SetWriteData flow synthesis
// (output disabled for BOOL type);
// SetWriteData getter
BOOL  get_SetWriteData(void);
// SetWriteData setter
// (setter disabled for BOOL type);
  
// SetTime flow acquisition
// (input disabled for BOOL type);
// SetTime flow synthesis
// (output disabled for BOOL type);
// SetTime getter
BOOL  get_SetTime(void);
// SetTime setter
// (setter disabled for BOOL type);
  
// Trigger flow acquisition
// (input disabled for BOOL type);
// Trigger flow synthesis
// (output disabled for BOOL type);
// Trigger getter
BOOL  get_Trigger(void);
// Trigger setter
// (setter disabled for BOOL type);
  
// PeakPBPlus flow acquisition
// (input disabled for BOOL type);
// PeakPBPlus flow synthesis
// (output disabled for BOOL type);
// PeakPBPlus getter
BOOL  get_PeakPBPlus(void);
// PeakPBPlus setter
// (setter disabled for BOOL type);
  
// PeakPBMinus flow acquisition
// (input disabled for BOOL type);
// PeakPBMinus flow synthesis
// (output disabled for BOOL type);
// PeakPBMinus getter
BOOL  get_PeakPBMinus(void);
// PeakPBMinus setter
// (setter disabled for BOOL type);
  
// PeepPBPlus flow acquisition
// (input disabled for BOOL type);
// PeepPBPlus flow synthesis
// (output disabled for BOOL type);
// PeepPBPlus getter
BOOL  get_PeepPBPlus(void);
// PeepPBPlus setter
// (setter disabled for BOOL type);
  
// PeepPBMinus flow acquisition
// (input disabled for BOOL type);
// PeepPBMinus flow synthesis
// (output disabled for BOOL type);
// PeepPBMinus getter
BOOL  get_PeepPBMinus(void);
// PeepPBMinus setter
// (setter disabled for BOOL type);
  
// FrPBPlus flow acquisition
// (input disabled for BOOL type);
// FrPBPlus flow synthesis
// (output disabled for BOOL type);
// FrPBPlus getter
BOOL  get_FrPBPlus(void);
// FrPBPlus setter
// (setter disabled for BOOL type);
  
// FrPBMinus flow acquisition
// (input disabled for BOOL type);
// FrPBMinus flow synthesis
// (output disabled for BOOL type);
// FrPBMinus getter
BOOL  get_FrPBMinus(void);
// FrPBMinus setter
// (setter disabled for BOOL type);
  
// RecruitmentOn flow acquisition
// (input disabled for BOOL type);
// RecruitmentOn flow synthesis
// (output disabled for BOOL type);
// RecruitmentOn getter
BOOL  get_RecruitmentOn(void);
// RecruitmentOn setter
// (setter disabled for BOOL type);
  
// RecruitmentOff flow acquisition
// (input disabled for BOOL type);
// RecruitmentOff flow synthesis
// (output disabled for BOOL type);
// RecruitmentOff getter
BOOL  get_RecruitmentOff(void);
// RecruitmentOff setter
// (setter disabled for BOOL type);
  
// RecruitmentMem flow acquisition
// (input disabled for BOOL type);
// RecruitmentMem flow synthesis
// (output disabled for BOOL type);
// RecruitmentMem getter
BOOL  get_RecruitmentMem(void);
// RecruitmentMem setter
// (setter disabled for BOOL type);
  
// RecruitmentRunning flow acquisition
// (input disabled for BOOL type);
// RecruitmentRunning flow synthesis
// (output disabled for BOOL type);
// RecruitmentRunning getter
BOOL  get_RecruitmentRunning(void);
// RecruitmentRunning setter
// (setter disabled for BOOL type);
  
// RecruitmentEnding flow acquisition
// (input disabled for BOOL type);
// RecruitmentEnding flow synthesis
// (output disabled for BOOL type);
// RecruitmentEnding getter
BOOL  get_RecruitmentEnding(void);
// RecruitmentEnding setter
// (setter disabled for BOOL type);
  
// RecruitmentEnd flow acquisition
// (input disabled for BOOL type);
// RecruitmentEnd flow synthesis
// (output disabled for BOOL type);
// RecruitmentEnd getter
BOOL  get_RecruitmentEnd(void);
// RecruitmentEnd setter
// (setter disabled for BOOL type);
  
// ErrorAck flow acquisition
// (input disabled for BOOL type);
// ErrorAck flow synthesis
// (output disabled for BOOL type);
// ErrorAck getter
BOOL  get_ErrorAck(void);
// ErrorAck setter
// (setter disabled for BOOL type);
  
// Reset flow acquisition
// (input disabled for BOOL type);
// Reset flow synthesis
// (output disabled for BOOL type);
// Reset getter
BOOL  get_Reset(void);
// Reset setter
// (setter disabled for BOOL type);
  
// PIDError flow acquisition
// (input disabled for BOOL type);
// PIDError flow synthesis
// (output disabled for BOOL type);
// PIDError getter
BOOL  get_PIDError(void);
// PIDError setter
// (setter disabled for BOOL type);
  
// PeakDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeakDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// PeakDisplayPreAlarmHI getter
BOOL  get_PeakDisplayPreAlarmHI(void);
// PeakDisplayPreAlarmHI setter
// (setter disabled for BOOL type);
  
// PeakDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeakDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
// PeakDisplayAlarmHI getter
BOOL  get_PeakDisplayAlarmHI(void);
// PeakDisplayAlarmHI setter
// (setter disabled for BOOL type);
  
// PeakDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeakDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PeakDisplayPreAlarmLOW getter
BOOL  get_PeakDisplayPreAlarmLOW(void);
// PeakDisplayPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// PeakDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeakDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PeakDisplayAlarmLOW getter
BOOL  get_PeakDisplayAlarmLOW(void);
// PeakDisplayAlarmLOW setter
// (setter disabled for BOOL type);
  
// PeakInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeakInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// PeakInterlockPreAlarmHI getter
BOOL  get_PeakInterlockPreAlarmHI(void);
// PeakInterlockPreAlarmHI setter
// (setter disabled for BOOL type);
  
// PeakInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeakInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
// PeakInterlockAlarmHI getter
BOOL  get_PeakInterlockAlarmHI(void);
// PeakInterlockAlarmHI setter
// (setter disabled for BOOL type);
  
// PeakInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeakInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PeakInterlockPreAlarmLOW getter
BOOL  get_PeakInterlockPreAlarmLOW(void);
// PeakInterlockPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// PeakInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeakInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PeakInterlockAlarmLOW getter
BOOL  get_PeakInterlockAlarmLOW(void);
// PeakInterlockAlarmLOW setter
// (setter disabled for BOOL type);
  
// PlateauDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PlateauDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// PlateauDisplayPreAlarmHI getter
BOOL  get_PlateauDisplayPreAlarmHI(void);
// PlateauDisplayPreAlarmHI setter
// (setter disabled for BOOL type);
  
// PlateauDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// PlateauDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
// PlateauDisplayAlarmHI getter
BOOL  get_PlateauDisplayAlarmHI(void);
// PlateauDisplayAlarmHI setter
// (setter disabled for BOOL type);
  
// PlateauDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PlateauDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PlateauDisplayPreAlarmLOW getter
BOOL  get_PlateauDisplayPreAlarmLOW(void);
// PlateauDisplayPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// PlateauDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PlateauDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PlateauDisplayAlarmLOW getter
BOOL  get_PlateauDisplayAlarmLOW(void);
// PlateauDisplayAlarmLOW setter
// (setter disabled for BOOL type);
  
// PlateauInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PlateauInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// PlateauInterlockPreAlarmHI getter
BOOL  get_PlateauInterlockPreAlarmHI(void);
// PlateauInterlockPreAlarmHI setter
// (setter disabled for BOOL type);
  
// PlateauInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// PlateauInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
// PlateauInterlockAlarmHI getter
BOOL  get_PlateauInterlockAlarmHI(void);
// PlateauInterlockAlarmHI setter
// (setter disabled for BOOL type);
  
// PlateauInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PlateauInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PlateauInterlockPreAlarmLOW getter
BOOL  get_PlateauInterlockPreAlarmLOW(void);
// PlateauInterlockPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// PlateauInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PlateauInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PlateauInterlockAlarmLOW getter
BOOL  get_PlateauInterlockAlarmLOW(void);
// PlateauInterlockAlarmLOW setter
// (setter disabled for BOOL type);
  
// PeepDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeepDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// PeepDisplayPreAlarmHI getter
BOOL  get_PeepDisplayPreAlarmHI(void);
// PeepDisplayPreAlarmHI setter
// (setter disabled for BOOL type);
  
// PeepDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeepDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
// PeepDisplayAlarmHI getter
BOOL  get_PeepDisplayAlarmHI(void);
// PeepDisplayAlarmHI setter
// (setter disabled for BOOL type);
  
// PeepDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeepDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PeepDisplayPreAlarmLOW getter
BOOL  get_PeepDisplayPreAlarmLOW(void);
// PeepDisplayPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// PeepDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeepDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PeepDisplayAlarmLOW getter
BOOL  get_PeepDisplayAlarmLOW(void);
// PeepDisplayAlarmLOW setter
// (setter disabled for BOOL type);
  
// PeepInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeepInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// PeepInterlockPreAlarmHI getter
BOOL  get_PeepInterlockPreAlarmHI(void);
// PeepInterlockPreAlarmHI setter
// (setter disabled for BOOL type);
  
// PeepInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// PeepInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
// PeepInterlockAlarmHI getter
BOOL  get_PeepInterlockAlarmHI(void);
// PeepInterlockAlarmHI setter
// (setter disabled for BOOL type);
  
// PeepInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeepInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PeepInterlockPreAlarmLOW getter
BOOL  get_PeepInterlockPreAlarmLOW(void);
// PeepInterlockPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// PeepInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// PeepInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
// PeepInterlockAlarmLOW getter
BOOL  get_PeepInterlockAlarmLOW(void);
// PeepInterlockAlarmLOW setter
// (setter disabled for BOOL type);
  
// FrequencyDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// FrequencyDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// FrequencyDisplayPreAlarmHI getter
BOOL  get_FrequencyDisplayPreAlarmHI(void);
// FrequencyDisplayPreAlarmHI setter
// (setter disabled for BOOL type);
  
// FrequencyDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// FrequencyDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
// FrequencyDisplayAlarmHI getter
BOOL  get_FrequencyDisplayAlarmHI(void);
// FrequencyDisplayAlarmHI setter
// (setter disabled for BOOL type);
  
// FrequencyDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// FrequencyDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// FrequencyDisplayPreAlarmLOW getter
BOOL  get_FrequencyDisplayPreAlarmLOW(void);
// FrequencyDisplayPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// FrequencyDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// FrequencyDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
// FrequencyDisplayAlarmLOW getter
BOOL  get_FrequencyDisplayAlarmLOW(void);
// FrequencyDisplayAlarmLOW setter
// (setter disabled for BOOL type);
  
// FrequencyInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// FrequencyInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// FrequencyInterlockPreAlarmHI getter
BOOL  get_FrequencyInterlockPreAlarmHI(void);
// FrequencyInterlockPreAlarmHI setter
// (setter disabled for BOOL type);
  
// FrequencyInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// FrequencyInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
// FrequencyInterlockAlarmHI getter
BOOL  get_FrequencyInterlockAlarmHI(void);
// FrequencyInterlockAlarmHI setter
// (setter disabled for BOOL type);
  
// FrequencyInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// FrequencyInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// FrequencyInterlockPreAlarmLOW getter
BOOL  get_FrequencyInterlockPreAlarmLOW(void);
// FrequencyInterlockPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// FrequencyInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// FrequencyInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
// FrequencyInterlockAlarmLOW getter
BOOL  get_FrequencyInterlockAlarmLOW(void);
// FrequencyInterlockAlarmLOW setter
// (setter disabled for BOOL type);
  
// TidalVolumeDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// TidalVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// TidalVolumeDisplayPreAlarmHI getter
BOOL  get_TidalVolumeDisplayPreAlarmHI(void);
// TidalVolumeDisplayPreAlarmHI setter
// (setter disabled for BOOL type);
  
// TidalVolumeDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// TidalVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
// TidalVolumeDisplayAlarmHI getter
BOOL  get_TidalVolumeDisplayAlarmHI(void);
// TidalVolumeDisplayAlarmHI setter
// (setter disabled for BOOL type);
  
// TidalVolumeDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TidalVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// TidalVolumeDisplayPreAlarmLOW getter
BOOL  get_TidalVolumeDisplayPreAlarmLOW(void);
// TidalVolumeDisplayPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// TidalVolumeDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TidalVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
// TidalVolumeDisplayAlarmLOW getter
BOOL  get_TidalVolumeDisplayAlarmLOW(void);
// TidalVolumeDisplayAlarmLOW setter
// (setter disabled for BOOL type);
  
// TidalVolumeInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// TidalVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// TidalVolumeInterlockPreAlarmHI getter
BOOL  get_TidalVolumeInterlockPreAlarmHI(void);
// TidalVolumeInterlockPreAlarmHI setter
// (setter disabled for BOOL type);
  
// TidalVolumeInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// TidalVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
// TidalVolumeInterlockAlarmHI getter
BOOL  get_TidalVolumeInterlockAlarmHI(void);
// TidalVolumeInterlockAlarmHI setter
// (setter disabled for BOOL type);
  
// TidalVolumeInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TidalVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// TidalVolumeInterlockPreAlarmLOW getter
BOOL  get_TidalVolumeInterlockPreAlarmLOW(void);
// TidalVolumeInterlockPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// TidalVolumeInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TidalVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
// TidalVolumeInterlockAlarmLOW getter
BOOL  get_TidalVolumeInterlockAlarmLOW(void);
// TidalVolumeInterlockAlarmLOW setter
// (setter disabled for BOOL type);
  
// MinuteVolumeDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// MinuteVolumeDisplayPreAlarmHI getter
BOOL  get_MinuteVolumeDisplayPreAlarmHI(void);
// MinuteVolumeDisplayPreAlarmHI setter
// (setter disabled for BOOL type);
  
// MinuteVolumeDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
// MinuteVolumeDisplayAlarmHI getter
BOOL  get_MinuteVolumeDisplayAlarmHI(void);
// MinuteVolumeDisplayAlarmHI setter
// (setter disabled for BOOL type);
  
// MinuteVolumeDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// MinuteVolumeDisplayPreAlarmLOW getter
BOOL  get_MinuteVolumeDisplayPreAlarmLOW(void);
// MinuteVolumeDisplayPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// MinuteVolumeDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
// MinuteVolumeDisplayAlarmLOW getter
BOOL  get_MinuteVolumeDisplayAlarmLOW(void);
// MinuteVolumeDisplayAlarmLOW setter
// (setter disabled for BOOL type);
  
// MinuteVolumeInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// MinuteVolumeInterlockPreAlarmHI getter
BOOL  get_MinuteVolumeInterlockPreAlarmHI(void);
// MinuteVolumeInterlockPreAlarmHI setter
// (setter disabled for BOOL type);
  
// MinuteVolumeInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
// MinuteVolumeInterlockAlarmHI getter
BOOL  get_MinuteVolumeInterlockAlarmHI(void);
// MinuteVolumeInterlockAlarmHI setter
// (setter disabled for BOOL type);
  
// MinuteVolumeInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// MinuteVolumeInterlockPreAlarmLOW getter
BOOL  get_MinuteVolumeInterlockPreAlarmLOW(void);
// MinuteVolumeInterlockPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// MinuteVolumeInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// MinuteVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
// MinuteVolumeInterlockAlarmLOW getter
BOOL  get_MinuteVolumeInterlockAlarmLOW(void);
// MinuteVolumeInterlockAlarmLOW setter
// (setter disabled for BOOL type);
  
// TriggerDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// TriggerDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// TriggerDisplayPreAlarmHI getter
BOOL  get_TriggerDisplayPreAlarmHI(void);
// TriggerDisplayPreAlarmHI setter
// (setter disabled for BOOL type);
  
// TriggerDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// TriggerDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
// TriggerDisplayAlarmHI getter
BOOL  get_TriggerDisplayAlarmHI(void);
// TriggerDisplayAlarmHI setter
// (setter disabled for BOOL type);
  
// TriggerDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TriggerDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// TriggerDisplayPreAlarmLOW getter
BOOL  get_TriggerDisplayPreAlarmLOW(void);
// TriggerDisplayPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// TriggerDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TriggerDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
// TriggerDisplayAlarmLOW getter
BOOL  get_TriggerDisplayAlarmLOW(void);
// TriggerDisplayAlarmLOW setter
// (setter disabled for BOOL type);
  
// TriggerInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// TriggerInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// TriggerInterlockPreAlarmHI getter
BOOL  get_TriggerInterlockPreAlarmHI(void);
// TriggerInterlockPreAlarmHI setter
// (setter disabled for BOOL type);
  
// TriggerInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// TriggerInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
// TriggerInterlockAlarmHI getter
BOOL  get_TriggerInterlockAlarmHI(void);
// TriggerInterlockAlarmHI setter
// (setter disabled for BOOL type);
  
// TriggerInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TriggerInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// TriggerInterlockPreAlarmLOW getter
BOOL  get_TriggerInterlockPreAlarmLOW(void);
// TriggerInterlockPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// TriggerInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// TriggerInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
// TriggerInterlockAlarmLOW getter
BOOL  get_TriggerInterlockAlarmLOW(void);
// TriggerInterlockAlarmLOW setter
// (setter disabled for BOOL type);
  
// BatteryDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// BatteryDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// BatteryDisplayPreAlarmHI getter
BOOL  get_BatteryDisplayPreAlarmHI(void);
// BatteryDisplayPreAlarmHI setter
// (setter disabled for BOOL type);
  
// BatteryDisplayAlarmHI flow acquisition
// (input disabled for BOOL type);
// BatteryDisplayAlarmHI flow synthesis
// (output disabled for BOOL type);
// BatteryDisplayAlarmHI getter
BOOL  get_BatteryDisplayAlarmHI(void);
// BatteryDisplayAlarmHI setter
// (setter disabled for BOOL type);
  
// BatteryDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// BatteryDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// BatteryDisplayPreAlarmLOW getter
BOOL  get_BatteryDisplayPreAlarmLOW(void);
// BatteryDisplayPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// BatteryDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type);
// BatteryDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type);
// BatteryDisplayAlarmLOW getter
BOOL  get_BatteryDisplayAlarmLOW(void);
// BatteryDisplayAlarmLOW setter
// (setter disabled for BOOL type);
  
// BatteryInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type);
// BatteryInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type);
// BatteryInterlockPreAlarmHI getter
BOOL  get_BatteryInterlockPreAlarmHI(void);
// BatteryInterlockPreAlarmHI setter
// (setter disabled for BOOL type);
  
// BatteryInterlockAlarmHI flow acquisition
// (input disabled for BOOL type);
// BatteryInterlockAlarmHI flow synthesis
// (output disabled for BOOL type);
// BatteryInterlockAlarmHI getter
BOOL  get_BatteryInterlockAlarmHI(void);
// BatteryInterlockAlarmHI setter
// (setter disabled for BOOL type);
  
// BatteryInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type);
// BatteryInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type);
// BatteryInterlockPreAlarmLOW getter
BOOL  get_BatteryInterlockPreAlarmLOW(void);
// BatteryInterlockPreAlarmLOW setter
// (setter disabled for BOOL type);
  
// BatteryInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type);
// BatteryInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type);
// BatteryInterlockAlarmLOW getter
BOOL  get_BatteryInterlockAlarmLOW(void);
// BatteryInterlockAlarmLOW setter
// (setter disabled for BOOL type);
  
// Enable flow acquisition
// (input disabled for BOOL type);
// Enable flow synthesis
// (output disabled for BOOL type);
// Enable getter
BOOL  get_Enable(void);
// Enable setter
// (setter disabled for BOOL type);
  
// Busy flow acquisition
// (input disabled for BOOL type);
// Busy flow synthesis
// (output disabled for BOOL type);
// Busy getter
BOOL  get_Busy(void);
// Busy setter
// (setter disabled for BOOL type);
  
// POWER_Enable flow acquisition
// (input disabled for BOOL type);
// POWER_Enable flow synthesis
// (output disabled for BOOL type);
// POWER_Enable getter
BOOL  get_POWER_Enable(void);
// POWER_Enable setter
// (setter disabled for BOOL type);
  
// POWER_Status flow acquisition
// (input disabled for BOOL type);
// POWER_Status flow synthesis
// (output disabled for BOOL type);
// POWER_Status getter
BOOL  get_POWER_Status(void);
// POWER_Status setter
// (setter disabled for BOOL type);
  
// POWER_Error flow acquisition
// (input disabled for BOOL type);
// POWER_Error flow synthesis
// (output disabled for BOOL type);
// POWER_Error getter
BOOL  get_POWER_Error(void);
// POWER_Error setter
// (setter disabled for BOOL type);
  
// RESET_Execute flow acquisition
// (input disabled for BOOL type);
// RESET_Execute flow synthesis
// (output disabled for BOOL type);
// RESET_Execute getter
BOOL  get_RESET_Execute(void);
// RESET_Execute setter
// (setter disabled for BOOL type);
  
// RESET_Done flow acquisition
// (input disabled for BOOL type);
// RESET_Done flow synthesis
// (output disabled for BOOL type);
// RESET_Done getter
BOOL  get_RESET_Done(void);
// RESET_Done setter
// (setter disabled for BOOL type);
  
// RESET_Error flow acquisition
// (input disabled for BOOL type);
// RESET_Error flow synthesis
// (output disabled for BOOL type);
// RESET_Error getter
BOOL  get_RESET_Error(void);
// RESET_Error setter
// (setter disabled for BOOL type);
  
// HOME_Execute flow acquisition
// (input disabled for BOOL type);
// HOME_Execute flow synthesis
// (output disabled for BOOL type);
// HOME_Execute getter
BOOL  get_HOME_Execute(void);
// HOME_Execute setter
// (setter disabled for BOOL type);
  
// HOME_Done flow acquisition
// (input disabled for BOOL type);
// HOME_Done flow synthesis
// (output disabled for BOOL type);
// HOME_Done getter
BOOL  get_HOME_Done(void);
// HOME_Done setter
// (setter disabled for BOOL type);
  
// HOME_Error flow acquisition
// (input disabled for BOOL type);
// HOME_Error flow synthesis
// (output disabled for BOOL type);
// HOME_Error getter
BOOL  get_HOME_Error(void);
// HOME_Error setter
// (setter disabled for BOOL type);
  
// HALT_Execute flow acquisition
// (input disabled for BOOL type);
// HALT_Execute flow synthesis
// (output disabled for BOOL type);
// HALT_Execute getter
BOOL  get_HALT_Execute(void);
// HALT_Execute setter
// (setter disabled for BOOL type);
  
// HALT_Done flow acquisition
// (input disabled for BOOL type);
// HALT_Done flow synthesis
// (output disabled for BOOL type);
// HALT_Done getter
BOOL  get_HALT_Done(void);
// HALT_Done setter
// (setter disabled for BOOL type);
  
// HALT_Error flow acquisition
// (input disabled for BOOL type);
// HALT_Error flow synthesis
// (output disabled for BOOL type);
// HALT_Error getter
BOOL  get_HALT_Error(void);
// HALT_Error setter
// (setter disabled for BOOL type);
  
// ABSOLUTE_Execute flow acquisition
// (input disabled for BOOL type);
// ABSOLUTE_Execute flow synthesis
// (output disabled for BOOL type);
// ABSOLUTE_Execute getter
BOOL  get_ABSOLUTE_Execute(void);
// ABSOLUTE_Execute setter
// (setter disabled for BOOL type);
  
// ABSOLUTE_Done flow acquisition
// (input disabled for BOOL type);
// ABSOLUTE_Done flow synthesis
// (output disabled for BOOL type);
// ABSOLUTE_Done getter
BOOL  get_ABSOLUTE_Done(void);
// ABSOLUTE_Done setter
// (setter disabled for BOOL type);
  
// ABSOLUTE_Error flow acquisition
// (input disabled for BOOL type);
// ABSOLUTE_Error flow synthesis
// (output disabled for BOOL type);
// ABSOLUTE_Error getter
BOOL  get_ABSOLUTE_Error(void);
// ABSOLUTE_Error setter
// (setter disabled for BOOL type);
  
// RELATIVE_Execute flow acquisition
// (input disabled for BOOL type);
// RELATIVE_Execute flow synthesis
// (output disabled for BOOL type);
// RELATIVE_Execute getter
BOOL  get_RELATIVE_Execute(void);
// RELATIVE_Execute setter
// (setter disabled for BOOL type);
  
// RELATIVE_Done flow acquisition
// (input disabled for BOOL type);
// RELATIVE_Done flow synthesis
// (output disabled for BOOL type);
// RELATIVE_Done getter
BOOL  get_RELATIVE_Done(void);
// RELATIVE_Done setter
// (setter disabled for BOOL type);
  
// RELATIVE_Error flow acquisition
// (input disabled for BOOL type);
// RELATIVE_Error flow synthesis
// (output disabled for BOOL type);
// RELATIVE_Error getter
BOOL  get_RELATIVE_Error(void);
// RELATIVE_Error setter
// (setter disabled for BOOL type);
  
// Velocity_Execute flow acquisition
// (input disabled for BOOL type);
// Velocity_Execute flow synthesis
// (output disabled for BOOL type);
// Velocity_Execute getter
BOOL  get_Velocity_Execute(void);
// Velocity_Execute setter
// (setter disabled for BOOL type);
  
// Velocity_Current flow acquisition
// (input disabled for BOOL type);
// Velocity_Current flow synthesis
// (output disabled for BOOL type);
// Velocity_Current getter
BOOL  get_Velocity_Current(void);
// Velocity_Current setter
// (setter disabled for BOOL type);
  
// Velocity_invelocity flow acquisition
// (input disabled for BOOL type);
// Velocity_invelocity flow synthesis
// (output disabled for BOOL type);
// Velocity_invelocity getter
BOOL  get_Velocity_invelocity(void);
// Velocity_invelocity setter
// (setter disabled for BOOL type);
  
// Velocity_Error flow acquisition
// (input disabled for BOOL type);
// Velocity_Error flow synthesis
// (output disabled for BOOL type);
// Velocity_Error getter
BOOL  get_Velocity_Error(void);
// Velocity_Error setter
// (setter disabled for BOOL type);
  
// JOG_Forward flow acquisition
// (input disabled for BOOL type);
// JOG_Forward flow synthesis
// (output disabled for BOOL type);
// JOG_Forward getter
BOOL  get_JOG_Forward(void);
// JOG_Forward setter
// (setter disabled for BOOL type);
  
// JOG_Backward flow acquisition
// (input disabled for BOOL type);
// JOG_Backward flow synthesis
// (output disabled for BOOL type);
// JOG_Backward getter
BOOL  get_JOG_Backward(void);
// JOG_Backward setter
// (setter disabled for BOOL type);
  
// JOG_Invelocity flow acquisition
// (input disabled for BOOL type);
// JOG_Invelocity flow synthesis
// (output disabled for BOOL type);
// JOG_Invelocity getter
BOOL  get_JOG_Invelocity(void);
// JOG_Invelocity setter
// (setter disabled for BOOL type);
  
// JOG_Error flow acquisition
// (input disabled for BOOL type);
// JOG_Error flow synthesis
// (output disabled for BOOL type);
// JOG_Error getter
BOOL  get_JOG_Error(void);
// JOG_Error setter
// (setter disabled for BOOL type);
  
// Permissions flow acquisition
// (input disabled for BOOL type);
// Permissions flow synthesis
// (output disabled for BOOL type);
// Permissions getter
BOOL  get_Permissions(void);
// Permissions setter
// (setter disabled for BOOL type);
  
// Operation_ON flow acquisition
// (input disabled for BOOL type);
// Operation_ON flow synthesis
// (output disabled for BOOL type);
// Operation_ON getter
BOOL  get_Operation_ON(void);
// Operation_ON setter
// (setter disabled for BOOL type);
  
// Emergency flow acquisition
// (input disabled for BOOL type);
// Emergency flow synthesis
// (output disabled for BOOL type);
// Emergency getter
BOOL  get_Emergency(void);
// Emergency setter
// (setter disabled for BOOL type);
  
// GeneralAck flow acquisition
// (input disabled for BOOL type);
// GeneralAck flow synthesis
// (output disabled for BOOL type);
// GeneralAck getter
BOOL  get_GeneralAck(void);
// GeneralAck setter
// (setter disabled for BOOL type);
  
// MotorAck flow acquisition
// (input disabled for BOOL type);
// MotorAck flow synthesis
// (output disabled for BOOL type);
// MotorAck getter
BOOL  get_MotorAck(void);
// MotorAck setter
// (setter disabled for BOOL type);
  
// ProcessAck flow acquisition
// (input disabled for BOOL type);
// ProcessAck flow synthesis
// (output disabled for BOOL type);
// ProcessAck getter
BOOL  get_ProcessAck(void);
// ProcessAck setter
// (setter disabled for BOOL type);
  
// GeneralReset flow acquisition
// (input disabled for BOOL type);
// GeneralReset flow synthesis
// (output disabled for BOOL type);
// GeneralReset getter
BOOL  get_GeneralReset(void);
// GeneralReset setter
// (setter disabled for BOOL type);
  
// MotorReset flow acquisition
// (input disabled for BOOL type);
// MotorReset flow synthesis
// (output disabled for BOOL type);
// MotorReset getter
BOOL  get_MotorReset(void);
// MotorReset setter
// (setter disabled for BOOL type);
  
// ProcessReset flow acquisition
// (input disabled for BOOL type);
// ProcessReset flow synthesis
// (output disabled for BOOL type);
// ProcessReset getter
BOOL  get_ProcessReset(void);
// ProcessReset setter
// (setter disabled for BOOL type);
  
// PeakValue flow acquisition
// (input disabled for UI_16 type);
// PeakValue flow synthesis
// (output disabled for UI_16 type);
// PeakValue getter
UI_16 get_PeakValue(void);
// PeakValue setter
// (setter disabled for UI_16 type);
  
// PlateauValue flow acquisition
// (input disabled for UI_16 type);
// PlateauValue flow synthesis
// (output disabled for UI_16 type);
// PlateauValue getter
UI_16 get_PlateauValue(void);
// PlateauValue setter
// (setter disabled for UI_16 type);
  
// PeepValue flow acquisition
// (input disabled for UI_16 type);
// PeepValue flow synthesis
// (output disabled for UI_16 type);
// PeepValue getter
UI_16 get_PeepValue(void);
// PeepValue setter
// (setter disabled for UI_16 type);
  
// FrequencyValue flow acquisition
// (input disabled for UI_16 type);
// FrequencyValue flow synthesis
// (output disabled for UI_16 type);
// FrequencyValue getter
UI_16 get_FrequencyValue(void);
// FrequencyValue setter
// (setter disabled for UI_16 type);
  
// TidalVolumeValue flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeValue flow synthesis
// (output disabled for UI_16 type);
// TidalVolumeValue getter
UI_16 get_TidalVolumeValue(void);
// TidalVolumeValue setter
// (setter disabled for UI_16 type);
  
// MinumValue flow acquisition
// (input disabled for UI_16 type);
// MinumValue flow synthesis
// (output disabled for UI_16 type);
// MinumValue getter
UI_16 get_MinumValue(void);
// MinumValue setter
// (setter disabled for UI_16 type);
  
// DrivingPreassure flow acquisition
// (input disabled for UI_16 type);
// DrivingPreassure flow synthesis
// (output disabled for UI_16 type);
// DrivingPreassure getter
UI_16 get_DrivingPreassure(void);
// DrivingPreassure setter
// (setter disabled for UI_16 type);
  
// BatteryVoltage flow acquisition
// (input disabled for UI_16 type);
// BatteryVoltage flow synthesis
// (output disabled for UI_16 type);
// BatteryVoltage getter
UI_16 get_BatteryVoltage(void);
// BatteryVoltage setter
// (setter disabled for UI_16 type);
  
// BatteryCapacity flow acquisition
// (input disabled for UI_16 type);
// BatteryCapacity flow synthesis
// (output disabled for UI_16 type);
// BatteryCapacity getter
UI_16 get_BatteryCapacity(void);
// BatteryCapacity setter
// (setter disabled for UI_16 type);
  
// Pressure1 flow acquisition
// (input disabled for UI_16 type);
// Pressure1 flow synthesis
// (output disabled for UI_16 type);
// Pressure1 getter
UI_16 get_Pressure1(void);
// Pressure1 setter
// (setter disabled for UI_16 type);
  
// Pressure2 flow acquisition
// (input disabled for UI_16 type);
// Pressure2 flow synthesis
// (output disabled for UI_16 type);
// Pressure2 getter
UI_16 get_Pressure2(void);
// Pressure2 setter
// (setter disabled for UI_16 type);
  
// VolumeAccumulated flow acquisition
// (input disabled for UI_16 type);
// VolumeAccumulated flow synthesis
// (output disabled for UI_16 type);
// VolumeAccumulated getter
UI_16 get_VolumeAccumulated(void);
// VolumeAccumulated setter
// (setter disabled for UI_16 type);
  
// Flow flow acquisition
// (input disabled for UI_16 type);
// Flow flow synthesis
// (output disabled for UI_16 type);
// Flow getter
UI_16 get_Flow(void);
// Flow setter
// (setter disabled for UI_16 type);
  
// SystemDateRetVal flow acquisition
// (input disabled for UI_16 type);
// SystemDateRetVal flow synthesis
// (output disabled for UI_16 type);
// SystemDateRetVal getter
UI_16 get_SystemDateRetVal(void);
// SystemDateRetVal setter
// (setter disabled for UI_16 type);
  
// SystemDateOut flow acquisition
// (input disabled for DateTime type);
// SystemDateOut flow synthesis
// (output disabled for DateTime type);
// SystemDateOut getter
t_datetime get_SystemDateOut(void);
// SystemDateOut setter
// (setter disabled for DateTime type);
  
// LocalDateRetVal flow acquisition
// (input disabled for UI_16 type);
// LocalDateRetVal flow synthesis
// (output disabled for UI_16 type);
// LocalDateRetVal getter
UI_16 get_LocalDateRetVal(void);
// LocalDateRetVal setter
// (setter disabled for UI_16 type);
  
// LocalDateOut flow acquisition
// (input disabled for DateTime type);
// LocalDateOut flow synthesis
// (output disabled for DateTime type);
// LocalDateOut getter
t_datetime get_LocalDateOut(void);
// LocalDateOut setter
// (setter disabled for DateTime type);
  
// WriteDateTime flow acquisition
// (input disabled for DateTime type);
// WriteDateTime flow synthesis
// (output disabled for DateTime type);
// WriteDateTime getter
t_datetime get_WriteDateTime(void);
// WriteDateTime setter
// (setter disabled for DateTime type);
  
// WriteDateOut flow acquisition
// (input disabled for DateTime type);
// WriteDateOut flow synthesis
// (output disabled for DateTime type);
// WriteDateOut getter
t_datetime get_WriteDateOut(void);
// WriteDateOut setter
// (setter disabled for DateTime type);
  
// StatuSetTime flow acquisition
// (input disabled for DateTime type);
// StatuSetTime flow synthesis
// (output disabled for DateTime type);
// StatuSetTime getter
t_datetime get_StatuSetTime(void);
// StatuSetTime setter
// (setter disabled for DateTime type);
  
// PeakMax flow acquisition
// (input disabled for UI_16 type);
// PeakMax flow synthesis
// (output disabled for UI_16 type);
// PeakMax getter
UI_16 get_PeakMax(void);
// PeakMax setter
// (setter disabled for UI_16 type);
  
// PeakSetpoint flow acquisition
// (input disabled for UI_16 type);
// PeakSetpoint flow synthesis
// (output disabled for UI_16 type);
// PeakSetpoint getter
UI_16 get_PeakSetpoint(void);
// PeakSetpoint setter
// (setter disabled for UI_16 type);
  
// PeakMin flow acquisition
// (input disabled for UI_16 type);
// PeakMin flow synthesis
// (output disabled for UI_16 type);
// PeakMin getter
UI_16 get_PeakMin(void);
// PeakMin setter
// (setter disabled for UI_16 type);
  
// PlateauMax flow acquisition
// (input disabled for UI_16 type);
// PlateauMax flow synthesis
// (output disabled for UI_16 type);
// PlateauMax getter
UI_16 get_PlateauMax(void);
// PlateauMax setter
// (setter disabled for UI_16 type);
  
// PlateauSetpoint flow acquisition
// (input disabled for UI_16 type);
// PlateauSetpoint flow synthesis
// (output disabled for UI_16 type);
// PlateauSetpoint getter
UI_16 get_PlateauSetpoint(void);
// PlateauSetpoint setter
// (setter disabled for UI_16 type);
  
// PlateauMin flow acquisition
// (input disabled for UI_16 type);
// PlateauMin flow synthesis
// (output disabled for UI_16 type);
// PlateauMin getter
UI_16 get_PlateauMin(void);
// PlateauMin setter
// (setter disabled for UI_16 type);
  
// PeepMax flow acquisition
// (input disabled for UI_16 type);
// PeepMax flow synthesis
// (output disabled for UI_16 type);
// PeepMax getter
UI_16 get_PeepMax(void);
// PeepMax setter
// (setter disabled for UI_16 type);
  
// PeepSetpoint flow acquisition
// (input disabled for UI_16 type);
// PeepSetpoint flow synthesis
// (output disabled for UI_16 type);
// PeepSetpoint getter
UI_16 get_PeepSetpoint(void);
// PeepSetpoint setter
// (setter disabled for UI_16 type);
  
// PeepMin flow acquisition
// (input disabled for UI_16 type);
// PeepMin flow synthesis
// (output disabled for UI_16 type);
// PeepMin getter
UI_16 get_PeepMin(void);
// PeepMin setter
// (setter disabled for UI_16 type);
  
// FrequencyMax flow acquisition
// (input disabled for UI_16 type);
// FrequencyMax flow synthesis
// (output disabled for UI_16 type);
// FrequencyMax getter
UI_16 get_FrequencyMax(void);
// FrequencyMax setter
// (setter disabled for UI_16 type);
  
// FrequencySetpoint flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpoint flow synthesis
// (output disabled for UI_16 type);
// FrequencySetpoint getter
UI_16 get_FrequencySetpoint(void);
// FrequencySetpoint setter
// (setter disabled for UI_16 type);
  
// FrequencyMin flow acquisition
// (input disabled for UI_16 type);
// FrequencyMin flow synthesis
// (output disabled for UI_16 type);
// FrequencyMin getter
UI_16 get_FrequencyMin(void);
// FrequencyMin setter
// (setter disabled for UI_16 type);
  
// TidalVolumenMax flow acquisition
// (input disabled for UI_16 type);
// TidalVolumenMax flow synthesis
// (output disabled for UI_16 type);
// TidalVolumenMax getter
UI_16 get_TidalVolumenMax(void);
// TidalVolumenMax setter
// (setter disabled for UI_16 type);
  
// TidalVolumenSetpoint flow acquisition
// (input disabled for UI_16 type);
// TidalVolumenSetpoint flow synthesis
// (output disabled for UI_16 type);
// TidalVolumenSetpoint getter
UI_16 get_TidalVolumenSetpoint(void);
// TidalVolumenSetpoint setter
// (setter disabled for UI_16 type);
  
// TidalVolumenMin flow acquisition
// (input disabled for UI_16 type);
// TidalVolumenMin flow synthesis
// (output disabled for UI_16 type);
// TidalVolumenMin getter
UI_16 get_TidalVolumenMin(void);
// TidalVolumenMin setter
// (setter disabled for UI_16 type);
  
// MinimumVolumeMax flow acquisition
// (input disabled for UI_16 type);
// MinimumVolumeMax flow synthesis
// (output disabled for UI_16 type);
// MinimumVolumeMax getter
UI_16 get_MinimumVolumeMax(void);
// MinimumVolumeMax setter
// (setter disabled for UI_16 type);
  
// MinimumVolumeSetpoint flow acquisition
// (input disabled for UI_16 type);
// MinimumVolumeSetpoint flow synthesis
// (output disabled for UI_16 type);
// MinimumVolumeSetpoint getter
UI_16 get_MinimumVolumeSetpoint(void);
// MinimumVolumeSetpoint setter
// (setter disabled for UI_16 type);
  
// MinimumVolumeMin flow acquisition
// (input disabled for UI_16 type);
// MinimumVolumeMin flow synthesis
// (output disabled for UI_16 type);
// MinimumVolumeMin getter
UI_16 get_MinimumVolumeMin(void);
// MinimumVolumeMin setter
// (setter disabled for UI_16 type);
  
// TriggerSetpoint flow acquisition
// (input disabled for UI_16 type);
// TriggerSetpoint flow synthesis
// (output disabled for UI_16 type);
// TriggerSetpoint getter
UI_16 get_TriggerSetpoint(void);
// TriggerSetpoint setter
// (setter disabled for UI_16 type);
  
// RecruitmentTimer flow acquisition
// (input disabled for TimeUI_16 type);
// RecruitmentTimer flow synthesis
// (output disabled for TimeUI_16 type);
// RecruitmentTimer getter
UI_16 get_RecruitmentTimer(void);
// RecruitmentTimer setter
// (setter disabled for TimeUI_16 type);
  
// RecruitmentElap flow acquisition
// (input disabled for TimeUI_16 type);
// RecruitmentElap flow synthesis
// (output disabled for TimeUI_16 type);
// RecruitmentElap getter
UI_16 get_RecruitmentElap(void);
// RecruitmentElap setter
// (setter disabled for TimeUI_16 type);
  
// TimeStart flow acquisition
// (input disabled for DateTime type);
// TimeStart flow synthesis
// (output disabled for DateTime type);
// TimeStart getter
t_datetime get_TimeStart(void);
// TimeStart setter
// (setter disabled for DateTime type);
  
// TimeEnd flow acquisition
// (input disabled for DateTime type);
// TimeEnd flow synthesis
// (output disabled for DateTime type);
// TimeEnd getter
t_datetime get_TimeEnd(void);
// TimeEnd setter
// (setter disabled for DateTime type);
  
// FlowSetpoint flow acquisition
// (input disabled for UI_16 type);
// FlowSetpoint flow synthesis
// (output disabled for UI_16 type);
// FlowSetpoint getter
UI_16 get_FlowSetpoint(void);
// FlowSetpoint setter
// (setter disabled for UI_16 type);
  
// PIDOutReal flow acquisition
// (input disabled for FL_16 type);
// PIDOutReal flow synthesis
// (output disabled for FL_16 type);
// PIDOutReal getter
FL_16 get_PIDOutReal(void);
// PIDOutReal setter
// (setter disabled for FL_16 type);
  
// PIDOutInt flow acquisition
// (input disabled for UI_16 type);
// PIDOutInt flow synthesis
// (output disabled for UI_16 type);
// PIDOutInt getter
UI_16 get_PIDOutInt(void);
// PIDOutInt setter
// (setter disabled for UI_16 type);
  
// PIDOutPWM flow acquisition
// (input disabled for UI_16 type);
// PIDOutPWM flow synthesis
// (output disabled for UI_16 type);
// PIDOutPWM getter
UI_16 get_PIDOutPWM(void);
// PIDOutPWM setter
// (setter disabled for UI_16 type);
  
// PIDOutMotor flow acquisition
// (input disabled for UI_16 type);
// PIDOutMotor flow synthesis
// (output disabled for UI_16 type);
// PIDOutMotor getter
UI_16 get_PIDOutMotor(void);
// PIDOutMotor setter
// (setter disabled for UI_16 type);
  
// PeakSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
// PeakSetpointPreAlarmHI getter
UI_16 get_PeakSetpointPreAlarmHI(void);
// PeakSetpointPreAlarmHI setter
// (setter disabled for UI_16 type);
  
// PeakSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
// PeakSetpointAlarmHI getter
UI_16 get_PeakSetpointAlarmHI(void);
// PeakSetpointAlarmHI setter
// (setter disabled for UI_16 type);
  
// PeakSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// PeakSetpointPreAlarmLOW getter
UI_16 get_PeakSetpointPreAlarmLOW(void);
// PeakSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type);
  
// PeakSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// PeakSetpointAlarmLOW getter
UI_16 get_PeakSetpointAlarmLOW(void);
// PeakSetpointAlarmLOW setter
// (setter disabled for UI_16 type);
  
// PeakSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
// PeakSetpointHysteresisHI getter
UI_16 get_PeakSetpointHysteresisHI(void);
// PeakSetpointHysteresisHI setter
// (setter disabled for UI_16 type);
  
// PeakSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// PeakSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
// PeakSetpointHysteresisLOW getter
UI_16 get_PeakSetpointHysteresisLOW(void);
// PeakSetpointHysteresisLOW setter
// (setter disabled for UI_16 type);
  
// PlateauSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PlateauSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
// PlateauSetpointPreAlarmHI getter
UI_16 get_PlateauSetpointPreAlarmHI(void);
// PlateauSetpointPreAlarmHI setter
// (setter disabled for UI_16 type);
  
// PlateauSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PlateauSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
// PlateauSetpointAlarmHI getter
UI_16 get_PlateauSetpointAlarmHI(void);
// PlateauSetpointAlarmHI setter
// (setter disabled for UI_16 type);
  
// PlateauSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PlateauSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// PlateauSetpointPreAlarmLOW getter
UI_16 get_PlateauSetpointPreAlarmLOW(void);
// PlateauSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type);
  
// PlateaukSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PlateaukSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// PlateaukSetpointAlarmLOW getter
UI_16 get_PlateaukSetpointAlarmLOW(void);
// PlateaukSetpointAlarmLOW setter
// (setter disabled for UI_16 type);
  
// PlateaukSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// PlateaukSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
// PlateaukSetpointHysteresisHI getter
UI_16 get_PlateaukSetpointHysteresisHI(void);
// PlateaukSetpointHysteresisHI setter
// (setter disabled for UI_16 type);
  
// PlateaukSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// PlateaukSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
// PlateaukSetpointHysteresisLOW getter
UI_16 get_PlateaukSetpointHysteresisLOW(void);
// PlateaukSetpointHysteresisLOW setter
// (setter disabled for UI_16 type);
  
// PeepSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
// PeepSetpointPreAlarmHI getter
UI_16 get_PeepSetpointPreAlarmHI(void);
// PeepSetpointPreAlarmHI setter
// (setter disabled for UI_16 type);
  
// PeepSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
// PeepSetpointAlarmHI getter
UI_16 get_PeepSetpointAlarmHI(void);
// PeepSetpointAlarmHI setter
// (setter disabled for UI_16 type);
  
// PeepSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// PeepSetpointPreAlarmLOW getter
UI_16 get_PeepSetpointPreAlarmLOW(void);
// PeepSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type);
  
// PeepSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// PeepSetpointAlarmLOW getter
UI_16 get_PeepSetpointAlarmLOW(void);
// PeepSetpointAlarmLOW setter
// (setter disabled for UI_16 type);
  
// PeepSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
// PeepSetpointHysteresisHI getter
UI_16 get_PeepSetpointHysteresisHI(void);
// PeepSetpointHysteresisHI setter
// (setter disabled for UI_16 type);
  
// PeepSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// PeepSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
// PeepSetpointHysteresisLOW getter
UI_16 get_PeepSetpointHysteresisLOW(void);
// PeepSetpointHysteresisLOW setter
// (setter disabled for UI_16 type);
  
// FrequencySetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
// FrequencySetpointPreAlarmHI getter
UI_16 get_FrequencySetpointPreAlarmHI(void);
// FrequencySetpointPreAlarmHI setter
// (setter disabled for UI_16 type);
  
// FrequencySetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
// FrequencySetpointAlarmHI getter
UI_16 get_FrequencySetpointAlarmHI(void);
// FrequencySetpointAlarmHI setter
// (setter disabled for UI_16 type);
  
// FrequencySetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// FrequencySetpointPreAlarmLOW getter
UI_16 get_FrequencySetpointPreAlarmLOW(void);
// FrequencySetpointPreAlarmLOW setter
// (setter disabled for UI_16 type);
  
// FrequencySetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// FrequencySetpointAlarmLOW getter
UI_16 get_FrequencySetpointAlarmLOW(void);
// FrequencySetpointAlarmLOW setter
// (setter disabled for UI_16 type);
  
// FrequencySetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
// FrequencySetpointHysteresisHI getter
UI_16 get_FrequencySetpointHysteresisHI(void);
// FrequencySetpointHysteresisHI setter
// (setter disabled for UI_16 type);
  
// FrequencySetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// FrequencySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
// FrequencySetpointHysteresisLOW getter
UI_16 get_FrequencySetpointHysteresisLOW(void);
// FrequencySetpointHysteresisLOW setter
// (setter disabled for UI_16 type);
  
// TidalVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
// TidalVolumeSetpointPreAlarmHI getter
UI_16 get_TidalVolumeSetpointPreAlarmHI(void);
// TidalVolumeSetpointPreAlarmHI setter
// (setter disabled for UI_16 type);
  
// TidalVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
// TidalVolumeSetpointAlarmHI getter
UI_16 get_TidalVolumeSetpointAlarmHI(void);
// TidalVolumeSetpointAlarmHI setter
// (setter disabled for UI_16 type);
  
// TidalVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// TidalVolumeSetpointPreAlarmLOW getter
UI_16 get_TidalVolumeSetpointPreAlarmLOW(void);
// TidalVolumeSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type);
  
// TidalVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// TidalVolumeSetpointAlarmLOW getter
UI_16 get_TidalVolumeSetpointAlarmLOW(void);
// TidalVolumeSetpointAlarmLOW setter
// (setter disabled for UI_16 type);
  
// TidalVolumeHystersisHI flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeHystersisHI flow synthesis
// (output disabled for UI_16 type);
// TidalVolumeHystersisHI getter
UI_16 get_TidalVolumeHystersisHI(void);
// TidalVolumeHystersisHI setter
// (setter disabled for UI_16 type);
  
// TidalVolumeHystersisLOW flow acquisition
// (input disabled for UI_16 type);
// TidalVolumeHystersisLOW flow synthesis
// (output disabled for UI_16 type);
// TidalVolumeHystersisLOW getter
UI_16 get_TidalVolumeHystersisLOW(void);
// TidalVolumeHystersisLOW setter
// (setter disabled for UI_16 type);
  
// MinuteVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
// MinuteVolumeSetpointPreAlarmHI getter
UI_16 get_MinuteVolumeSetpointPreAlarmHI(void);
// MinuteVolumeSetpointPreAlarmHI setter
// (setter disabled for UI_16 type);
  
// MinuteVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
// MinuteVolumeSetpointAlarmHI getter
UI_16 get_MinuteVolumeSetpointAlarmHI(void);
// MinuteVolumeSetpointAlarmHI setter
// (setter disabled for UI_16 type);
  
// MinuteVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// MinuteVolumeSetpointPreAlarmLOW getter
UI_16 get_MinuteVolumeSetpointPreAlarmLOW(void);
// MinuteVolumeSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type);
  
// MinuteVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// MinuteVolumeSetpointAlarmLOW getter
UI_16 get_MinuteVolumeSetpointAlarmLOW(void);
// MinuteVolumeSetpointAlarmLOW setter
// (setter disabled for UI_16 type);
  
// MinuteVolumeHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type);
// MinuteVolumeHysteresisHI getter
UI_16 get_MinuteVolumeHysteresisHI(void);
// MinuteVolumeHysteresisHI setter
// (setter disabled for UI_16 type);
  
// MinuteVolumeHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// MinuteVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
// MinuteVolumeHysteresisLOW getter
UI_16 get_MinuteVolumeHysteresisLOW(void);
// MinuteVolumeHysteresisLOW setter
// (setter disabled for UI_16 type);
  
// TriggeVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// TriggeVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
// TriggeVolumeSetpointPreAlarmHI getter
UI_16 get_TriggeVolumeSetpointPreAlarmHI(void);
// TriggeVolumeSetpointPreAlarmHI setter
// (setter disabled for UI_16 type);
  
// TriggerVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// TriggerVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
// TriggerVolumeSetpointAlarmHI getter
UI_16 get_TriggerVolumeSetpointAlarmHI(void);
// TriggerVolumeSetpointAlarmHI setter
// (setter disabled for UI_16 type);
  
// TriggerVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// TriggerVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// TriggerVolumeSetpointPreAlarmLOW getter
UI_16 get_TriggerVolumeSetpointPreAlarmLOW(void);
// TriggerVolumeSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type);
  
// TriggerVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// TriggerVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// TriggerVolumeSetpointAlarmLOW getter
UI_16 get_TriggerVolumeSetpointAlarmLOW(void);
// TriggerVolumeSetpointAlarmLOW setter
// (setter disabled for UI_16 type);
  
// TriggerVolumeHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// TriggerVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type);
// TriggerVolumeHysteresisHI getter
UI_16 get_TriggerVolumeHysteresisHI(void);
// TriggerVolumeHysteresisHI setter
// (setter disabled for UI_16 type);
  
// TriggerVolumeHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// TriggerVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
// TriggerVolumeHysteresisLOW getter
UI_16 get_TriggerVolumeHysteresisLOW(void);
// TriggerVolumeHysteresisLOW setter
// (setter disabled for UI_16 type);
  
// BatterySetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type);
// BatterySetpointPreAlarmHI getter
UI_16 get_BatterySetpointPreAlarmHI(void);
// BatterySetpointPreAlarmHI setter
// (setter disabled for UI_16 type);
  
// BatterySetpointAlarmHI flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type);
// BatterySetpointAlarmHI getter
UI_16 get_BatterySetpointAlarmHI(void);
// BatterySetpointAlarmHI setter
// (setter disabled for UI_16 type);
  
// BatterySetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// BatterySetpointPreAlarmLOW getter
UI_16 get_BatterySetpointPreAlarmLOW(void);
// BatterySetpointPreAlarmLOW setter
// (setter disabled for UI_16 type);
  
// BatterySetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type);
// BatterySetpointAlarmLOW getter
UI_16 get_BatterySetpointAlarmLOW(void);
// BatterySetpointAlarmLOW setter
// (setter disabled for UI_16 type);
  
// BatterySetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type);
// BatterySetpointHysteresisHI getter
UI_16 get_BatterySetpointHysteresisHI(void);
// BatterySetpointHysteresisHI setter
// (setter disabled for UI_16 type);
  
// BatterySetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type);
// BatterySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type);
// BatterySetpointHysteresisLOW getter
UI_16 get_BatterySetpointHysteresisLOW(void);
// BatterySetpointHysteresisLOW setter
// (setter disabled for UI_16 type);
  
// Frecuency flow acquisition
// (input disabled for UI_16 type);
// Frecuency flow synthesis
// (output disabled for UI_16 type);
// Frecuency getter
UI_16 get_Frecuency(void);
// Frecuency setter
// (setter disabled for UI_16 type);
  
// Duty_Cycle flow acquisition
// (input disabled for UI_16 type);
// Duty_Cycle flow synthesis
// (output disabled for UI_16 type);
// Duty_Cycle getter
UI_16 get_Duty_Cycle(void);
// Duty_Cycle setter
// (setter disabled for UI_16 type);
  
// HOME_Position flow acquisition
// (input disabled for UI_16 type);
// HOME_Position flow synthesis
// (output disabled for UI_16 type);
// HOME_Position getter
UI_16 get_HOME_Position(void);
// HOME_Position setter
// (setter disabled for UI_16 type);
  
// ABSOLUTE_Position flow acquisition
// (input disabled for UI_16 type);
// ABSOLUTE_Position flow synthesis
// (output disabled for UI_16 type);
// ABSOLUTE_Position getter
UI_16 get_ABSOLUTE_Position(void);
// ABSOLUTE_Position setter
// (setter disabled for UI_16 type);
  
// ABSOLUTE_Velocity flow acquisition
// (input disabled for UI_16 type);
// ABSOLUTE_Velocity flow synthesis
// (output disabled for UI_16 type);
// ABSOLUTE_Velocity getter
UI_16 get_ABSOLUTE_Velocity(void);
// ABSOLUTE_Velocity setter
// (setter disabled for UI_16 type);
  
// RELATIVE_Distance flow acquisition
// (input disabled for UI_16 type);
// RELATIVE_Distance flow synthesis
// (output disabled for UI_16 type);
// RELATIVE_Distance getter
UI_16 get_RELATIVE_Distance(void);
// RELATIVE_Distance setter
// (setter disabled for UI_16 type);
  
// RELATIVE_Velocity flow acquisition
// (input disabled for UI_16 type);
// RELATIVE_Velocity flow synthesis
// (output disabled for UI_16 type);
// RELATIVE_Velocity getter
UI_16 get_RELATIVE_Velocity(void);
// RELATIVE_Velocity setter
// (setter disabled for UI_16 type);
  
// Velocity_Velocity flow acquisition
// (input disabled for UI_16 type);
// Velocity_Velocity flow synthesis
// (output disabled for UI_16 type);
// Velocity_Velocity getter
UI_16 get_Velocity_Velocity(void);
// Velocity_Velocity setter
// (setter disabled for UI_16 type);
  
// JOG_Velocity flow acquisition
// (input disabled for UI_16 type);
// JOG_Velocity flow synthesis
// (output disabled for UI_16 type);
// JOG_Velocity getter
UI_16 get_JOG_Velocity(void);
// JOG_Velocity setter
// (setter disabled for UI_16 type);
  
// EmergencyStop flow acquisition
void acquire_EmergencyStop(void);
// EmergencyStop flow synthesis
// (output disabled for DI type);
// EmergencyStop getter

// EmergencyStop setter
// (setter disabled for DI type);
  
// TurnAxisHwHi flow acquisition
void acquire_TurnAxisHwHi(void);
// TurnAxisHwHi flow synthesis
// (output disabled for DI_pu type);
// TurnAxisHwHi getter
// (getter disabled for DI_pu type);
// TurnAxisHwHi setter
// (setter disabled for DI_pu type);
  
// BeaconLightRed flow acquisition
// (input disabled for DO type);
// BeaconLightRed flow synthesis
void synthesize_BeaconLightRed(void);
// BeaconLightRed getter
// (getter disabled for DO type);
// BeaconLightRed setter

  
// BeaconLightOrange flow acquisition
// (input disabled for DO type);
// BeaconLightOrange flow synthesis
void synthesize_BeaconLightOrange(void);
// BeaconLightOrange getter
// (getter disabled for DO type);
// BeaconLightOrange setter

  
// TurnAxisPulse flow acquisition
// (input disabled for DO type);
// TurnAxisPulse flow synthesis
void synthesize_TurnAxisPulse(void);
// TurnAxisPulse getter
// (getter disabled for DO type);
// TurnAxisPulse setter

  
// TurnAxisDir flow acquisition
// (input disabled for DO type);
// TurnAxisDir flow synthesis
void synthesize_TurnAxisDir(void);
// TurnAxisDir getter
// (getter disabled for DO type);
// TurnAxisDir setter

  
// TurnAxisEnable flow acquisition
// (input disabled for DO type);
// TurnAxisEnable flow synthesis
void synthesize_TurnAxisEnable(void);
// TurnAxisEnable getter
// (getter disabled for DO type);
// TurnAxisEnable setter

  
// BeaconLightGreen flow acquisition
// (input disabled for DO type);
// BeaconLightGreen flow synthesis
void synthesize_BeaconLightGreen(void);
// BeaconLightGreen getter
// (getter disabled for DO type);
// BeaconLightGreen setter

  
// Buzzer flow acquisition
// (input disabled for DO type);
// Buzzer flow synthesis
void synthesize_Buzzer(void);
// Buzzer getter
// (getter disabled for DO type);
// Buzzer setter

  
// PeepValve flow acquisition
// (input disabled for PWM type);
// PeepValve flow synthesis
void synthesize_PeepValve(void);
// PeepValve getter
// (getter not implemented for PWM type);
// PeepValve setter
// (setter not implemented for PWM type);
  
// TurnAxisHwLow flow acquisition
void acquire_TurnAxisHwLow(void);
// TurnAxisHwLow flow synthesis
// (output disabled for DI_pu type);
// TurnAxisHwLow getter
// (getter disabled for DI_pu type);
// TurnAxisHwLow setter
// (setter disabled for DI_pu type);
  
// ServoPend flow acquisition
void acquire_ServoPend(void);
// ServoPend flow synthesis
// (output disabled for DI type);
// ServoPend getter

// ServoPend setter
// (setter disabled for DI type);
  
// ServoAlarm flow acquisition
void acquire_ServoAlarm(void);
// ServoAlarm flow synthesis
// (output disabled for DI_pu type);
// ServoAlarm getter
// (getter disabled for DI_pu type);
// ServoAlarm setter
// (setter disabled for DI_pu type);
  
// AtmosfericPressure flow acquisition
void acquire_AtmosfericPressure(void);
// AtmosfericPressure flow synthesis
// (output disabled for ADC type);
// AtmosfericPressure getter

// AtmosfericPressure setter
// (setter disabled for ADC type);
  
// DiferentialPressure flow acquisition
void acquire_DiferentialPressure(void);
// DiferentialPressure flow synthesis
// (output disabled for ADC type);
// DiferentialPressure getter

// DiferentialPressure setter
// (setter disabled for ADC type);
  
// SerTX flow acquisition
// (input disabled for Bus type);
// SerTX flow synthesis
// (output disabled for Bus type);
// SerTX getter
// (getter disabled for Bus type);
// SerTX setter
// (setter disabled for Bus type);
  
// SerRX flow acquisition
// (input disabled for Bus type);
// SerRX flow synthesis
// (output disabled for Bus type);
// SerRX getter
// (getter disabled for Bus type);
// SerRX setter
// (setter disabled for Bus type);
  
// EmgcyButton flow acquisition
// (input disabled for TBD type);
// EmgcyButton flow synthesis
// (output disabled for TBD type);
// EmgcyButton getter
// (getter disabled for TBD type);
// EmgcyButton setter
// (setter disabled for TBD type);
  
// ApiVersion flow acquisition
// (input disabled for UI_8 type);
// ApiVersion flow synthesis
// (output disabled for UI_8 type);
// ApiVersion getter
UI_8 get_ApiVersion(void);
// ApiVersion setter
// (setter disabled for UI_8 type);
  
// MachineUuid flow acquisition
// (input disabled for Variable type);
// MachineUuid flow synthesis
// (output disabled for Variable type);
// MachineUuid getter
t_machineuuid get_MachineUuid(void);
// MachineUuid setter
// (setter disabled for Variable type);
  
// FirmwareVersion flow acquisition
// (input disabled for UI_16 type);
// FirmwareVersion flow synthesis
// (output disabled for UI_16 type);
// FirmwareVersion getter
UI_16 get_FirmwareVersion(void);
// FirmwareVersion setter
// (setter disabled for UI_16 type);
  
// Uptime15mCounter flow acquisition
// (input disabled for UI_16 type);
// Uptime15mCounter flow synthesis
// (output disabled for UI_16 type);
// Uptime15mCounter getter
UI_16 get_Uptime15mCounter(void);
// Uptime15mCounter setter
// (setter disabled for UI_16 type);
  
// MutedAlarmSeconds flow acquisition
// (input disabled for UI_8 type);
// MutedAlarmSeconds flow synthesis
// (output disabled for UI_8 type);
// MutedAlarmSeconds getter
UI_8 get_MutedAlarmSeconds(void);
// MutedAlarmSeconds setter
// (setter disabled for UI_8 type);
  
// CycleRpmLast30s flow acquisition
// (input disabled for UI_16 type);
// CycleRpmLast30s flow synthesis
// (output disabled for UI_16 type);
// CycleRpmLast30s getter
UI_16 get_CycleRpmLast30s(void);
// CycleRpmLast30s setter
// (setter disabled for UI_16 type);
  
// CycleIndications flow acquisition
// (input disabled for Variable type);
// CycleIndications flow synthesis
// (output disabled for Variable type);
// CycleIndications getter
t_cycleindications get_CycleIndications(void);
// CycleIndications setter
// (setter disabled for Variable type);
  
// CycleDuration flow acquisition
// (input disabled for TimeUI_16 type);
// CycleDuration flow synthesis
// (output disabled for TimeUI_16 type);
// CycleDuration getter
UI_16 get_CycleDuration(void);
// CycleDuration setter
// (setter disabled for TimeUI_16 type);
  
// VentilationFlags flow acquisition
// (input disabled for Variable type);
// VentilationFlags flow synthesis
// (output disabled for Variable type);
// VentilationFlags getter
t_ventilationflags get_VentilationFlags(void);
// VentilationFlags setter
// (setter disabled for Variable type);
  
// VolumeSetting flow acquisition
// (input disabled for UI_16 type);
// VolumeSetting flow synthesis
// (output disabled for UI_16 type);
// VolumeSetting getter
UI_16 get_VolumeSetting(void);
// VolumeSetting setter
// (setter disabled for UI_16 type);
  
// RpmSetting flow acquisition
// (input disabled for UI_16 type);
// RpmSetting flow synthesis
// (output disabled for UI_16 type);
// RpmSetting getter
UI_16 get_RpmSetting(void);
// RpmSetting setter
// (setter disabled for UI_16 type);
  
// RampDegreesSetting flow acquisition
// (input disabled for UI_8 type);
// RampDegreesSetting flow synthesis
// (output disabled for UI_8 type);
// RampDegreesSetting getter
UI_8 get_RampDegreesSetting(void);
// RampDegreesSetting setter
// (setter disabled for UI_8 type);
  
// EiRatioSetting flow acquisition
// (input disabled for UI_8 type);
// EiRatioSetting flow synthesis
// (output disabled for UI_8 type);
// EiRatioSetting getter
UI_8 get_EiRatioSetting(void);
// EiRatioSetting setter
// (setter disabled for UI_8 type);
  
// RecruitTimer flow acquisition
// (input disabled for UI_8 type);
// RecruitTimer flow synthesis
// (output disabled for UI_8 type);
// RecruitTimer getter
UI_8 get_RecruitTimer(void);
// RecruitTimer setter
// (setter disabled for UI_8 type);
  
// IncomingFrame flow acquisition
// (input disabled for Variable type);
// IncomingFrame flow synthesis
// (output disabled for Variable type);
// IncomingFrame getter
t_incomingframe get_IncomingFrame(void);
// IncomingFrame setter
// (setter disabled for Variable type);
  
// OutgoingFrame flow acquisition
// (input disabled for Variable type);
// OutgoingFrame flow synthesis
// (output disabled for Variable type);
// OutgoingFrame getter
t_outgoingframe get_OutgoingFrame(void);
// OutgoingFrame setter
// (setter disabled for Variable type);
  
// Display flow acquisition
// (input disabled for Display type);
// Display flow synthesis
// (output disabled for Display type);
// Display getter
// (getter disabled for Display type);
// Display setter
// (setter disabled for Display type);
  
// DisplayPlus flow acquisition
// (input disabled for Display type);
// DisplayPlus flow synthesis
// (output disabled for Display type);
// DisplayPlus getter
// (getter disabled for Display type);
// DisplayPlus setter
// (setter disabled for Display type);
  
// DisplayButtons flow acquisition
// (input disabled for Keyboard type);
// DisplayButtons flow synthesis
// (output disabled for Keyboard type);
// DisplayButtons getter
// (getter disabled for Keyboard type);
// DisplayButtons setter
// (setter disabled for Keyboard type);
  
// FlowPublic flow acquisition
// (input disabled for UI_16 type);
// FlowPublic flow synthesis
// (output disabled for UI_16 type);
// FlowPublic getter
UI_16 get_FlowPublic(void);
// FlowPublic setter
// (setter disabled for UI_16 type);
  
// Power_EN flow acquisition
// (input disabled for BOOL type);
// Power_EN flow synthesis
// (output disabled for BOOL type);
// Power_EN getter
BOOL  get_Power_EN(void);
// Power_EN setter
// (setter disabled for BOOL type);
  
// Power_Axis flow acquisition
// (input disabled for BOOL type);
// Power_Axis flow synthesis
// (output disabled for BOOL type);
// Power_Axis getter
BOOL  get_Power_Axis(void);
// Power_Axis setter
// (setter disabled for BOOL type);
  
// Power_ENO flow acquisition
// (input disabled for BOOL type);
// Power_ENO flow synthesis
// (output disabled for BOOL type);
// Power_ENO getter
BOOL  get_Power_ENO(void);
// Power_ENO setter
// (setter disabled for BOOL type);
  
// Reset_EN flow acquisition
// (input disabled for BOOL type);
// Reset_EN flow synthesis
// (output disabled for BOOL type);
// Reset_EN getter
BOOL  get_Reset_EN(void);
// Reset_EN setter
// (setter disabled for BOOL type);
  
// Reset_Axis flow acquisition
// (input disabled for BOOL type);
// Reset_Axis flow synthesis
// (output disabled for BOOL type);
// Reset_Axis getter
BOOL  get_Reset_Axis(void);
// Reset_Axis setter
// (setter disabled for BOOL type);
  
// Reset_ENO flow acquisition
// (input disabled for BOOL type);
// Reset_ENO flow synthesis
// (output disabled for BOOL type);
// Reset_ENO getter
BOOL  get_Reset_ENO(void);
// Reset_ENO setter
// (setter disabled for BOOL type);
  
// Home_EN flow acquisition
// (input disabled for BOOL type);
// Home_EN flow synthesis
// (output disabled for BOOL type);
// Home_EN getter
BOOL  get_Home_EN(void);
// Home_EN setter
// (setter disabled for BOOL type);
  
// Home_Axis flow acquisition
// (input disabled for BOOL type);
// Home_Axis flow synthesis
// (output disabled for BOOL type);
// Home_Axis getter
BOOL  get_Home_Axis(void);
// Home_Axis setter
// (setter disabled for BOOL type);
  
// Home_ENO flow acquisition
// (input disabled for BOOL type);
// Home_ENO flow synthesis
// (output disabled for BOOL type);
// Home_ENO getter
BOOL  get_Home_ENO(void);
// Home_ENO setter
// (setter disabled for BOOL type);
  
// Halt_EN flow acquisition
// (input disabled for BOOL type);
// Halt_EN flow synthesis
// (output disabled for BOOL type);
// Halt_EN getter
BOOL  get_Halt_EN(void);
// Halt_EN setter
// (setter disabled for BOOL type);
  
// Halt_Axis flow acquisition
// (input disabled for BOOL type);
// Halt_Axis flow synthesis
// (output disabled for BOOL type);
// Halt_Axis getter
BOOL  get_Halt_Axis(void);
// Halt_Axis setter
// (setter disabled for BOOL type);
  
// Halt_ENO flow acquisition
// (input disabled for BOOL type);
// Halt_ENO flow synthesis
// (output disabled for BOOL type);
// Halt_ENO getter
BOOL  get_Halt_ENO(void);
// Halt_ENO setter
// (setter disabled for BOOL type);
  
// Absolute_EN flow acquisition
// (input disabled for BOOL type);
// Absolute_EN flow synthesis
// (output disabled for BOOL type);
// Absolute_EN getter
BOOL  get_Absolute_EN(void);
// Absolute_EN setter
// (setter disabled for BOOL type);
  
// Absolute_Axis flow acquisition
// (input disabled for BOOL type);
// Absolute_Axis flow synthesis
// (output disabled for BOOL type);
// Absolute_Axis getter
BOOL  get_Absolute_Axis(void);
// Absolute_Axis setter
// (setter disabled for BOOL type);
  
// Absolute_ENO flow acquisition
// (input disabled for BOOL type);
// Absolute_ENO flow synthesis
// (output disabled for BOOL type);
// Absolute_ENO getter
BOOL  get_Absolute_ENO(void);
// Absolute_ENO setter
// (setter disabled for BOOL type);
  
// Relative_EN flow acquisition
// (input disabled for BOOL type);
// Relative_EN flow synthesis
// (output disabled for BOOL type);
// Relative_EN getter
BOOL  get_Relative_EN(void);
// Relative_EN setter
// (setter disabled for BOOL type);
  
// Relative_Axis flow acquisition
// (input disabled for BOOL type);
// Relative_Axis flow synthesis
// (output disabled for BOOL type);
// Relative_Axis getter
BOOL  get_Relative_Axis(void);
// Relative_Axis setter
// (setter disabled for BOOL type);
  
// Relative_ENO flow acquisition
// (input disabled for BOOL type);
// Relative_ENO flow synthesis
// (output disabled for BOOL type);
// Relative_ENO getter
BOOL  get_Relative_ENO(void);
// Relative_ENO setter
// (setter disabled for BOOL type);
  
// Velocity_EN flow acquisition
// (input disabled for BOOL type);
// Velocity_EN flow synthesis
// (output disabled for BOOL type);
// Velocity_EN getter
BOOL  get_Velocity_EN(void);
// Velocity_EN setter
// (setter disabled for BOOL type);
  
// Velocity_Axis flow acquisition
// (input disabled for BOOL type);
// Velocity_Axis flow synthesis
// (output disabled for BOOL type);
// Velocity_Axis getter
BOOL  get_Velocity_Axis(void);
// Velocity_Axis setter
// (setter disabled for BOOL type);
  
// Velocity_ENO flow acquisition
// (input disabled for BOOL type);
// Velocity_ENO flow synthesis
// (output disabled for BOOL type);
// Velocity_ENO getter
BOOL  get_Velocity_ENO(void);
// Velocity_ENO setter
// (setter disabled for BOOL type);
  
// Jog_EN flow acquisition
// (input disabled for BOOL type);
// Jog_EN flow synthesis
// (output disabled for BOOL type);
// Jog_EN getter
BOOL  get_Jog_EN(void);
// Jog_EN setter
// (setter disabled for BOOL type);
  
// Jog_Axis flow acquisition
// (input disabled for BOOL type);
// Jog_Axis flow synthesis
// (output disabled for BOOL type);
// Jog_Axis getter
BOOL  get_Jog_Axis(void);
// Jog_Axis setter
// (setter disabled for BOOL type);
  
// Jog_ENO flow acquisition
// (input disabled for BOOL type);
// Jog_ENO flow synthesis
// (output disabled for BOOL type);
// Jog_ENO getter
BOOL  get_Jog_ENO(void);
// Jog_ENO setter
// (setter disabled for BOOL type);
  
// EN flow acquisition
// (input disabled for BOOL type);
// EN flow synthesis
// (output disabled for BOOL type);
// EN getter
BOOL  get_EN(void);
// EN setter
// (setter disabled for BOOL type);
  
// EN0 flow acquisition
// (input disabled for BOOL type);
// EN0 flow synthesis
// (output disabled for BOOL type);
// EN0 getter
BOOL  get_EN0(void);
// EN0 setter
// (setter disabled for BOOL type);
  
// CycleStartTime flow acquisition

// CycleStartTime flow synthesis

// CycleStartTime getter
UI_32 get_CycleStartTime(void);
// CycleStartTime setter
// (setter disabled for TimeUI_32 type);

#endif /* _DRE_H */