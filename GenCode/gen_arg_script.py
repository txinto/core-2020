import os, distutils.dir_util

import sys

numargs = len(sys.argv)
print 'Number of arguments:', numargs , 'arguments.'
print 'Argument List:', str(sys.argv)

if (numargs >=2):
    replacement = sys.argv[1]
    if (numargs >=3):
        typestr = sys.argv[2]
        if (typestr == "-"):
            typestr = ""

        if (numargs >=4):
            replaceabbrev = sys.argv[3]
            if (replaceabbrev == "-"):
                replaceabbrev = replacement

        else:
            replaceabbrev = replacement
    else:
        typestr = ""

else:
    replacement = "S1"
    typestr = ""
    replaceabbrev = "s1"


toreplace = "__replace__"
toreplaceabbrev = "__replaceabbrev__"
src = "./_replace_"
template = "./" + toreplace + typestr + "_template"

if not os.path.isdir(template):
    template = "./" + toreplaceabbrev + typestr + "_template"
    dst = "../"+replaceabbrev + typestr
else:
    dst = "../"+replacement + typestr

distutils.dir_util.copy_tree(template, src)
for dname, dirs, files in os.walk(src):
    for fname in files:
        fpath = os.path.join(dname, fname)
        with open(fpath) as f:
            s = f.read()
        s = s.replace(toreplace, replacement)
        s = s.replace(toreplaceabbrev, replaceabbrev)
        with open(fpath, "w") as f:
            f.write(s)

        newpath = fpath.replace(toreplace, replacement).replace(toreplaceabbrev, replaceabbrev).replace(".cpp_",".cpp")
        os.rename(fpath, newpath)


os.rename(src,dst)
