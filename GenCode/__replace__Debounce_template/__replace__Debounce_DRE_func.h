#ifndef _{__replace__}DEBOUNCE_DRE_FUNC_H
#define _{__replace__}DEBOUNCE_DRE_FUNC_H

void setup_{__replace__}Raw(void);
void acquire_{__replace__}Raw(void);
void setup_{__replace__}Filt(void);

#endif /* _{__replace__}DEBOUNCE_DRE_FUNC_H */
