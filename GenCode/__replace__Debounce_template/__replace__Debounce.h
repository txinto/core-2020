#ifndef {__replace__}_DEBOUNCE_H
#define {__replace__}_DEBOUNCE_H

#include "../CommonLibrary/CommonLibrary.h"

void {__replace__}DebounceInit(void);
void {__replace__}Debounce(void); /*!< Manager that commands the mode detection */

/*** Manually added definitions ***/
#define T_{__replace__}_RANGE_STABILITY_TIME ((uint16_t)CALC_CYCLE_COUNT_FOR_TIME(50000))  /*!< Time to filter the appearance of a new mode */

#endif /* {__replace__}_DEBOUNCE_H */
