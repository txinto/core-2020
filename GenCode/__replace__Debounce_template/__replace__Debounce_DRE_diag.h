#ifndef _{__replace__}DEBOUNCE_DRE_DIAG_H
#define _{__replace__}DEBOUNCE_DRE_DIAG_H
    /// Input
    bool enable_{__replace__}Raw;
    uint16_t {__replace__}Raw;
    /// Output
    bool enable_{__replace__}Filt;
    uint16_t {__replace__}Filt;
#endif /* _{__replace__}DEBOUNCE_DRE_DIAG_H */
