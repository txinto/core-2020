#ifndef {__replace__}_MODE_H
#define {__replace__}_MODE_H

#include "../CommonLibrary/CommonLibrary.h"

#include "{__replace__}Mode_autogen.h"

/*!< Time to filter the appearance of a new mode */
#define T_{__replace__}_RANGE_STABILITY_TIME ((uint16_t)CALC_CYCLE_COUNT_FOR_TIME(50000))  

/**** FSM includes ****/
void {__replace__}ModeInit(void);
void {__replace__}Mode(void); /*!< Manager that commands the mode detection */


#endif /* {__replace__}_MODE_H */
