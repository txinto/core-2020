#ifndef _{__replace__}MODE_DRE_DIAG_H
#define _{__replace__}MODE_DRE_DIAG_H
    /// Input
    bool enable_{__replace__}Sense;
    uint16_t {__replace__}Sense;
    /// Output
    bool enable_{__replace__}Mode;
    t_enum_{__replace__}Mode {__replace__}Mode;
#endif /* _{__replace__}MODE_DRE_DIAG_H */
