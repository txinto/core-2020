#ifndef _{__replace__}MODE_DRE_H
#define _{__replace__}MODE_DRE_H
    /// Input
    uint16_t {__replace__}Sense;
    /// Output
    t_enum_{__replace__}Mode {__replace__}Mode;
#endif /* _{__replace__}MODE_DRE_H */
