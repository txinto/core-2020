
/**
 Vector with the frontier values for discretization
 */
uint16_t {__replace__}Mode_values[] = {
    {__replace__}_LOW_CAL,
    {__replace__}_DEGRADED_LOW_CAL,
    {__replace__}_NORMAL_CAL,
    {__replace__}_DEGRADED_HIGH_CAL,
    {__replace__}_HIGH_CAL




};


/**
 Vector with the frontier values for discretization,
 * with hystheresis value applied
 */
uint16_t {__replace__}Mode_hyst_values[] = {
    {__replace__}_LOW_HYST_CAL,
    {__replace__}_DEGRADED_LOW_HYST_CAL,
    {__replace__}_NORMAL_HYST_CAL,
    {__replace__}_DEGRADED_HIGH_HYST_CAL,
    {__replace__}_HIGH_HYST_CAL




};
