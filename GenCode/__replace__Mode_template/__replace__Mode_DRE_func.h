#ifndef _{__replace__}MODE_DRE_FUNC_H
#define _{__replace__}MODE_DRE_FUNC_H

void setup_{__replace__}Sense(void);
void acquire_{__replace__}Sense(void);
void setup_{__replace__}Mode(void);

#endif /* _{__replace__}MODE_DRE_FUNC_H */
