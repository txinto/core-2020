#include <core_cfg.h>
#include <core_pinout.h>
#include <DRE.h>

// --- DRE data structure declaration ---
t_dre dre;
t_diag diag;

// --- DRE flow initialization functions ---


  
// STATUS_LED flow acquisition
// (setup input disabled for LED type)
// STATUS_LED flow synthesis
// (output disabled for LED type)

  
// statusLED flow acquisition
// (setup input disabled for DO type)
// statusLED flow synthesis
void setup_statusLED_output(void){ 
pinMode(PIN_statusLED, OUTPUT); 
};

  
// dbgLedBlinkTimer flow acquisition
// (setup input disabled for Variable type)
// dbgLedBlinkTimer flow synthesis
// (output disabled for Variable type)

  
// DBG_LED flow acquisition
// (setup input disabled for Variable type)
// DBG_LED flow synthesis
// (output disabled for Variable type)

  
// SDA flow acquisition
// (setup input disabled for Bus type)
// SDA flow synthesis
// (output disabled for Bus type)

  
// SCL flow acquisition
// (setup input disabled for Bus type)
// SCL flow synthesis
// (output disabled for Bus type)

  
// ResetRelay flow acquisition
// (setup input disabled for DO type)
// ResetRelay flow synthesis
void setup_ResetRelay_output(void){ 
pinMode(PIN_ResetRelay, OUTPUT); 
};

  
// LCD_DB7 flow acquisition
// (setup input disabled for DO type)
// LCD_DB7 flow synthesis
void setup_LCD_DB7_output(void){ 
pinMode(PIN_LCD_DB7, OUTPUT); 
};

  
// LCD_DB6 flow acquisition
// (setup input disabled for DO type)
// LCD_DB6 flow synthesis
void setup_LCD_DB6_output(void){ 
pinMode(PIN_LCD_DB6, OUTPUT); 
};

  
// LCD_DB5 flow acquisition
// (setup input disabled for DO type)
// LCD_DB5 flow synthesis
void setup_LCD_DB5_output(void){ 
pinMode(PIN_LCD_DB5, OUTPUT); 
};

  
// LCD_DB4 flow acquisition
// (setup input disabled for DO type)
// LCD_DB4 flow synthesis
void setup_LCD_DB4_output(void){ 
pinMode(PIN_LCD_DB4, OUTPUT); 
};

  
// LCD_DB3 flow acquisition
// (setup input disabled for DO type)
// LCD_DB3 flow synthesis
void setup_LCD_DB3_output(void){ 
pinMode(PIN_LCD_DB3, OUTPUT); 
};

  
// LCD_DB2 flow acquisition
// (setup input disabled for DO type)
// LCD_DB2 flow synthesis
void setup_LCD_DB2_output(void){ 
pinMode(PIN_LCD_DB2, OUTPUT); 
};

  
// LCD_DB1 flow acquisition
// (setup input disabled for DO type)
// LCD_DB1 flow synthesis
void setup_LCD_DB1_output(void){ 
pinMode(PIN_LCD_DB1, OUTPUT); 
};

  
// LCD_DB0 flow acquisition
// (setup input disabled for DO type)
// LCD_DB0 flow synthesis
void setup_LCD_DB0_output(void){ 
pinMode(PIN_LCD_DB0, OUTPUT); 
};

  
// BME_CS2 flow acquisition
// (setup input disabled for Bus type)
// BME_CS2 flow synthesis
// (output disabled for Bus type)

  
// BME_MISO flow acquisition
// (setup input disabled for Bus type)
// BME_MISO flow synthesis
// (output disabled for Bus type)

  
// BME_MOSI flow acquisition
// (setup input disabled for Bus type)
// BME_MOSI flow synthesis
// (output disabled for Bus type)

  
// BME_SCK flow acquisition
// (setup input disabled for Bus type)
// BME_SCK flow synthesis
// (output disabled for Bus type)

  
// BME_CS1 flow acquisition
// (setup input disabled for Bus type)
// BME_CS1 flow synthesis
// (output disabled for Bus type)

  
// _5V flow acquisition
// (setup input disabled for Power type)
// _5V flow synthesis
// (output disabled for Power type)

  
// GND flow acquisition
// (setup input disabled for Power type)
// GND flow synthesis
// (output disabled for Power type)

  
// INT_FS flow acquisition
// (setup input disabled for Variable type)
// INT_FS flow synthesis
// (output disabled for Variable type)

  
// INT_FC flow acquisition
// (setup input disabled for Variable type)
// INT_FC flow synthesis
// (output disabled for Variable type)

  
// INT_HS flow acquisition
// (setup input disabled for Variable type)
// INT_HS flow synthesis
// (output disabled for Variable type)

  
// INT_HC flow acquisition
// (setup input disabled for Variable type)
// INT_HC flow synthesis
// (output disabled for Variable type)

  
// SystemDate_W flow acquisition
// (setup input disabled for Variable type)
// SystemDate_W flow synthesis
// (output disabled for Variable type)

  
// SystemTime_W flow acquisition
// (setup input disabled for Variable type)
// SystemTime_W flow synthesis
// (output disabled for Variable type)

  
// PeakFactor flow acquisition
// (setup input disabled for Variable type)
// PeakFactor flow synthesis
// (output disabled for Variable type)

  
// PlateauFactor flow acquisition
// (setup input disabled for Variable type)
// PlateauFactor flow synthesis
// (output disabled for Variable type)

  
// PeepFactor flow acquisition
// (setup input disabled for Variable type)
// PeepFactor flow synthesis
// (output disabled for Variable type)

  
// FrequencyFactor flow acquisition
// (setup input disabled for Variable type)
// FrequencyFactor flow synthesis
// (output disabled for Variable type)

  
// TidalVolumenFactor flow acquisition
// (setup input disabled for Variable type)
// TidalVolumenFactor flow synthesis
// (output disabled for Variable type)

  
// MinimumVolumeFactor flow acquisition
// (setup input disabled for Variable type)
// MinimumVolumeFactor flow synthesis
// (output disabled for Variable type)

  
// TriggerFactor flow acquisition
// (setup input disabled for Variable type)
// TriggerFactor flow synthesis
// (output disabled for Variable type)

  
// BatteryLevelTrigger flow acquisition
// (setup input disabled for Variable type)
// BatteryLevelTrigger flow synthesis
// (output disabled for Variable type)

  
// PatientType flow acquisition
// (setup input disabled for Variable type)
// PatientType flow synthesis
// (output disabled for Variable type)

  
// PatientWeight flow acquisition
// (setup input disabled for Variable type)
// PatientWeight flow synthesis
// (output disabled for Variable type)

  
// PatientHeight flow acquisition
// (setup input disabled for Variable type)
// PatientHeight flow synthesis
// (output disabled for Variable type)

  
// TidalVolume flow acquisition
// (setup input disabled for Variable type)
// TidalVolume flow synthesis
// (output disabled for Variable type)

  
// PIDMode flow acquisition
// (setup input disabled for Variable type)
// PIDMode flow synthesis
// (output disabled for Variable type)

  
// PIDState flow acquisition
// (setup input disabled for Variable type)
// PIDState flow synthesis
// (output disabled for Variable type)

  
// PIDErrorBits flow acquisition
// (setup input disabled for Variable type)
// PIDErrorBits flow synthesis
// (output disabled for Variable type)

  
// Alarms flow acquisition
// (setup input disabled for Variable type)
// Alarms flow synthesis
// (output disabled for Variable type)

  
// Status flow acquisition
// (setup input disabled for Variable type)
// Status flow synthesis
// (output disabled for Variable type)

  
// POWER_StartMode flow acquisition
// (setup input disabled for Variable type)
// POWER_StartMode flow synthesis
// (output disabled for Variable type)

  
// POWER_StopMode flow acquisition
// (setup input disabled for Variable type)
// POWER_StopMode flow synthesis
// (output disabled for Variable type)

  
// HOME_Mode flow acquisition
// (setup input disabled for Variable type)
// HOME_Mode flow synthesis
// (output disabled for Variable type)

  
// SetWriteData flow acquisition
// (setup input disabled for BOOL type)
// SetWriteData flow synthesis
// (output disabled for BOOL type)

  
// SetTime flow acquisition
// (setup input disabled for BOOL type)
// SetTime flow synthesis
// (output disabled for BOOL type)

  
// Trigger flow acquisition
// (setup input disabled for BOOL type)
// Trigger flow synthesis
// (output disabled for BOOL type)

  
// PeakPBPlus flow acquisition
// (setup input disabled for BOOL type)
// PeakPBPlus flow synthesis
// (output disabled for BOOL type)

  
// PeakPBMinus flow acquisition
// (setup input disabled for BOOL type)
// PeakPBMinus flow synthesis
// (output disabled for BOOL type)

  
// PeepPBPlus flow acquisition
// (setup input disabled for BOOL type)
// PeepPBPlus flow synthesis
// (output disabled for BOOL type)

  
// PeepPBMinus flow acquisition
// (setup input disabled for BOOL type)
// PeepPBMinus flow synthesis
// (output disabled for BOOL type)

  
// FrPBPlus flow acquisition
// (setup input disabled for BOOL type)
// FrPBPlus flow synthesis
// (output disabled for BOOL type)

  
// FrPBMinus flow acquisition
// (setup input disabled for BOOL type)
// FrPBMinus flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentOn flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentOn flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentOff flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentOff flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentMem flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentMem flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentRunning flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentRunning flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentEnding flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentEnding flow synthesis
// (output disabled for BOOL type)

  
// RecruitmentEnd flow acquisition
// (setup input disabled for BOOL type)
// RecruitmentEnd flow synthesis
// (output disabled for BOOL type)

  
// ErrorAck flow acquisition
// (setup input disabled for BOOL type)
// ErrorAck flow synthesis
// (output disabled for BOOL type)

  
// Reset flow acquisition
// (setup input disabled for BOOL type)
// Reset flow synthesis
// (output disabled for BOOL type)

  
// PIDError flow acquisition
// (setup input disabled for BOOL type)
// PIDError flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeakDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeakDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeakDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeakDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeakDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeakInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeakInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeakInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeakInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeakInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PlateauDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PlateauDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PlateauDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PlateauDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PlateauInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PlateauInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PlateauInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PlateauInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PlateauInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeepDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeepDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeepDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeepDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeepInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// PeepInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeepInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// PeepInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// PeepInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// FrequencyDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// FrequencyDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// FrequencyDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// FrequencyDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// FrequencyInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// FrequencyInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// FrequencyInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// FrequencyInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// FrequencyInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TidalVolumeInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TidalVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// MinuteVolumeInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// MinuteVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TriggerDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TriggerDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TriggerDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TriggerDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TriggerInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// TriggerInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TriggerInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// TriggerInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// TriggerInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// BatteryDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// BatteryDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// BatteryDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryDisplayAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// BatteryDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockPreAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// BatteryInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockAlarmHI flow acquisition
// (setup input disabled for BOOL type)
// BatteryInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockPreAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// BatteryInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// BatteryInterlockAlarmLOW flow acquisition
// (setup input disabled for BOOL type)
// BatteryInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)

  
// Enable flow acquisition
// (setup input disabled for BOOL type)
// Enable flow synthesis
// (output disabled for BOOL type)

  
// Busy flow acquisition
// (setup input disabled for BOOL type)
// Busy flow synthesis
// (output disabled for BOOL type)

  
// POWER_Enable flow acquisition
// (setup input disabled for BOOL type)
// POWER_Enable flow synthesis
// (output disabled for BOOL type)

  
// POWER_Status flow acquisition
// (setup input disabled for BOOL type)
// POWER_Status flow synthesis
// (output disabled for BOOL type)

  
// POWER_Error flow acquisition
// (setup input disabled for BOOL type)
// POWER_Error flow synthesis
// (output disabled for BOOL type)

  
// RESET_Execute flow acquisition
// (setup input disabled for BOOL type)
// RESET_Execute flow synthesis
// (output disabled for BOOL type)

  
// RESET_Done flow acquisition
// (setup input disabled for BOOL type)
// RESET_Done flow synthesis
// (output disabled for BOOL type)

  
// RESET_Error flow acquisition
// (setup input disabled for BOOL type)
// RESET_Error flow synthesis
// (output disabled for BOOL type)

  
// HOME_Execute flow acquisition
// (setup input disabled for BOOL type)
// HOME_Execute flow synthesis
// (output disabled for BOOL type)

  
// HOME_Done flow acquisition
// (setup input disabled for BOOL type)
// HOME_Done flow synthesis
// (output disabled for BOOL type)

  
// HOME_Error flow acquisition
// (setup input disabled for BOOL type)
// HOME_Error flow synthesis
// (output disabled for BOOL type)

  
// HALT_Execute flow acquisition
// (setup input disabled for BOOL type)
// HALT_Execute flow synthesis
// (output disabled for BOOL type)

  
// HALT_Done flow acquisition
// (setup input disabled for BOOL type)
// HALT_Done flow synthesis
// (output disabled for BOOL type)

  
// HALT_Error flow acquisition
// (setup input disabled for BOOL type)
// HALT_Error flow synthesis
// (output disabled for BOOL type)

  
// ABSOLUTE_Execute flow acquisition
// (setup input disabled for BOOL type)
// ABSOLUTE_Execute flow synthesis
// (output disabled for BOOL type)

  
// ABSOLUTE_Done flow acquisition
// (setup input disabled for BOOL type)
// ABSOLUTE_Done flow synthesis
// (output disabled for BOOL type)

  
// ABSOLUTE_Error flow acquisition
// (setup input disabled for BOOL type)
// ABSOLUTE_Error flow synthesis
// (output disabled for BOOL type)

  
// RELATIVE_Execute flow acquisition
// (setup input disabled for BOOL type)
// RELATIVE_Execute flow synthesis
// (output disabled for BOOL type)

  
// RELATIVE_Done flow acquisition
// (setup input disabled for BOOL type)
// RELATIVE_Done flow synthesis
// (output disabled for BOOL type)

  
// RELATIVE_Error flow acquisition
// (setup input disabled for BOOL type)
// RELATIVE_Error flow synthesis
// (output disabled for BOOL type)

  
// Velocity_Execute flow acquisition
// (setup input disabled for BOOL type)
// Velocity_Execute flow synthesis
// (output disabled for BOOL type)

  
// Velocity_Current flow acquisition
// (setup input disabled for BOOL type)
// Velocity_Current flow synthesis
// (output disabled for BOOL type)

  
// Velocity_invelocity flow acquisition
// (setup input disabled for BOOL type)
// Velocity_invelocity flow synthesis
// (output disabled for BOOL type)

  
// Velocity_Error flow acquisition
// (setup input disabled for BOOL type)
// Velocity_Error flow synthesis
// (output disabled for BOOL type)

  
// JOG_Forward flow acquisition
// (setup input disabled for BOOL type)
// JOG_Forward flow synthesis
// (output disabled for BOOL type)

  
// JOG_Backward flow acquisition
// (setup input disabled for BOOL type)
// JOG_Backward flow synthesis
// (output disabled for BOOL type)

  
// JOG_Invelocity flow acquisition
// (setup input disabled for BOOL type)
// JOG_Invelocity flow synthesis
// (output disabled for BOOL type)

  
// JOG_Error flow acquisition
// (setup input disabled for BOOL type)
// JOG_Error flow synthesis
// (output disabled for BOOL type)

  
// Permissions flow acquisition
// (setup input disabled for BOOL type)
// Permissions flow synthesis
// (output disabled for BOOL type)

  
// Operation_ON flow acquisition
// (setup input disabled for BOOL type)
// Operation_ON flow synthesis
// (output disabled for BOOL type)

  
// Emergency flow acquisition
// (setup input disabled for BOOL type)
// Emergency flow synthesis
// (output disabled for BOOL type)

  
// GeneralAck flow acquisition
// (setup input disabled for BOOL type)
// GeneralAck flow synthesis
// (output disabled for BOOL type)

  
// MotorAck flow acquisition
// (setup input disabled for BOOL type)
// MotorAck flow synthesis
// (output disabled for BOOL type)

  
// ProcessAck flow acquisition
// (setup input disabled for BOOL type)
// ProcessAck flow synthesis
// (output disabled for BOOL type)

  
// GeneralReset flow acquisition
// (setup input disabled for BOOL type)
// GeneralReset flow synthesis
// (output disabled for BOOL type)

  
// MotorReset flow acquisition
// (setup input disabled for BOOL type)
// MotorReset flow synthesis
// (output disabled for BOOL type)

  
// ProcessReset flow acquisition
// (setup input disabled for BOOL type)
// ProcessReset flow synthesis
// (output disabled for BOOL type)

  
// PeakValue flow acquisition
// (setup input disabled for UI_16 type)
// PeakValue flow synthesis
// (output disabled for UI_16 type)

  
// PlateauValue flow acquisition
// (setup input disabled for UI_16 type)
// PlateauValue flow synthesis
// (output disabled for UI_16 type)

  
// PeepValue flow acquisition
// (setup input disabled for UI_16 type)
// PeepValue flow synthesis
// (output disabled for UI_16 type)

  
// FrequencyValue flow acquisition
// (setup input disabled for UI_16 type)
// FrequencyValue flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeValue flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeValue flow synthesis
// (output disabled for UI_16 type)

  
// MinumValue flow acquisition
// (setup input disabled for UI_16 type)
// MinumValue flow synthesis
// (output disabled for UI_16 type)

  
// DrivingPreassure flow acquisition
// (setup input disabled for UI_16 type)
// DrivingPreassure flow synthesis
// (output disabled for UI_16 type)

  
// BatteryVoltage flow acquisition
// (setup input disabled for UI_16 type)
// BatteryVoltage flow synthesis
// (output disabled for UI_16 type)

  
// BatteryCapacity flow acquisition
// (setup input disabled for UI_16 type)
// BatteryCapacity flow synthesis
// (output disabled for UI_16 type)

  
// Pressure1 flow acquisition
// (setup input disabled for UI_16 type)
// Pressure1 flow synthesis
// (output disabled for UI_16 type)

  
// Pressure2 flow acquisition
// (setup input disabled for UI_16 type)
// Pressure2 flow synthesis
// (output disabled for UI_16 type)

  
// VolumeAccumulated flow acquisition
// (setup input disabled for UI_16 type)
// VolumeAccumulated flow synthesis
// (output disabled for UI_16 type)

  
// Flow flow acquisition
// (setup input disabled for UI_16 type)
// Flow flow synthesis
// (output disabled for UI_16 type)

  
// SystemDateRetVal flow acquisition
// (setup input disabled for UI_16 type)
// SystemDateRetVal flow synthesis
// (output disabled for UI_16 type)

  
// SystemDateOut flow acquisition
// (setup input disabled for DateTime type)
// SystemDateOut flow synthesis
// (output disabled for DateTime type)

  
// LocalDateRetVal flow acquisition
// (setup input disabled for UI_16 type)
// LocalDateRetVal flow synthesis
// (output disabled for UI_16 type)

  
// LocalDateOut flow acquisition
// (setup input disabled for DateTime type)
// LocalDateOut flow synthesis
// (output disabled for DateTime type)

  
// WriteDateTime flow acquisition
// (setup input disabled for DateTime type)
// WriteDateTime flow synthesis
// (output disabled for DateTime type)

  
// WriteDateOut flow acquisition
// (setup input disabled for DateTime type)
// WriteDateOut flow synthesis
// (output disabled for DateTime type)

  
// StatuSetTime flow acquisition
// (setup input disabled for DateTime type)
// StatuSetTime flow synthesis
// (output disabled for DateTime type)

  
// PeakMax flow acquisition
// (setup input disabled for UI_16 type)
// PeakMax flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PeakMin flow acquisition
// (setup input disabled for UI_16 type)
// PeakMin flow synthesis
// (output disabled for UI_16 type)

  
// PlateauMax flow acquisition
// (setup input disabled for UI_16 type)
// PlateauMax flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// PlateauSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PlateauMin flow acquisition
// (setup input disabled for UI_16 type)
// PlateauMin flow synthesis
// (output disabled for UI_16 type)

  
// PeepMax flow acquisition
// (setup input disabled for UI_16 type)
// PeepMax flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PeepMin flow acquisition
// (setup input disabled for UI_16 type)
// PeepMin flow synthesis
// (output disabled for UI_16 type)

  
// FrequencyMax flow acquisition
// (setup input disabled for UI_16 type)
// FrequencyMax flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpoint flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpoint flow synthesis
// (output disabled for UI_16 type)

  
// FrequencyMin flow acquisition
// (setup input disabled for UI_16 type)
// FrequencyMin flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumenMax flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumenMax flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumenSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumenSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumenMin flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumenMin flow synthesis
// (output disabled for UI_16 type)

  
// MinimumVolumeMax flow acquisition
// (setup input disabled for UI_16 type)
// MinimumVolumeMax flow synthesis
// (output disabled for UI_16 type)

  
// MinimumVolumeSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// MinimumVolumeSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// MinimumVolumeMin flow acquisition
// (setup input disabled for UI_16 type)
// MinimumVolumeMin flow synthesis
// (output disabled for UI_16 type)

  
// TriggerSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// TriggerSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// RecruitmentTimer flow acquisition
// (setup input disabled for TimeUI_16 type)
// RecruitmentTimer flow synthesis
// (output disabled for TimeUI_16 type)

  
// RecruitmentElap flow acquisition
// (setup input disabled for TimeUI_16 type)
// RecruitmentElap flow synthesis
// (output disabled for TimeUI_16 type)

  
// TimeStart flow acquisition
// (setup input disabled for DateTime type)
// TimeStart flow synthesis
// (output disabled for DateTime type)

  
// TimeEnd flow acquisition
// (setup input disabled for DateTime type)
// TimeEnd flow synthesis
// (output disabled for DateTime type)

  
// FlowSetpoint flow acquisition
// (setup input disabled for UI_16 type)
// FlowSetpoint flow synthesis
// (output disabled for UI_16 type)

  
// PIDOutReal flow acquisition
// (setup input disabled for FL_16 type)
// PIDOutReal flow synthesis
// (output disabled for FL_16 type)

  
// PIDOutInt flow acquisition
// (setup input disabled for UI_16 type)
// PIDOutInt flow synthesis
// (output disabled for UI_16 type)

  
// PIDOutPWM flow acquisition
// (setup input disabled for UI_16 type)
// PIDOutPWM flow synthesis
// (output disabled for UI_16 type)

  
// PIDOutMotor flow acquisition
// (setup input disabled for UI_16 type)
// PIDOutMotor flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// PeakSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeakSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PlateauSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PlateauSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PlateauSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PlateauSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PlateaukSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PlateaukSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PlateaukSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// PlateaukSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// PlateaukSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// PlateaukSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// PeepSetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// PeepSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// FrequencySetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// FrequencySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeHystersisHI flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeHystersisHI flow synthesis
// (output disabled for UI_16 type)

  
// TidalVolumeHystersisLOW flow acquisition
// (setup input disabled for UI_16 type)
// TidalVolumeHystersisLOW flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// MinuteVolumeHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// MinuteVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// TriggeVolumeSetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// TriggeVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeSetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// TriggerVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeSetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// TriggerVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeSetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// TriggerVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// TriggerVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// TriggerVolumeHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// TriggerVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointPreAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointAlarmHI flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointPreAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointAlarmLOW flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointHysteresisHI flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)

  
// BatterySetpointHysteresisLOW flow acquisition
// (setup input disabled for UI_16 type)
// BatterySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)

  
// Frecuency flow acquisition
// (setup input disabled for UI_16 type)
// Frecuency flow synthesis
// (output disabled for UI_16 type)

  
// Duty_Cycle flow acquisition
// (setup input disabled for UI_16 type)
// Duty_Cycle flow synthesis
// (output disabled for UI_16 type)

  
// HOME_Position flow acquisition
// (setup input disabled for UI_16 type)
// HOME_Position flow synthesis
// (output disabled for UI_16 type)

  
// ABSOLUTE_Position flow acquisition
// (setup input disabled for UI_16 type)
// ABSOLUTE_Position flow synthesis
// (output disabled for UI_16 type)

  
// ABSOLUTE_Velocity flow acquisition
// (setup input disabled for UI_16 type)
// ABSOLUTE_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// RELATIVE_Distance flow acquisition
// (setup input disabled for UI_16 type)
// RELATIVE_Distance flow synthesis
// (output disabled for UI_16 type)

  
// RELATIVE_Velocity flow acquisition
// (setup input disabled for UI_16 type)
// RELATIVE_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// Velocity_Velocity flow acquisition
// (setup input disabled for UI_16 type)
// Velocity_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// JOG_Velocity flow acquisition
// (setup input disabled for UI_16 type)
// JOG_Velocity flow synthesis
// (output disabled for UI_16 type)

  
// EmergencyStop flow acquisition
void setup_EmergencyStop(void){ 
pinMode(PIN_EmergencyStop,INPUT); 
};


// EmergencyStop flow synthesis
// (output disabled for DI type)

  
// TurnAxisHwHi flow acquisition
void setup_TurnAxisHwHi(void){ 
pinMode(PIN_TurnAxisHwHi, INPUT_PULLUP); 
};
// TurnAxisHwHi flow synthesis
// (output disabled for DI_pu type)

  
// BeaconLightRed flow acquisition
// (setup input disabled for DO type)
// BeaconLightRed flow synthesis
void setup_BeaconLightRed_output(void){ 
pinMode(PIN_BeaconLightRed, OUTPUT); 
};

  
// BeaconLightOrange flow acquisition
// (setup input disabled for DO type)
// BeaconLightOrange flow synthesis
void setup_BeaconLightOrange_output(void){ 
pinMode(PIN_BeaconLightOrange, OUTPUT); 
};

  
// TurnAxisPulse flow acquisition
// (setup input disabled for DO type)
// TurnAxisPulse flow synthesis
void setup_TurnAxisPulse_output(void){ 
pinMode(PIN_TurnAxisPulse, OUTPUT); 
};

  
// TurnAxisDir flow acquisition
// (setup input disabled for DO type)
// TurnAxisDir flow synthesis
void setup_TurnAxisDir_output(void){ 
pinMode(PIN_TurnAxisDir, OUTPUT); 
};

  
// TurnAxisEnable flow acquisition
// (setup input disabled for DO type)
// TurnAxisEnable flow synthesis
void setup_TurnAxisEnable_output(void){ 
pinMode(PIN_TurnAxisEnable, OUTPUT); 
};

  
// BeaconLightGreen flow acquisition
// (setup input disabled for DO type)
// BeaconLightGreen flow synthesis
void setup_BeaconLightGreen_output(void){ 
pinMode(PIN_BeaconLightGreen, OUTPUT); 
};

  
// Buzzer flow acquisition
// (setup input disabled for DO type)
// Buzzer flow synthesis
void setup_Buzzer_output(void){ 
pinMode(PIN_Buzzer, OUTPUT); 
};

  
// PeepValve flow acquisition
// (setup input disabled for PWM type)
// PeepValve flow synthesis
void setup_PeepValve(void){ 
#ifndef _PWM_RES_SET_
#define _PWM_RES_SET_
analogWriteResolution(CFG_PWM_RESOLUTION);
#endif
analogWriteFrequency(PIN_PeepValve, PIN_PeepValve_FREQ);
};


  
// TurnAxisHwLow flow acquisition
void setup_TurnAxisHwLow(void){ 
pinMode(PIN_TurnAxisHwLow, INPUT_PULLUP); 
};
// TurnAxisHwLow flow synthesis
// (output disabled for DI_pu type)

  
// ServoPend flow acquisition
void setup_ServoPend(void){ 
pinMode(PIN_ServoPend,INPUT); 
};


// ServoPend flow synthesis
// (output disabled for DI type)

  
// ServoAlarm flow acquisition
void setup_ServoAlarm(void){ 
pinMode(PIN_ServoAlarm, INPUT_PULLUP); 
};
// ServoAlarm flow synthesis
// (output disabled for DI_pu type)

  
// AtmosfericPressure flow acquisition
void setup_AtmosfericPressure(void){ 
pinMode(PIN_AtmosfericPressure,INPUT); 
};
// AtmosfericPressure flow synthesis
// (output disabled for ADC type)

  
// DiferentialPressure flow acquisition
void setup_DiferentialPressure(void){ 
pinMode(PIN_DiferentialPressure,INPUT); 
};
// DiferentialPressure flow synthesis
// (output disabled for ADC type)

  
// SerTX flow acquisition
// (setup input disabled for Bus type)
// SerTX flow synthesis
// (output disabled for Bus type)

  
// SerRX flow acquisition
// (setup input disabled for Bus type)
// SerRX flow synthesis
// (output disabled for Bus type)

  
// EmgcyButton flow acquisition
// (setup input disabled for TBD type)
// EmgcyButton flow synthesis
// (output disabled for TBD type)

  
// ApiVersion flow acquisition
// (setup input disabled for UI_8 type)
// ApiVersion flow synthesis
// (output disabled for UI_8 type)

  
// MachineUuid flow acquisition
// (setup input disabled for Variable type)
// MachineUuid flow synthesis
// (output disabled for Variable type)

  
// FirmwareVersion flow acquisition
// (setup input disabled for UI_16 type)
// FirmwareVersion flow synthesis
// (output disabled for UI_16 type)

  
// Uptime15mCounter flow acquisition
// (setup input disabled for UI_16 type)
// Uptime15mCounter flow synthesis
// (output disabled for UI_16 type)

  
// MutedAlarmSeconds flow acquisition
// (setup input disabled for UI_8 type)
// MutedAlarmSeconds flow synthesis
// (output disabled for UI_8 type)

  
// CycleRpmLast30s flow acquisition
// (setup input disabled for UI_16 type)
// CycleRpmLast30s flow synthesis
// (output disabled for UI_16 type)

  
// CycleIndications flow acquisition
// (setup input disabled for Variable type)
// CycleIndications flow synthesis
// (output disabled for Variable type)

  
// CycleDuration flow acquisition
// (setup input disabled for TimeUI_16 type)
// CycleDuration flow synthesis
// (output disabled for TimeUI_16 type)

  
// VentilationFlags flow acquisition
// (setup input disabled for Variable type)
// VentilationFlags flow synthesis
// (output disabled for Variable type)

  
// VolumeSetting flow acquisition
// (setup input disabled for UI_16 type)
// VolumeSetting flow synthesis
// (output disabled for UI_16 type)

  
// RpmSetting flow acquisition
// (setup input disabled for UI_16 type)
// RpmSetting flow synthesis
// (output disabled for UI_16 type)

  
// RampDegreesSetting flow acquisition
// (setup input disabled for UI_8 type)
// RampDegreesSetting flow synthesis
// (output disabled for UI_8 type)

  
// EiRatioSetting flow acquisition
// (setup input disabled for UI_8 type)
// EiRatioSetting flow synthesis
// (output disabled for UI_8 type)

  
// RecruitTimer flow acquisition
// (setup input disabled for UI_8 type)
// RecruitTimer flow synthesis
// (output disabled for UI_8 type)

  
// IncomingFrame flow acquisition
// (setup input disabled for Variable type)
// IncomingFrame flow synthesis
// (output disabled for Variable type)

  
// OutgoingFrame flow acquisition
// (setup input disabled for Variable type)
// OutgoingFrame flow synthesis
// (output disabled for Variable type)

  
// Display flow acquisition
// (setup input disabled for Display type)
// Display flow synthesis
// (output disabled for Display type)

  
// DisplayPlus flow acquisition
// (setup input disabled for Display type)
// DisplayPlus flow synthesis
// (output disabled for Display type)

  
// DisplayButtons flow acquisition
// (setup input disabled for Keyboard type)
// DisplayButtons flow synthesis
// (output disabled for Keyboard type)

  
// FlowPublic flow acquisition
// (setup input disabled for UI_16 type)
// FlowPublic flow synthesis
// (output disabled for UI_16 type)

  
// Power_EN flow acquisition
// (setup input disabled for BOOL type)
// Power_EN flow synthesis
// (output disabled for BOOL type)

  
// Power_Axis flow acquisition
// (setup input disabled for BOOL type)
// Power_Axis flow synthesis
// (output disabled for BOOL type)

  
// Power_ENO flow acquisition
// (setup input disabled for BOOL type)
// Power_ENO flow synthesis
// (output disabled for BOOL type)

  
// Reset_EN flow acquisition
// (setup input disabled for BOOL type)
// Reset_EN flow synthesis
// (output disabled for BOOL type)

  
// Reset_Axis flow acquisition
// (setup input disabled for BOOL type)
// Reset_Axis flow synthesis
// (output disabled for BOOL type)

  
// Reset_ENO flow acquisition
// (setup input disabled for BOOL type)
// Reset_ENO flow synthesis
// (output disabled for BOOL type)

  
// Home_EN flow acquisition
// (setup input disabled for BOOL type)
// Home_EN flow synthesis
// (output disabled for BOOL type)

  
// Home_Axis flow acquisition
// (setup input disabled for BOOL type)
// Home_Axis flow synthesis
// (output disabled for BOOL type)

  
// Home_ENO flow acquisition
// (setup input disabled for BOOL type)
// Home_ENO flow synthesis
// (output disabled for BOOL type)

  
// Halt_EN flow acquisition
// (setup input disabled for BOOL type)
// Halt_EN flow synthesis
// (output disabled for BOOL type)

  
// Halt_Axis flow acquisition
// (setup input disabled for BOOL type)
// Halt_Axis flow synthesis
// (output disabled for BOOL type)

  
// Halt_ENO flow acquisition
// (setup input disabled for BOOL type)
// Halt_ENO flow synthesis
// (output disabled for BOOL type)

  
// Absolute_EN flow acquisition
// (setup input disabled for BOOL type)
// Absolute_EN flow synthesis
// (output disabled for BOOL type)

  
// Absolute_Axis flow acquisition
// (setup input disabled for BOOL type)
// Absolute_Axis flow synthesis
// (output disabled for BOOL type)

  
// Absolute_ENO flow acquisition
// (setup input disabled for BOOL type)
// Absolute_ENO flow synthesis
// (output disabled for BOOL type)

  
// Relative_EN flow acquisition
// (setup input disabled for BOOL type)
// Relative_EN flow synthesis
// (output disabled for BOOL type)

  
// Relative_Axis flow acquisition
// (setup input disabled for BOOL type)
// Relative_Axis flow synthesis
// (output disabled for BOOL type)

  
// Relative_ENO flow acquisition
// (setup input disabled for BOOL type)
// Relative_ENO flow synthesis
// (output disabled for BOOL type)

  
// Velocity_EN flow acquisition
// (setup input disabled for BOOL type)
// Velocity_EN flow synthesis
// (output disabled for BOOL type)

  
// Velocity_Axis flow acquisition
// (setup input disabled for BOOL type)
// Velocity_Axis flow synthesis
// (output disabled for BOOL type)

  
// Velocity_ENO flow acquisition
// (setup input disabled for BOOL type)
// Velocity_ENO flow synthesis
// (output disabled for BOOL type)

  
// Jog_EN flow acquisition
// (setup input disabled for BOOL type)
// Jog_EN flow synthesis
// (output disabled for BOOL type)

  
// Jog_Axis flow acquisition
// (setup input disabled for BOOL type)
// Jog_Axis flow synthesis
// (output disabled for BOOL type)

  
// Jog_ENO flow acquisition
// (setup input disabled for BOOL type)
// Jog_ENO flow synthesis
// (output disabled for BOOL type)

  
// EN flow acquisition
// (setup input disabled for BOOL type)
// EN flow synthesis
// (output disabled for BOOL type)

  
// EN0 flow acquisition
// (setup input disabled for BOOL type)
// EN0 flow synthesis
// (output disabled for BOOL type)

  
// CycleStartTime flow acquisition

// CycleStartTime flow synthesis




// --- DRE flow acquisition and flow synthesis functions ---

  
// STATUS_LED flow acquisition
// (input disabled for LED type)
// STATUS_LED flow synthesis
// (output disabled for LED type)
// STATUS_LED getter
// (getter disabled for LED type)
// STATUS_LED setter
// (setter disabled for LED type)

  
// statusLED flow acquisition
// (input disabled for DO type)
// statusLED flow synthesis
void synthesize_statusLED(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_statusLED==TRUE) { 
digitalWrite(PIN_statusLED,diag.statusLED); 
} else { 
#endif 
digitalWrite(PIN_statusLED,dre.statusLED); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// statusLED getter
// (getter disabled for DO type)
// statusLED setter


  
// dbgLedBlinkTimer flow acquisition
// (input disabled for Variable type)
// dbgLedBlinkTimer flow synthesis
// (output disabled for Variable type)
// dbgLedBlinkTimer getter
t_dbgledblinktimer get_dbgLedBlinkTimer(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_dbgLedBlinkTimer==TRUE) {
		return diag.dbgLedBlinkTimer;
	} else {
#endif
		return dre.dbgLedBlinkTimer;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// dbgLedBlinkTimer setter
// (setter disabled for Variable type)

  
// DBG_LED flow acquisition
// (input disabled for Variable type)
// DBG_LED flow synthesis
// (output disabled for Variable type)
// DBG_LED getter
t_dbg_led get_DBG_LED(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_DBG_LED==TRUE) {
		return diag.DBG_LED;
	} else {
#endif
		return dre.DBG_LED;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// DBG_LED setter
// (setter disabled for Variable type)

  
// SDA flow acquisition
// (input disabled for Bus type)
// SDA flow synthesis
// (output disabled for Bus type)
// SDA getter
// (getter disabled for Bus type)
// SDA setter
// (setter disabled for Bus type)

  
// SCL flow acquisition
// (input disabled for Bus type)
// SCL flow synthesis
// (output disabled for Bus type)
// SCL getter
// (getter disabled for Bus type)
// SCL setter
// (setter disabled for Bus type)

  
// ResetRelay flow acquisition
// (input disabled for DO type)
// ResetRelay flow synthesis
void synthesize_ResetRelay(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_ResetRelay==TRUE) { 
digitalWrite(PIN_ResetRelay,diag.ResetRelay); 
} else { 
#endif 
digitalWrite(PIN_ResetRelay,dre.ResetRelay); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// ResetRelay getter
// (getter disabled for DO type)
// ResetRelay setter


  
// LCD_DB7 flow acquisition
// (input disabled for DO type)
// LCD_DB7 flow synthesis
void synthesize_LCD_DB7(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB7==TRUE) { 
digitalWrite(PIN_LCD_DB7,diag.LCD_DB7); 
} else { 
#endif 
digitalWrite(PIN_LCD_DB7,dre.LCD_DB7); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// LCD_DB7 getter
// (getter disabled for DO type)
// LCD_DB7 setter


  
// LCD_DB6 flow acquisition
// (input disabled for DO type)
// LCD_DB6 flow synthesis
void synthesize_LCD_DB6(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB6==TRUE) { 
digitalWrite(PIN_LCD_DB6,diag.LCD_DB6); 
} else { 
#endif 
digitalWrite(PIN_LCD_DB6,dre.LCD_DB6); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// LCD_DB6 getter
// (getter disabled for DO type)
// LCD_DB6 setter


  
// LCD_DB5 flow acquisition
// (input disabled for DO type)
// LCD_DB5 flow synthesis
void synthesize_LCD_DB5(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB5==TRUE) { 
digitalWrite(PIN_LCD_DB5,diag.LCD_DB5); 
} else { 
#endif 
digitalWrite(PIN_LCD_DB5,dre.LCD_DB5); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// LCD_DB5 getter
// (getter disabled for DO type)
// LCD_DB5 setter


  
// LCD_DB4 flow acquisition
// (input disabled for DO type)
// LCD_DB4 flow synthesis
void synthesize_LCD_DB4(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB4==TRUE) { 
digitalWrite(PIN_LCD_DB4,diag.LCD_DB4); 
} else { 
#endif 
digitalWrite(PIN_LCD_DB4,dre.LCD_DB4); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// LCD_DB4 getter
// (getter disabled for DO type)
// LCD_DB4 setter


  
// LCD_DB3 flow acquisition
// (input disabled for DO type)
// LCD_DB3 flow synthesis
void synthesize_LCD_DB3(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB3==TRUE) { 
digitalWrite(PIN_LCD_DB3,diag.LCD_DB3); 
} else { 
#endif 
digitalWrite(PIN_LCD_DB3,dre.LCD_DB3); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// LCD_DB3 getter
// (getter disabled for DO type)
// LCD_DB3 setter


  
// LCD_DB2 flow acquisition
// (input disabled for DO type)
// LCD_DB2 flow synthesis
void synthesize_LCD_DB2(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB2==TRUE) { 
digitalWrite(PIN_LCD_DB2,diag.LCD_DB2); 
} else { 
#endif 
digitalWrite(PIN_LCD_DB2,dre.LCD_DB2); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// LCD_DB2 getter
// (getter disabled for DO type)
// LCD_DB2 setter


  
// LCD_DB1 flow acquisition
// (input disabled for DO type)
// LCD_DB1 flow synthesis
void synthesize_LCD_DB1(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB1==TRUE) { 
digitalWrite(PIN_LCD_DB1,diag.LCD_DB1); 
} else { 
#endif 
digitalWrite(PIN_LCD_DB1,dre.LCD_DB1); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// LCD_DB1 getter
// (getter disabled for DO type)
// LCD_DB1 setter


  
// LCD_DB0 flow acquisition
// (input disabled for DO type)
// LCD_DB0 flow synthesis
void synthesize_LCD_DB0(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_LCD_DB0==TRUE) { 
digitalWrite(PIN_LCD_DB0,diag.LCD_DB0); 
} else { 
#endif 
digitalWrite(PIN_LCD_DB0,dre.LCD_DB0); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// LCD_DB0 getter
// (getter disabled for DO type)
// LCD_DB0 setter


  
// BME_CS2 flow acquisition
// (input disabled for Bus type)
// BME_CS2 flow synthesis
// (output disabled for Bus type)
// BME_CS2 getter
// (getter disabled for Bus type)
// BME_CS2 setter
// (setter disabled for Bus type)

  
// BME_MISO flow acquisition
// (input disabled for Bus type)
// BME_MISO flow synthesis
// (output disabled for Bus type)
// BME_MISO getter
// (getter disabled for Bus type)
// BME_MISO setter
// (setter disabled for Bus type)

  
// BME_MOSI flow acquisition
// (input disabled for Bus type)
// BME_MOSI flow synthesis
// (output disabled for Bus type)
// BME_MOSI getter
// (getter disabled for Bus type)
// BME_MOSI setter
// (setter disabled for Bus type)

  
// BME_SCK flow acquisition
// (input disabled for Bus type)
// BME_SCK flow synthesis
// (output disabled for Bus type)
// BME_SCK getter
// (getter disabled for Bus type)
// BME_SCK setter
// (setter disabled for Bus type)

  
// BME_CS1 flow acquisition
// (input disabled for Bus type)
// BME_CS1 flow synthesis
// (output disabled for Bus type)
// BME_CS1 getter
// (getter disabled for Bus type)
// BME_CS1 setter
// (setter disabled for Bus type)

  
// _5V flow acquisition
// (input disabled for Power type)
// _5V flow synthesis
// (output disabled for Power type)
// _5V getter
// (getter disabled for Power type)
// _5V setter
// (setter disabled for Power type)

  
// GND flow acquisition
// (input disabled for Power type)
// GND flow synthesis
// (output disabled for Power type)
// GND getter
// (getter disabled for Power type)
// GND setter
// (setter disabled for Power type)

  
// INT_FS flow acquisition
// (input disabled for Variable type)
// INT_FS flow synthesis
// (output disabled for Variable type)
// INT_FS getter
t_int_fs get_INT_FS(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_INT_FS==TRUE) {
		return diag.INT_FS;
	} else {
#endif
		return dre.INT_FS;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// INT_FS setter
// (setter disabled for Variable type)

  
// INT_FC flow acquisition
// (input disabled for Variable type)
// INT_FC flow synthesis
// (output disabled for Variable type)
// INT_FC getter
t_int_fc get_INT_FC(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_INT_FC==TRUE) {
		return diag.INT_FC;
	} else {
#endif
		return dre.INT_FC;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// INT_FC setter
// (setter disabled for Variable type)

  
// INT_HS flow acquisition
// (input disabled for Variable type)
// INT_HS flow synthesis
// (output disabled for Variable type)
// INT_HS getter
t_int_hs get_INT_HS(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_INT_HS==TRUE) {
		return diag.INT_HS;
	} else {
#endif
		return dre.INT_HS;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// INT_HS setter
// (setter disabled for Variable type)

  
// INT_HC flow acquisition
// (input disabled for Variable type)
// INT_HC flow synthesis
// (output disabled for Variable type)
// INT_HC getter
t_int_hc get_INT_HC(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_INT_HC==TRUE) {
		return diag.INT_HC;
	} else {
#endif
		return dre.INT_HC;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// INT_HC setter
// (setter disabled for Variable type)

  
// SystemDate_W flow acquisition
// (input disabled for Variable type)
// SystemDate_W flow synthesis
// (output disabled for Variable type)
// SystemDate_W getter
t_systemdate_w get_SystemDate_W(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_SystemDate_W==TRUE) {
		return diag.SystemDate_W;
	} else {
#endif
		return dre.SystemDate_W;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// SystemDate_W setter
// (setter disabled for Variable type)

  
// SystemTime_W flow acquisition
// (input disabled for Variable type)
// SystemTime_W flow synthesis
// (output disabled for Variable type)
// SystemTime_W getter
t_systemtime_w get_SystemTime_W(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_SystemTime_W==TRUE) {
		return diag.SystemTime_W;
	} else {
#endif
		return dre.SystemTime_W;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// SystemTime_W setter
// (setter disabled for Variable type)

  
// PeakFactor flow acquisition
// (input disabled for Variable type)
// PeakFactor flow synthesis
// (output disabled for Variable type)
// PeakFactor getter
t_peakfactor get_PeakFactor(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_PeakFactor==TRUE) {
		return diag.PeakFactor;
	} else {
#endif
		return dre.PeakFactor;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// PeakFactor setter
// (setter disabled for Variable type)

  
// PlateauFactor flow acquisition
// (input disabled for Variable type)
// PlateauFactor flow synthesis
// (output disabled for Variable type)
// PlateauFactor getter
t_plateaufactor get_PlateauFactor(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_PlateauFactor==TRUE) {
		return diag.PlateauFactor;
	} else {
#endif
		return dre.PlateauFactor;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// PlateauFactor setter
// (setter disabled for Variable type)

  
// PeepFactor flow acquisition
// (input disabled for Variable type)
// PeepFactor flow synthesis
// (output disabled for Variable type)
// PeepFactor getter
t_peepfactor get_PeepFactor(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_PeepFactor==TRUE) {
		return diag.PeepFactor;
	} else {
#endif
		return dre.PeepFactor;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// PeepFactor setter
// (setter disabled for Variable type)

  
// FrequencyFactor flow acquisition
// (input disabled for Variable type)
// FrequencyFactor flow synthesis
// (output disabled for Variable type)
// FrequencyFactor getter
t_frequencyfactor get_FrequencyFactor(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_FrequencyFactor==TRUE) {
		return diag.FrequencyFactor;
	} else {
#endif
		return dre.FrequencyFactor;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// FrequencyFactor setter
// (setter disabled for Variable type)

  
// TidalVolumenFactor flow acquisition
// (input disabled for Variable type)
// TidalVolumenFactor flow synthesis
// (output disabled for Variable type)
// TidalVolumenFactor getter
t_tidalvolumenfactor get_TidalVolumenFactor(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_TidalVolumenFactor==TRUE) {
		return diag.TidalVolumenFactor;
	} else {
#endif
		return dre.TidalVolumenFactor;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// TidalVolumenFactor setter
// (setter disabled for Variable type)

  
// MinimumVolumeFactor flow acquisition
// (input disabled for Variable type)
// MinimumVolumeFactor flow synthesis
// (output disabled for Variable type)
// MinimumVolumeFactor getter
t_minimumvolumefactor get_MinimumVolumeFactor(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_MinimumVolumeFactor==TRUE) {
		return diag.MinimumVolumeFactor;
	} else {
#endif
		return dre.MinimumVolumeFactor;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// MinimumVolumeFactor setter
// (setter disabled for Variable type)

  
// TriggerFactor flow acquisition
// (input disabled for Variable type)
// TriggerFactor flow synthesis
// (output disabled for Variable type)
// TriggerFactor getter
t_triggerfactor get_TriggerFactor(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_TriggerFactor==TRUE) {
		return diag.TriggerFactor;
	} else {
#endif
		return dre.TriggerFactor;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// TriggerFactor setter
// (setter disabled for Variable type)

  
// BatteryLevelTrigger flow acquisition
// (input disabled for Variable type)
// BatteryLevelTrigger flow synthesis
// (output disabled for Variable type)
// BatteryLevelTrigger getter
t_batteryleveltrigger get_BatteryLevelTrigger(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_BatteryLevelTrigger==TRUE) {
		return diag.BatteryLevelTrigger;
	} else {
#endif
		return dre.BatteryLevelTrigger;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// BatteryLevelTrigger setter
// (setter disabled for Variable type)

  
// PatientType flow acquisition
// (input disabled for Variable type)
// PatientType flow synthesis
// (output disabled for Variable type)
// PatientType getter
t_patienttype get_PatientType(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_PatientType==TRUE) {
		return diag.PatientType;
	} else {
#endif
		return dre.PatientType;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// PatientType setter
// (setter disabled for Variable type)

  
// PatientWeight flow acquisition
// (input disabled for Variable type)
// PatientWeight flow synthesis
// (output disabled for Variable type)
// PatientWeight getter
t_patientweight get_PatientWeight(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_PatientWeight==TRUE) {
		return diag.PatientWeight;
	} else {
#endif
		return dre.PatientWeight;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// PatientWeight setter
// (setter disabled for Variable type)

  
// PatientHeight flow acquisition
// (input disabled for Variable type)
// PatientHeight flow synthesis
// (output disabled for Variable type)
// PatientHeight getter
t_patientheight get_PatientHeight(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_PatientHeight==TRUE) {
		return diag.PatientHeight;
	} else {
#endif
		return dre.PatientHeight;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// PatientHeight setter
// (setter disabled for Variable type)

  
// TidalVolume flow acquisition
// (input disabled for Variable type)
// TidalVolume flow synthesis
// (output disabled for Variable type)
// TidalVolume getter
t_tidalvolume get_TidalVolume(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_TidalVolume==TRUE) {
		return diag.TidalVolume;
	} else {
#endif
		return dre.TidalVolume;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// TidalVolume setter
// (setter disabled for Variable type)

  
// PIDMode flow acquisition
// (input disabled for Variable type)
// PIDMode flow synthesis
// (output disabled for Variable type)
// PIDMode getter
t_pidmode get_PIDMode(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_PIDMode==TRUE) {
		return diag.PIDMode;
	} else {
#endif
		return dre.PIDMode;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// PIDMode setter
// (setter disabled for Variable type)

  
// PIDState flow acquisition
// (input disabled for Variable type)
// PIDState flow synthesis
// (output disabled for Variable type)
// PIDState getter
t_pidstate get_PIDState(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_PIDState==TRUE) {
		return diag.PIDState;
	} else {
#endif
		return dre.PIDState;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// PIDState setter
// (setter disabled for Variable type)

  
// PIDErrorBits flow acquisition
// (input disabled for Variable type)
// PIDErrorBits flow synthesis
// (output disabled for Variable type)
// PIDErrorBits getter
t_piderrorbits get_PIDErrorBits(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_PIDErrorBits==TRUE) {
		return diag.PIDErrorBits;
	} else {
#endif
		return dre.PIDErrorBits;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// PIDErrorBits setter
// (setter disabled for Variable type)

  
// Alarms flow acquisition
// (input disabled for Variable type)
// Alarms flow synthesis
// (output disabled for Variable type)
// Alarms getter
t_alarms get_Alarms(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_Alarms==TRUE) {
		return diag.Alarms;
	} else {
#endif
		return dre.Alarms;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// Alarms setter
// (setter disabled for Variable type)

  
// Status flow acquisition
// (input disabled for Variable type)
// Status flow synthesis
// (output disabled for Variable type)
// Status getter
t_status get_Status(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_Status==TRUE) {
		return diag.Status;
	} else {
#endif
		return dre.Status;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// Status setter
// (setter disabled for Variable type)

  
// POWER_StartMode flow acquisition
// (input disabled for Variable type)
// POWER_StartMode flow synthesis
// (output disabled for Variable type)
// POWER_StartMode getter
t_power_startmode get_POWER_StartMode(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_POWER_StartMode==TRUE) {
		return diag.POWER_StartMode;
	} else {
#endif
		return dre.POWER_StartMode;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// POWER_StartMode setter
// (setter disabled for Variable type)

  
// POWER_StopMode flow acquisition
// (input disabled for Variable type)
// POWER_StopMode flow synthesis
// (output disabled for Variable type)
// POWER_StopMode getter
t_power_stopmode get_POWER_StopMode(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_POWER_StopMode==TRUE) {
		return diag.POWER_StopMode;
	} else {
#endif
		return dre.POWER_StopMode;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// POWER_StopMode setter
// (setter disabled for Variable type)

  
// HOME_Mode flow acquisition
// (input disabled for Variable type)
// HOME_Mode flow synthesis
// (output disabled for Variable type)
// HOME_Mode getter
t_home_mode get_HOME_Mode(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_HOME_Mode==TRUE) {
		return diag.HOME_Mode;
	} else {
#endif
		return dre.HOME_Mode;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// HOME_Mode setter
// (setter disabled for Variable type)

  
// SetWriteData flow acquisition
// (input disabled for BOOL type)
// SetWriteData flow synthesis
// (output disabled for BOOL type)
// SetWriteData getter
BOOL  get_SetWriteData(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_SetWriteData==TRUE) {
return diag.SetWriteData;
} else {
#endif
return dre.SetWriteData;
#ifdef _DIAG_ACTIVE
}
#endif
};
// SetWriteData setter
// (setter disabled for BOOL type)

  
// SetTime flow acquisition
// (input disabled for BOOL type)
// SetTime flow synthesis
// (output disabled for BOOL type)
// SetTime getter
BOOL  get_SetTime(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_SetTime==TRUE) {
return diag.SetTime;
} else {
#endif
return dre.SetTime;
#ifdef _DIAG_ACTIVE
}
#endif
};
// SetTime setter
// (setter disabled for BOOL type)

  
// Trigger flow acquisition
// (input disabled for BOOL type)
// Trigger flow synthesis
// (output disabled for BOOL type)
// Trigger getter
BOOL  get_Trigger(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Trigger==TRUE) {
return diag.Trigger;
} else {
#endif
return dre.Trigger;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Trigger setter
// (setter disabled for BOOL type)

  
// PeakPBPlus flow acquisition
// (input disabled for BOOL type)
// PeakPBPlus flow synthesis
// (output disabled for BOOL type)
// PeakPBPlus getter
BOOL  get_PeakPBPlus(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakPBPlus==TRUE) {
return diag.PeakPBPlus;
} else {
#endif
return dre.PeakPBPlus;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakPBPlus setter
// (setter disabled for BOOL type)

  
// PeakPBMinus flow acquisition
// (input disabled for BOOL type)
// PeakPBMinus flow synthesis
// (output disabled for BOOL type)
// PeakPBMinus getter
BOOL  get_PeakPBMinus(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakPBMinus==TRUE) {
return diag.PeakPBMinus;
} else {
#endif
return dre.PeakPBMinus;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakPBMinus setter
// (setter disabled for BOOL type)

  
// PeepPBPlus flow acquisition
// (input disabled for BOOL type)
// PeepPBPlus flow synthesis
// (output disabled for BOOL type)
// PeepPBPlus getter
BOOL  get_PeepPBPlus(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepPBPlus==TRUE) {
return diag.PeepPBPlus;
} else {
#endif
return dre.PeepPBPlus;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepPBPlus setter
// (setter disabled for BOOL type)

  
// PeepPBMinus flow acquisition
// (input disabled for BOOL type)
// PeepPBMinus flow synthesis
// (output disabled for BOOL type)
// PeepPBMinus getter
BOOL  get_PeepPBMinus(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepPBMinus==TRUE) {
return diag.PeepPBMinus;
} else {
#endif
return dre.PeepPBMinus;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepPBMinus setter
// (setter disabled for BOOL type)

  
// FrPBPlus flow acquisition
// (input disabled for BOOL type)
// FrPBPlus flow synthesis
// (output disabled for BOOL type)
// FrPBPlus getter
BOOL  get_FrPBPlus(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrPBPlus==TRUE) {
return diag.FrPBPlus;
} else {
#endif
return dre.FrPBPlus;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrPBPlus setter
// (setter disabled for BOOL type)

  
// FrPBMinus flow acquisition
// (input disabled for BOOL type)
// FrPBMinus flow synthesis
// (output disabled for BOOL type)
// FrPBMinus getter
BOOL  get_FrPBMinus(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrPBMinus==TRUE) {
return diag.FrPBMinus;
} else {
#endif
return dre.FrPBMinus;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrPBMinus setter
// (setter disabled for BOOL type)

  
// RecruitmentOn flow acquisition
// (input disabled for BOOL type)
// RecruitmentOn flow synthesis
// (output disabled for BOOL type)
// RecruitmentOn getter
BOOL  get_RecruitmentOn(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RecruitmentOn==TRUE) {
return diag.RecruitmentOn;
} else {
#endif
return dre.RecruitmentOn;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RecruitmentOn setter
// (setter disabled for BOOL type)

  
// RecruitmentOff flow acquisition
// (input disabled for BOOL type)
// RecruitmentOff flow synthesis
// (output disabled for BOOL type)
// RecruitmentOff getter
BOOL  get_RecruitmentOff(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RecruitmentOff==TRUE) {
return diag.RecruitmentOff;
} else {
#endif
return dre.RecruitmentOff;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RecruitmentOff setter
// (setter disabled for BOOL type)

  
// RecruitmentMem flow acquisition
// (input disabled for BOOL type)
// RecruitmentMem flow synthesis
// (output disabled for BOOL type)
// RecruitmentMem getter
BOOL  get_RecruitmentMem(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RecruitmentMem==TRUE) {
return diag.RecruitmentMem;
} else {
#endif
return dre.RecruitmentMem;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RecruitmentMem setter
// (setter disabled for BOOL type)

  
// RecruitmentRunning flow acquisition
// (input disabled for BOOL type)
// RecruitmentRunning flow synthesis
// (output disabled for BOOL type)
// RecruitmentRunning getter
BOOL  get_RecruitmentRunning(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RecruitmentRunning==TRUE) {
return diag.RecruitmentRunning;
} else {
#endif
return dre.RecruitmentRunning;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RecruitmentRunning setter
// (setter disabled for BOOL type)

  
// RecruitmentEnding flow acquisition
// (input disabled for BOOL type)
// RecruitmentEnding flow synthesis
// (output disabled for BOOL type)
// RecruitmentEnding getter
BOOL  get_RecruitmentEnding(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RecruitmentEnding==TRUE) {
return diag.RecruitmentEnding;
} else {
#endif
return dre.RecruitmentEnding;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RecruitmentEnding setter
// (setter disabled for BOOL type)

  
// RecruitmentEnd flow acquisition
// (input disabled for BOOL type)
// RecruitmentEnd flow synthesis
// (output disabled for BOOL type)
// RecruitmentEnd getter
BOOL  get_RecruitmentEnd(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RecruitmentEnd==TRUE) {
return diag.RecruitmentEnd;
} else {
#endif
return dre.RecruitmentEnd;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RecruitmentEnd setter
// (setter disabled for BOOL type)

  
// ErrorAck flow acquisition
// (input disabled for BOOL type)
// ErrorAck flow synthesis
// (output disabled for BOOL type)
// ErrorAck getter
BOOL  get_ErrorAck(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_ErrorAck==TRUE) {
return diag.ErrorAck;
} else {
#endif
return dre.ErrorAck;
#ifdef _DIAG_ACTIVE
}
#endif
};
// ErrorAck setter
// (setter disabled for BOOL type)

  
// Reset flow acquisition
// (input disabled for BOOL type)
// Reset flow synthesis
// (output disabled for BOOL type)
// Reset getter
BOOL  get_Reset(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Reset==TRUE) {
return diag.Reset;
} else {
#endif
return dre.Reset;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Reset setter
// (setter disabled for BOOL type)

  
// PIDError flow acquisition
// (input disabled for BOOL type)
// PIDError flow synthesis
// (output disabled for BOOL type)
// PIDError getter
BOOL  get_PIDError(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PIDError==TRUE) {
return diag.PIDError;
} else {
#endif
return dre.PIDError;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PIDError setter
// (setter disabled for BOOL type)

  
// PeakDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeakDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// PeakDisplayPreAlarmHI getter
BOOL  get_PeakDisplayPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakDisplayPreAlarmHI==TRUE) {
return diag.PeakDisplayPreAlarmHI;
} else {
#endif
return dre.PeakDisplayPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakDisplayPreAlarmHI setter
// (setter disabled for BOOL type)

  
// PeakDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeakDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)
// PeakDisplayAlarmHI getter
BOOL  get_PeakDisplayAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakDisplayAlarmHI==TRUE) {
return diag.PeakDisplayAlarmHI;
} else {
#endif
return dre.PeakDisplayAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakDisplayAlarmHI setter
// (setter disabled for BOOL type)

  
// PeakDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeakDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PeakDisplayPreAlarmLOW getter
BOOL  get_PeakDisplayPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakDisplayPreAlarmLOW==TRUE) {
return diag.PeakDisplayPreAlarmLOW;
} else {
#endif
return dre.PeakDisplayPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakDisplayPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// PeakDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeakDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PeakDisplayAlarmLOW getter
BOOL  get_PeakDisplayAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakDisplayAlarmLOW==TRUE) {
return diag.PeakDisplayAlarmLOW;
} else {
#endif
return dre.PeakDisplayAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakDisplayAlarmLOW setter
// (setter disabled for BOOL type)

  
// PeakInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeakInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// PeakInterlockPreAlarmHI getter
BOOL  get_PeakInterlockPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakInterlockPreAlarmHI==TRUE) {
return diag.PeakInterlockPreAlarmHI;
} else {
#endif
return dre.PeakInterlockPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakInterlockPreAlarmHI setter
// (setter disabled for BOOL type)

  
// PeakInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeakInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)
// PeakInterlockAlarmHI getter
BOOL  get_PeakInterlockAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakInterlockAlarmHI==TRUE) {
return diag.PeakInterlockAlarmHI;
} else {
#endif
return dre.PeakInterlockAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakInterlockAlarmHI setter
// (setter disabled for BOOL type)

  
// PeakInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeakInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PeakInterlockPreAlarmLOW getter
BOOL  get_PeakInterlockPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakInterlockPreAlarmLOW==TRUE) {
return diag.PeakInterlockPreAlarmLOW;
} else {
#endif
return dre.PeakInterlockPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakInterlockPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// PeakInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeakInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PeakInterlockAlarmLOW getter
BOOL  get_PeakInterlockAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakInterlockAlarmLOW==TRUE) {
return diag.PeakInterlockAlarmLOW;
} else {
#endif
return dre.PeakInterlockAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakInterlockAlarmLOW setter
// (setter disabled for BOOL type)

  
// PlateauDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PlateauDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// PlateauDisplayPreAlarmHI getter
BOOL  get_PlateauDisplayPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauDisplayPreAlarmHI==TRUE) {
return diag.PlateauDisplayPreAlarmHI;
} else {
#endif
return dre.PlateauDisplayPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauDisplayPreAlarmHI setter
// (setter disabled for BOOL type)

  
// PlateauDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// PlateauDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)
// PlateauDisplayAlarmHI getter
BOOL  get_PlateauDisplayAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauDisplayAlarmHI==TRUE) {
return diag.PlateauDisplayAlarmHI;
} else {
#endif
return dre.PlateauDisplayAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauDisplayAlarmHI setter
// (setter disabled for BOOL type)

  
// PlateauDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PlateauDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PlateauDisplayPreAlarmLOW getter
BOOL  get_PlateauDisplayPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauDisplayPreAlarmLOW==TRUE) {
return diag.PlateauDisplayPreAlarmLOW;
} else {
#endif
return dre.PlateauDisplayPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauDisplayPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// PlateauDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PlateauDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PlateauDisplayAlarmLOW getter
BOOL  get_PlateauDisplayAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauDisplayAlarmLOW==TRUE) {
return diag.PlateauDisplayAlarmLOW;
} else {
#endif
return dre.PlateauDisplayAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauDisplayAlarmLOW setter
// (setter disabled for BOOL type)

  
// PlateauInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PlateauInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// PlateauInterlockPreAlarmHI getter
BOOL  get_PlateauInterlockPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauInterlockPreAlarmHI==TRUE) {
return diag.PlateauInterlockPreAlarmHI;
} else {
#endif
return dre.PlateauInterlockPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauInterlockPreAlarmHI setter
// (setter disabled for BOOL type)

  
// PlateauInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// PlateauInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)
// PlateauInterlockAlarmHI getter
BOOL  get_PlateauInterlockAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauInterlockAlarmHI==TRUE) {
return diag.PlateauInterlockAlarmHI;
} else {
#endif
return dre.PlateauInterlockAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauInterlockAlarmHI setter
// (setter disabled for BOOL type)

  
// PlateauInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PlateauInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PlateauInterlockPreAlarmLOW getter
BOOL  get_PlateauInterlockPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauInterlockPreAlarmLOW==TRUE) {
return diag.PlateauInterlockPreAlarmLOW;
} else {
#endif
return dre.PlateauInterlockPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauInterlockPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// PlateauInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PlateauInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PlateauInterlockAlarmLOW getter
BOOL  get_PlateauInterlockAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauInterlockAlarmLOW==TRUE) {
return diag.PlateauInterlockAlarmLOW;
} else {
#endif
return dre.PlateauInterlockAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauInterlockAlarmLOW setter
// (setter disabled for BOOL type)

  
// PeepDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeepDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// PeepDisplayPreAlarmHI getter
BOOL  get_PeepDisplayPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepDisplayPreAlarmHI==TRUE) {
return diag.PeepDisplayPreAlarmHI;
} else {
#endif
return dre.PeepDisplayPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepDisplayPreAlarmHI setter
// (setter disabled for BOOL type)

  
// PeepDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeepDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)
// PeepDisplayAlarmHI getter
BOOL  get_PeepDisplayAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepDisplayAlarmHI==TRUE) {
return diag.PeepDisplayAlarmHI;
} else {
#endif
return dre.PeepDisplayAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepDisplayAlarmHI setter
// (setter disabled for BOOL type)

  
// PeepDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeepDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PeepDisplayPreAlarmLOW getter
BOOL  get_PeepDisplayPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepDisplayPreAlarmLOW==TRUE) {
return diag.PeepDisplayPreAlarmLOW;
} else {
#endif
return dre.PeepDisplayPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepDisplayPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// PeepDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeepDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PeepDisplayAlarmLOW getter
BOOL  get_PeepDisplayAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepDisplayAlarmLOW==TRUE) {
return diag.PeepDisplayAlarmLOW;
} else {
#endif
return dre.PeepDisplayAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepDisplayAlarmLOW setter
// (setter disabled for BOOL type)

  
// PeepInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeepInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// PeepInterlockPreAlarmHI getter
BOOL  get_PeepInterlockPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepInterlockPreAlarmHI==TRUE) {
return diag.PeepInterlockPreAlarmHI;
} else {
#endif
return dre.PeepInterlockPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepInterlockPreAlarmHI setter
// (setter disabled for BOOL type)

  
// PeepInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// PeepInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)
// PeepInterlockAlarmHI getter
BOOL  get_PeepInterlockAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepInterlockAlarmHI==TRUE) {
return diag.PeepInterlockAlarmHI;
} else {
#endif
return dre.PeepInterlockAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepInterlockAlarmHI setter
// (setter disabled for BOOL type)

  
// PeepInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeepInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PeepInterlockPreAlarmLOW getter
BOOL  get_PeepInterlockPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepInterlockPreAlarmLOW==TRUE) {
return diag.PeepInterlockPreAlarmLOW;
} else {
#endif
return dre.PeepInterlockPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepInterlockPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// PeepInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// PeepInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)
// PeepInterlockAlarmLOW getter
BOOL  get_PeepInterlockAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepInterlockAlarmLOW==TRUE) {
return diag.PeepInterlockAlarmLOW;
} else {
#endif
return dre.PeepInterlockAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepInterlockAlarmLOW setter
// (setter disabled for BOOL type)

  
// FrequencyDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// FrequencyDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// FrequencyDisplayPreAlarmHI getter
BOOL  get_FrequencyDisplayPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyDisplayPreAlarmHI==TRUE) {
return diag.FrequencyDisplayPreAlarmHI;
} else {
#endif
return dre.FrequencyDisplayPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyDisplayPreAlarmHI setter
// (setter disabled for BOOL type)

  
// FrequencyDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// FrequencyDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)
// FrequencyDisplayAlarmHI getter
BOOL  get_FrequencyDisplayAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyDisplayAlarmHI==TRUE) {
return diag.FrequencyDisplayAlarmHI;
} else {
#endif
return dre.FrequencyDisplayAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyDisplayAlarmHI setter
// (setter disabled for BOOL type)

  
// FrequencyDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// FrequencyDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// FrequencyDisplayPreAlarmLOW getter
BOOL  get_FrequencyDisplayPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyDisplayPreAlarmLOW==TRUE) {
return diag.FrequencyDisplayPreAlarmLOW;
} else {
#endif
return dre.FrequencyDisplayPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyDisplayPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// FrequencyDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// FrequencyDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)
// FrequencyDisplayAlarmLOW getter
BOOL  get_FrequencyDisplayAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyDisplayAlarmLOW==TRUE) {
return diag.FrequencyDisplayAlarmLOW;
} else {
#endif
return dre.FrequencyDisplayAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyDisplayAlarmLOW setter
// (setter disabled for BOOL type)

  
// FrequencyInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// FrequencyInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// FrequencyInterlockPreAlarmHI getter
BOOL  get_FrequencyInterlockPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyInterlockPreAlarmHI==TRUE) {
return diag.FrequencyInterlockPreAlarmHI;
} else {
#endif
return dre.FrequencyInterlockPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyInterlockPreAlarmHI setter
// (setter disabled for BOOL type)

  
// FrequencyInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// FrequencyInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)
// FrequencyInterlockAlarmHI getter
BOOL  get_FrequencyInterlockAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyInterlockAlarmHI==TRUE) {
return diag.FrequencyInterlockAlarmHI;
} else {
#endif
return dre.FrequencyInterlockAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyInterlockAlarmHI setter
// (setter disabled for BOOL type)

  
// FrequencyInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// FrequencyInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// FrequencyInterlockPreAlarmLOW getter
BOOL  get_FrequencyInterlockPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyInterlockPreAlarmLOW==TRUE) {
return diag.FrequencyInterlockPreAlarmLOW;
} else {
#endif
return dre.FrequencyInterlockPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyInterlockPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// FrequencyInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// FrequencyInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)
// FrequencyInterlockAlarmLOW getter
BOOL  get_FrequencyInterlockAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyInterlockAlarmLOW==TRUE) {
return diag.FrequencyInterlockAlarmLOW;
} else {
#endif
return dre.FrequencyInterlockAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyInterlockAlarmLOW setter
// (setter disabled for BOOL type)

  
// TidalVolumeDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// TidalVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// TidalVolumeDisplayPreAlarmHI getter
BOOL  get_TidalVolumeDisplayPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeDisplayPreAlarmHI==TRUE) {
return diag.TidalVolumeDisplayPreAlarmHI;
} else {
#endif
return dre.TidalVolumeDisplayPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeDisplayPreAlarmHI setter
// (setter disabled for BOOL type)

  
// TidalVolumeDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// TidalVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)
// TidalVolumeDisplayAlarmHI getter
BOOL  get_TidalVolumeDisplayAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeDisplayAlarmHI==TRUE) {
return diag.TidalVolumeDisplayAlarmHI;
} else {
#endif
return dre.TidalVolumeDisplayAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeDisplayAlarmHI setter
// (setter disabled for BOOL type)

  
// TidalVolumeDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TidalVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// TidalVolumeDisplayPreAlarmLOW getter
BOOL  get_TidalVolumeDisplayPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeDisplayPreAlarmLOW==TRUE) {
return diag.TidalVolumeDisplayPreAlarmLOW;
} else {
#endif
return dre.TidalVolumeDisplayPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeDisplayPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// TidalVolumeDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TidalVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)
// TidalVolumeDisplayAlarmLOW getter
BOOL  get_TidalVolumeDisplayAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeDisplayAlarmLOW==TRUE) {
return diag.TidalVolumeDisplayAlarmLOW;
} else {
#endif
return dre.TidalVolumeDisplayAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeDisplayAlarmLOW setter
// (setter disabled for BOOL type)

  
// TidalVolumeInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// TidalVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// TidalVolumeInterlockPreAlarmHI getter
BOOL  get_TidalVolumeInterlockPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeInterlockPreAlarmHI==TRUE) {
return diag.TidalVolumeInterlockPreAlarmHI;
} else {
#endif
return dre.TidalVolumeInterlockPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeInterlockPreAlarmHI setter
// (setter disabled for BOOL type)

  
// TidalVolumeInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// TidalVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)
// TidalVolumeInterlockAlarmHI getter
BOOL  get_TidalVolumeInterlockAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeInterlockAlarmHI==TRUE) {
return diag.TidalVolumeInterlockAlarmHI;
} else {
#endif
return dre.TidalVolumeInterlockAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeInterlockAlarmHI setter
// (setter disabled for BOOL type)

  
// TidalVolumeInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TidalVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// TidalVolumeInterlockPreAlarmLOW getter
BOOL  get_TidalVolumeInterlockPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeInterlockPreAlarmLOW==TRUE) {
return diag.TidalVolumeInterlockPreAlarmLOW;
} else {
#endif
return dre.TidalVolumeInterlockPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeInterlockPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// TidalVolumeInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TidalVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)
// TidalVolumeInterlockAlarmLOW getter
BOOL  get_TidalVolumeInterlockAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeInterlockAlarmLOW==TRUE) {
return diag.TidalVolumeInterlockAlarmLOW;
} else {
#endif
return dre.TidalVolumeInterlockAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeInterlockAlarmLOW setter
// (setter disabled for BOOL type)

  
// MinuteVolumeDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// MinuteVolumeDisplayPreAlarmHI getter
BOOL  get_MinuteVolumeDisplayPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeDisplayPreAlarmHI==TRUE) {
return diag.MinuteVolumeDisplayPreAlarmHI;
} else {
#endif
return dre.MinuteVolumeDisplayPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeDisplayPreAlarmHI setter
// (setter disabled for BOOL type)

  
// MinuteVolumeDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)
// MinuteVolumeDisplayAlarmHI getter
BOOL  get_MinuteVolumeDisplayAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeDisplayAlarmHI==TRUE) {
return diag.MinuteVolumeDisplayAlarmHI;
} else {
#endif
return dre.MinuteVolumeDisplayAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeDisplayAlarmHI setter
// (setter disabled for BOOL type)

  
// MinuteVolumeDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// MinuteVolumeDisplayPreAlarmLOW getter
BOOL  get_MinuteVolumeDisplayPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeDisplayPreAlarmLOW==TRUE) {
return diag.MinuteVolumeDisplayPreAlarmLOW;
} else {
#endif
return dre.MinuteVolumeDisplayPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeDisplayPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// MinuteVolumeDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)
// MinuteVolumeDisplayAlarmLOW getter
BOOL  get_MinuteVolumeDisplayAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeDisplayAlarmLOW==TRUE) {
return diag.MinuteVolumeDisplayAlarmLOW;
} else {
#endif
return dre.MinuteVolumeDisplayAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeDisplayAlarmLOW setter
// (setter disabled for BOOL type)

  
// MinuteVolumeInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// MinuteVolumeInterlockPreAlarmHI getter
BOOL  get_MinuteVolumeInterlockPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeInterlockPreAlarmHI==TRUE) {
return diag.MinuteVolumeInterlockPreAlarmHI;
} else {
#endif
return dre.MinuteVolumeInterlockPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeInterlockPreAlarmHI setter
// (setter disabled for BOOL type)

  
// MinuteVolumeInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)
// MinuteVolumeInterlockAlarmHI getter
BOOL  get_MinuteVolumeInterlockAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeInterlockAlarmHI==TRUE) {
return diag.MinuteVolumeInterlockAlarmHI;
} else {
#endif
return dre.MinuteVolumeInterlockAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeInterlockAlarmHI setter
// (setter disabled for BOOL type)

  
// MinuteVolumeInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// MinuteVolumeInterlockPreAlarmLOW getter
BOOL  get_MinuteVolumeInterlockPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeInterlockPreAlarmLOW==TRUE) {
return diag.MinuteVolumeInterlockPreAlarmLOW;
} else {
#endif
return dre.MinuteVolumeInterlockPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeInterlockPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// MinuteVolumeInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// MinuteVolumeInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)
// MinuteVolumeInterlockAlarmLOW getter
BOOL  get_MinuteVolumeInterlockAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeInterlockAlarmLOW==TRUE) {
return diag.MinuteVolumeInterlockAlarmLOW;
} else {
#endif
return dre.MinuteVolumeInterlockAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeInterlockAlarmLOW setter
// (setter disabled for BOOL type)

  
// TriggerDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// TriggerDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// TriggerDisplayPreAlarmHI getter
BOOL  get_TriggerDisplayPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerDisplayPreAlarmHI==TRUE) {
return diag.TriggerDisplayPreAlarmHI;
} else {
#endif
return dre.TriggerDisplayPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerDisplayPreAlarmHI setter
// (setter disabled for BOOL type)

  
// TriggerDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// TriggerDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)
// TriggerDisplayAlarmHI getter
BOOL  get_TriggerDisplayAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerDisplayAlarmHI==TRUE) {
return diag.TriggerDisplayAlarmHI;
} else {
#endif
return dre.TriggerDisplayAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerDisplayAlarmHI setter
// (setter disabled for BOOL type)

  
// TriggerDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TriggerDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// TriggerDisplayPreAlarmLOW getter
BOOL  get_TriggerDisplayPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerDisplayPreAlarmLOW==TRUE) {
return diag.TriggerDisplayPreAlarmLOW;
} else {
#endif
return dre.TriggerDisplayPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerDisplayPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// TriggerDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TriggerDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)
// TriggerDisplayAlarmLOW getter
BOOL  get_TriggerDisplayAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerDisplayAlarmLOW==TRUE) {
return diag.TriggerDisplayAlarmLOW;
} else {
#endif
return dre.TriggerDisplayAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerDisplayAlarmLOW setter
// (setter disabled for BOOL type)

  
// TriggerInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// TriggerInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// TriggerInterlockPreAlarmHI getter
BOOL  get_TriggerInterlockPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerInterlockPreAlarmHI==TRUE) {
return diag.TriggerInterlockPreAlarmHI;
} else {
#endif
return dre.TriggerInterlockPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerInterlockPreAlarmHI setter
// (setter disabled for BOOL type)

  
// TriggerInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// TriggerInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)
// TriggerInterlockAlarmHI getter
BOOL  get_TriggerInterlockAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerInterlockAlarmHI==TRUE) {
return diag.TriggerInterlockAlarmHI;
} else {
#endif
return dre.TriggerInterlockAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerInterlockAlarmHI setter
// (setter disabled for BOOL type)

  
// TriggerInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TriggerInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// TriggerInterlockPreAlarmLOW getter
BOOL  get_TriggerInterlockPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerInterlockPreAlarmLOW==TRUE) {
return diag.TriggerInterlockPreAlarmLOW;
} else {
#endif
return dre.TriggerInterlockPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerInterlockPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// TriggerInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// TriggerInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)
// TriggerInterlockAlarmLOW getter
BOOL  get_TriggerInterlockAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerInterlockAlarmLOW==TRUE) {
return diag.TriggerInterlockAlarmLOW;
} else {
#endif
return dre.TriggerInterlockAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerInterlockAlarmLOW setter
// (setter disabled for BOOL type)

  
// BatteryDisplayPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// BatteryDisplayPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// BatteryDisplayPreAlarmHI getter
BOOL  get_BatteryDisplayPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatteryDisplayPreAlarmHI==TRUE) {
return diag.BatteryDisplayPreAlarmHI;
} else {
#endif
return dre.BatteryDisplayPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatteryDisplayPreAlarmHI setter
// (setter disabled for BOOL type)

  
// BatteryDisplayAlarmHI flow acquisition
// (input disabled for BOOL type)
// BatteryDisplayAlarmHI flow synthesis
// (output disabled for BOOL type)
// BatteryDisplayAlarmHI getter
BOOL  get_BatteryDisplayAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatteryDisplayAlarmHI==TRUE) {
return diag.BatteryDisplayAlarmHI;
} else {
#endif
return dre.BatteryDisplayAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatteryDisplayAlarmHI setter
// (setter disabled for BOOL type)

  
// BatteryDisplayPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// BatteryDisplayPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// BatteryDisplayPreAlarmLOW getter
BOOL  get_BatteryDisplayPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatteryDisplayPreAlarmLOW==TRUE) {
return diag.BatteryDisplayPreAlarmLOW;
} else {
#endif
return dre.BatteryDisplayPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatteryDisplayPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// BatteryDisplayAlarmLOW flow acquisition
// (input disabled for BOOL type)
// BatteryDisplayAlarmLOW flow synthesis
// (output disabled for BOOL type)
// BatteryDisplayAlarmLOW getter
BOOL  get_BatteryDisplayAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatteryDisplayAlarmLOW==TRUE) {
return diag.BatteryDisplayAlarmLOW;
} else {
#endif
return dre.BatteryDisplayAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatteryDisplayAlarmLOW setter
// (setter disabled for BOOL type)

  
// BatteryInterlockPreAlarmHI flow acquisition
// (input disabled for BOOL type)
// BatteryInterlockPreAlarmHI flow synthesis
// (output disabled for BOOL type)
// BatteryInterlockPreAlarmHI getter
BOOL  get_BatteryInterlockPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatteryInterlockPreAlarmHI==TRUE) {
return diag.BatteryInterlockPreAlarmHI;
} else {
#endif
return dre.BatteryInterlockPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatteryInterlockPreAlarmHI setter
// (setter disabled for BOOL type)

  
// BatteryInterlockAlarmHI flow acquisition
// (input disabled for BOOL type)
// BatteryInterlockAlarmHI flow synthesis
// (output disabled for BOOL type)
// BatteryInterlockAlarmHI getter
BOOL  get_BatteryInterlockAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatteryInterlockAlarmHI==TRUE) {
return diag.BatteryInterlockAlarmHI;
} else {
#endif
return dre.BatteryInterlockAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatteryInterlockAlarmHI setter
// (setter disabled for BOOL type)

  
// BatteryInterlockPreAlarmLOW flow acquisition
// (input disabled for BOOL type)
// BatteryInterlockPreAlarmLOW flow synthesis
// (output disabled for BOOL type)
// BatteryInterlockPreAlarmLOW getter
BOOL  get_BatteryInterlockPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatteryInterlockPreAlarmLOW==TRUE) {
return diag.BatteryInterlockPreAlarmLOW;
} else {
#endif
return dre.BatteryInterlockPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatteryInterlockPreAlarmLOW setter
// (setter disabled for BOOL type)

  
// BatteryInterlockAlarmLOW flow acquisition
// (input disabled for BOOL type)
// BatteryInterlockAlarmLOW flow synthesis
// (output disabled for BOOL type)
// BatteryInterlockAlarmLOW getter
BOOL  get_BatteryInterlockAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatteryInterlockAlarmLOW==TRUE) {
return diag.BatteryInterlockAlarmLOW;
} else {
#endif
return dre.BatteryInterlockAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatteryInterlockAlarmLOW setter
// (setter disabled for BOOL type)

  
// Enable flow acquisition
// (input disabled for BOOL type)
// Enable flow synthesis
// (output disabled for BOOL type)
// Enable getter
BOOL  get_Enable(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Enable==TRUE) {
return diag.Enable;
} else {
#endif
return dre.Enable;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Enable setter
// (setter disabled for BOOL type)

  
// Busy flow acquisition
// (input disabled for BOOL type)
// Busy flow synthesis
// (output disabled for BOOL type)
// Busy getter
BOOL  get_Busy(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Busy==TRUE) {
return diag.Busy;
} else {
#endif
return dre.Busy;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Busy setter
// (setter disabled for BOOL type)

  
// POWER_Enable flow acquisition
// (input disabled for BOOL type)
// POWER_Enable flow synthesis
// (output disabled for BOOL type)
// POWER_Enable getter
BOOL  get_POWER_Enable(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_POWER_Enable==TRUE) {
return diag.POWER_Enable;
} else {
#endif
return dre.POWER_Enable;
#ifdef _DIAG_ACTIVE
}
#endif
};
// POWER_Enable setter
// (setter disabled for BOOL type)

  
// POWER_Status flow acquisition
// (input disabled for BOOL type)
// POWER_Status flow synthesis
// (output disabled for BOOL type)
// POWER_Status getter
BOOL  get_POWER_Status(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_POWER_Status==TRUE) {
return diag.POWER_Status;
} else {
#endif
return dre.POWER_Status;
#ifdef _DIAG_ACTIVE
}
#endif
};
// POWER_Status setter
// (setter disabled for BOOL type)

  
// POWER_Error flow acquisition
// (input disabled for BOOL type)
// POWER_Error flow synthesis
// (output disabled for BOOL type)
// POWER_Error getter
BOOL  get_POWER_Error(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_POWER_Error==TRUE) {
return diag.POWER_Error;
} else {
#endif
return dre.POWER_Error;
#ifdef _DIAG_ACTIVE
}
#endif
};
// POWER_Error setter
// (setter disabled for BOOL type)

  
// RESET_Execute flow acquisition
// (input disabled for BOOL type)
// RESET_Execute flow synthesis
// (output disabled for BOOL type)
// RESET_Execute getter
BOOL  get_RESET_Execute(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RESET_Execute==TRUE) {
return diag.RESET_Execute;
} else {
#endif
return dre.RESET_Execute;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RESET_Execute setter
// (setter disabled for BOOL type)

  
// RESET_Done flow acquisition
// (input disabled for BOOL type)
// RESET_Done flow synthesis
// (output disabled for BOOL type)
// RESET_Done getter
BOOL  get_RESET_Done(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RESET_Done==TRUE) {
return diag.RESET_Done;
} else {
#endif
return dre.RESET_Done;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RESET_Done setter
// (setter disabled for BOOL type)

  
// RESET_Error flow acquisition
// (input disabled for BOOL type)
// RESET_Error flow synthesis
// (output disabled for BOOL type)
// RESET_Error getter
BOOL  get_RESET_Error(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RESET_Error==TRUE) {
return diag.RESET_Error;
} else {
#endif
return dre.RESET_Error;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RESET_Error setter
// (setter disabled for BOOL type)

  
// HOME_Execute flow acquisition
// (input disabled for BOOL type)
// HOME_Execute flow synthesis
// (output disabled for BOOL type)
// HOME_Execute getter
BOOL  get_HOME_Execute(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_HOME_Execute==TRUE) {
return diag.HOME_Execute;
} else {
#endif
return dre.HOME_Execute;
#ifdef _DIAG_ACTIVE
}
#endif
};
// HOME_Execute setter
// (setter disabled for BOOL type)

  
// HOME_Done flow acquisition
// (input disabled for BOOL type)
// HOME_Done flow synthesis
// (output disabled for BOOL type)
// HOME_Done getter
BOOL  get_HOME_Done(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_HOME_Done==TRUE) {
return diag.HOME_Done;
} else {
#endif
return dre.HOME_Done;
#ifdef _DIAG_ACTIVE
}
#endif
};
// HOME_Done setter
// (setter disabled for BOOL type)

  
// HOME_Error flow acquisition
// (input disabled for BOOL type)
// HOME_Error flow synthesis
// (output disabled for BOOL type)
// HOME_Error getter
BOOL  get_HOME_Error(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_HOME_Error==TRUE) {
return diag.HOME_Error;
} else {
#endif
return dre.HOME_Error;
#ifdef _DIAG_ACTIVE
}
#endif
};
// HOME_Error setter
// (setter disabled for BOOL type)

  
// HALT_Execute flow acquisition
// (input disabled for BOOL type)
// HALT_Execute flow synthesis
// (output disabled for BOOL type)
// HALT_Execute getter
BOOL  get_HALT_Execute(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_HALT_Execute==TRUE) {
return diag.HALT_Execute;
} else {
#endif
return dre.HALT_Execute;
#ifdef _DIAG_ACTIVE
}
#endif
};
// HALT_Execute setter
// (setter disabled for BOOL type)

  
// HALT_Done flow acquisition
// (input disabled for BOOL type)
// HALT_Done flow synthesis
// (output disabled for BOOL type)
// HALT_Done getter
BOOL  get_HALT_Done(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_HALT_Done==TRUE) {
return diag.HALT_Done;
} else {
#endif
return dre.HALT_Done;
#ifdef _DIAG_ACTIVE
}
#endif
};
// HALT_Done setter
// (setter disabled for BOOL type)

  
// HALT_Error flow acquisition
// (input disabled for BOOL type)
// HALT_Error flow synthesis
// (output disabled for BOOL type)
// HALT_Error getter
BOOL  get_HALT_Error(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_HALT_Error==TRUE) {
return diag.HALT_Error;
} else {
#endif
return dre.HALT_Error;
#ifdef _DIAG_ACTIVE
}
#endif
};
// HALT_Error setter
// (setter disabled for BOOL type)

  
// ABSOLUTE_Execute flow acquisition
// (input disabled for BOOL type)
// ABSOLUTE_Execute flow synthesis
// (output disabled for BOOL type)
// ABSOLUTE_Execute getter
BOOL  get_ABSOLUTE_Execute(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_ABSOLUTE_Execute==TRUE) {
return diag.ABSOLUTE_Execute;
} else {
#endif
return dre.ABSOLUTE_Execute;
#ifdef _DIAG_ACTIVE
}
#endif
};
// ABSOLUTE_Execute setter
// (setter disabled for BOOL type)

  
// ABSOLUTE_Done flow acquisition
// (input disabled for BOOL type)
// ABSOLUTE_Done flow synthesis
// (output disabled for BOOL type)
// ABSOLUTE_Done getter
BOOL  get_ABSOLUTE_Done(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_ABSOLUTE_Done==TRUE) {
return diag.ABSOLUTE_Done;
} else {
#endif
return dre.ABSOLUTE_Done;
#ifdef _DIAG_ACTIVE
}
#endif
};
// ABSOLUTE_Done setter
// (setter disabled for BOOL type)

  
// ABSOLUTE_Error flow acquisition
// (input disabled for BOOL type)
// ABSOLUTE_Error flow synthesis
// (output disabled for BOOL type)
// ABSOLUTE_Error getter
BOOL  get_ABSOLUTE_Error(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_ABSOLUTE_Error==TRUE) {
return diag.ABSOLUTE_Error;
} else {
#endif
return dre.ABSOLUTE_Error;
#ifdef _DIAG_ACTIVE
}
#endif
};
// ABSOLUTE_Error setter
// (setter disabled for BOOL type)

  
// RELATIVE_Execute flow acquisition
// (input disabled for BOOL type)
// RELATIVE_Execute flow synthesis
// (output disabled for BOOL type)
// RELATIVE_Execute getter
BOOL  get_RELATIVE_Execute(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RELATIVE_Execute==TRUE) {
return diag.RELATIVE_Execute;
} else {
#endif
return dre.RELATIVE_Execute;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RELATIVE_Execute setter
// (setter disabled for BOOL type)

  
// RELATIVE_Done flow acquisition
// (input disabled for BOOL type)
// RELATIVE_Done flow synthesis
// (output disabled for BOOL type)
// RELATIVE_Done getter
BOOL  get_RELATIVE_Done(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RELATIVE_Done==TRUE) {
return diag.RELATIVE_Done;
} else {
#endif
return dre.RELATIVE_Done;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RELATIVE_Done setter
// (setter disabled for BOOL type)

  
// RELATIVE_Error flow acquisition
// (input disabled for BOOL type)
// RELATIVE_Error flow synthesis
// (output disabled for BOOL type)
// RELATIVE_Error getter
BOOL  get_RELATIVE_Error(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RELATIVE_Error==TRUE) {
return diag.RELATIVE_Error;
} else {
#endif
return dre.RELATIVE_Error;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RELATIVE_Error setter
// (setter disabled for BOOL type)

  
// Velocity_Execute flow acquisition
// (input disabled for BOOL type)
// Velocity_Execute flow synthesis
// (output disabled for BOOL type)
// Velocity_Execute getter
BOOL  get_Velocity_Execute(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Velocity_Execute==TRUE) {
return diag.Velocity_Execute;
} else {
#endif
return dre.Velocity_Execute;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Velocity_Execute setter
// (setter disabled for BOOL type)

  
// Velocity_Current flow acquisition
// (input disabled for BOOL type)
// Velocity_Current flow synthesis
// (output disabled for BOOL type)
// Velocity_Current getter
BOOL  get_Velocity_Current(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Velocity_Current==TRUE) {
return diag.Velocity_Current;
} else {
#endif
return dre.Velocity_Current;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Velocity_Current setter
// (setter disabled for BOOL type)

  
// Velocity_invelocity flow acquisition
// (input disabled for BOOL type)
// Velocity_invelocity flow synthesis
// (output disabled for BOOL type)
// Velocity_invelocity getter
BOOL  get_Velocity_invelocity(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Velocity_invelocity==TRUE) {
return diag.Velocity_invelocity;
} else {
#endif
return dre.Velocity_invelocity;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Velocity_invelocity setter
// (setter disabled for BOOL type)

  
// Velocity_Error flow acquisition
// (input disabled for BOOL type)
// Velocity_Error flow synthesis
// (output disabled for BOOL type)
// Velocity_Error getter
BOOL  get_Velocity_Error(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Velocity_Error==TRUE) {
return diag.Velocity_Error;
} else {
#endif
return dre.Velocity_Error;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Velocity_Error setter
// (setter disabled for BOOL type)

  
// JOG_Forward flow acquisition
// (input disabled for BOOL type)
// JOG_Forward flow synthesis
// (output disabled for BOOL type)
// JOG_Forward getter
BOOL  get_JOG_Forward(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_JOG_Forward==TRUE) {
return diag.JOG_Forward;
} else {
#endif
return dre.JOG_Forward;
#ifdef _DIAG_ACTIVE
}
#endif
};
// JOG_Forward setter
// (setter disabled for BOOL type)

  
// JOG_Backward flow acquisition
// (input disabled for BOOL type)
// JOG_Backward flow synthesis
// (output disabled for BOOL type)
// JOG_Backward getter
BOOL  get_JOG_Backward(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_JOG_Backward==TRUE) {
return diag.JOG_Backward;
} else {
#endif
return dre.JOG_Backward;
#ifdef _DIAG_ACTIVE
}
#endif
};
// JOG_Backward setter
// (setter disabled for BOOL type)

  
// JOG_Invelocity flow acquisition
// (input disabled for BOOL type)
// JOG_Invelocity flow synthesis
// (output disabled for BOOL type)
// JOG_Invelocity getter
BOOL  get_JOG_Invelocity(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_JOG_Invelocity==TRUE) {
return diag.JOG_Invelocity;
} else {
#endif
return dre.JOG_Invelocity;
#ifdef _DIAG_ACTIVE
}
#endif
};
// JOG_Invelocity setter
// (setter disabled for BOOL type)

  
// JOG_Error flow acquisition
// (input disabled for BOOL type)
// JOG_Error flow synthesis
// (output disabled for BOOL type)
// JOG_Error getter
BOOL  get_JOG_Error(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_JOG_Error==TRUE) {
return diag.JOG_Error;
} else {
#endif
return dre.JOG_Error;
#ifdef _DIAG_ACTIVE
}
#endif
};
// JOG_Error setter
// (setter disabled for BOOL type)

  
// Permissions flow acquisition
// (input disabled for BOOL type)
// Permissions flow synthesis
// (output disabled for BOOL type)
// Permissions getter
BOOL  get_Permissions(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Permissions==TRUE) {
return diag.Permissions;
} else {
#endif
return dre.Permissions;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Permissions setter
// (setter disabled for BOOL type)

  
// Operation_ON flow acquisition
// (input disabled for BOOL type)
// Operation_ON flow synthesis
// (output disabled for BOOL type)
// Operation_ON getter
BOOL  get_Operation_ON(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Operation_ON==TRUE) {
return diag.Operation_ON;
} else {
#endif
return dre.Operation_ON;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Operation_ON setter
// (setter disabled for BOOL type)

  
// Emergency flow acquisition
// (input disabled for BOOL type)
// Emergency flow synthesis
// (output disabled for BOOL type)
// Emergency getter
BOOL  get_Emergency(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Emergency==TRUE) {
return diag.Emergency;
} else {
#endif
return dre.Emergency;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Emergency setter
// (setter disabled for BOOL type)

  
// GeneralAck flow acquisition
// (input disabled for BOOL type)
// GeneralAck flow synthesis
// (output disabled for BOOL type)
// GeneralAck getter
BOOL  get_GeneralAck(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_GeneralAck==TRUE) {
return diag.GeneralAck;
} else {
#endif
return dre.GeneralAck;
#ifdef _DIAG_ACTIVE
}
#endif
};
// GeneralAck setter
// (setter disabled for BOOL type)

  
// MotorAck flow acquisition
// (input disabled for BOOL type)
// MotorAck flow synthesis
// (output disabled for BOOL type)
// MotorAck getter
BOOL  get_MotorAck(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MotorAck==TRUE) {
return diag.MotorAck;
} else {
#endif
return dre.MotorAck;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MotorAck setter
// (setter disabled for BOOL type)

  
// ProcessAck flow acquisition
// (input disabled for BOOL type)
// ProcessAck flow synthesis
// (output disabled for BOOL type)
// ProcessAck getter
BOOL  get_ProcessAck(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_ProcessAck==TRUE) {
return diag.ProcessAck;
} else {
#endif
return dre.ProcessAck;
#ifdef _DIAG_ACTIVE
}
#endif
};
// ProcessAck setter
// (setter disabled for BOOL type)

  
// GeneralReset flow acquisition
// (input disabled for BOOL type)
// GeneralReset flow synthesis
// (output disabled for BOOL type)
// GeneralReset getter
BOOL  get_GeneralReset(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_GeneralReset==TRUE) {
return diag.GeneralReset;
} else {
#endif
return dre.GeneralReset;
#ifdef _DIAG_ACTIVE
}
#endif
};
// GeneralReset setter
// (setter disabled for BOOL type)

  
// MotorReset flow acquisition
// (input disabled for BOOL type)
// MotorReset flow synthesis
// (output disabled for BOOL type)
// MotorReset getter
BOOL  get_MotorReset(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MotorReset==TRUE) {
return diag.MotorReset;
} else {
#endif
return dre.MotorReset;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MotorReset setter
// (setter disabled for BOOL type)

  
// ProcessReset flow acquisition
// (input disabled for BOOL type)
// ProcessReset flow synthesis
// (output disabled for BOOL type)
// ProcessReset getter
BOOL  get_ProcessReset(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_ProcessReset==TRUE) {
return diag.ProcessReset;
} else {
#endif
return dre.ProcessReset;
#ifdef _DIAG_ACTIVE
}
#endif
};
// ProcessReset setter
// (setter disabled for BOOL type)

  
// PeakValue flow acquisition
// (input disabled for UI_16 type)
// PeakValue flow synthesis
// (output disabled for UI_16 type)
// PeakValue getter
UI_16 get_PeakValue(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakValue==TRUE) {
return diag.PeakValue;
} else {
#endif
return dre.PeakValue;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakValue setter
// (setter disabled for UI_16 type)

  
// PlateauValue flow acquisition
// (input disabled for UI_16 type)
// PlateauValue flow synthesis
// (output disabled for UI_16 type)
// PlateauValue getter
UI_16 get_PlateauValue(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauValue==TRUE) {
return diag.PlateauValue;
} else {
#endif
return dre.PlateauValue;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauValue setter
// (setter disabled for UI_16 type)

  
// PeepValue flow acquisition
// (input disabled for UI_16 type)
// PeepValue flow synthesis
// (output disabled for UI_16 type)
// PeepValue getter
UI_16 get_PeepValue(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepValue==TRUE) {
return diag.PeepValue;
} else {
#endif
return dre.PeepValue;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepValue setter
// (setter disabled for UI_16 type)

  
// FrequencyValue flow acquisition
// (input disabled for UI_16 type)
// FrequencyValue flow synthesis
// (output disabled for UI_16 type)
// FrequencyValue getter
UI_16 get_FrequencyValue(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyValue==TRUE) {
return diag.FrequencyValue;
} else {
#endif
return dre.FrequencyValue;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyValue setter
// (setter disabled for UI_16 type)

  
// TidalVolumeValue flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeValue flow synthesis
// (output disabled for UI_16 type)
// TidalVolumeValue getter
UI_16 get_TidalVolumeValue(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeValue==TRUE) {
return diag.TidalVolumeValue;
} else {
#endif
return dre.TidalVolumeValue;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeValue setter
// (setter disabled for UI_16 type)

  
// MinumValue flow acquisition
// (input disabled for UI_16 type)
// MinumValue flow synthesis
// (output disabled for UI_16 type)
// MinumValue getter
UI_16 get_MinumValue(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinumValue==TRUE) {
return diag.MinumValue;
} else {
#endif
return dre.MinumValue;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinumValue setter
// (setter disabled for UI_16 type)

  
// DrivingPreassure flow acquisition
// (input disabled for UI_16 type)
// DrivingPreassure flow synthesis
// (output disabled for UI_16 type)
// DrivingPreassure getter
UI_16 get_DrivingPreassure(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_DrivingPreassure==TRUE) {
return diag.DrivingPreassure;
} else {
#endif
return dre.DrivingPreassure;
#ifdef _DIAG_ACTIVE
}
#endif
};
// DrivingPreassure setter
// (setter disabled for UI_16 type)

  
// BatteryVoltage flow acquisition
// (input disabled for UI_16 type)
// BatteryVoltage flow synthesis
// (output disabled for UI_16 type)
// BatteryVoltage getter
UI_16 get_BatteryVoltage(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatteryVoltage==TRUE) {
return diag.BatteryVoltage;
} else {
#endif
return dre.BatteryVoltage;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatteryVoltage setter
// (setter disabled for UI_16 type)

  
// BatteryCapacity flow acquisition
// (input disabled for UI_16 type)
// BatteryCapacity flow synthesis
// (output disabled for UI_16 type)
// BatteryCapacity getter
UI_16 get_BatteryCapacity(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatteryCapacity==TRUE) {
return diag.BatteryCapacity;
} else {
#endif
return dre.BatteryCapacity;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatteryCapacity setter
// (setter disabled for UI_16 type)

  
// Pressure1 flow acquisition
// (input disabled for UI_16 type)
// Pressure1 flow synthesis
// (output disabled for UI_16 type)
// Pressure1 getter
UI_16 get_Pressure1(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Pressure1==TRUE) {
return diag.Pressure1;
} else {
#endif
return dre.Pressure1;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Pressure1 setter
// (setter disabled for UI_16 type)

  
// Pressure2 flow acquisition
// (input disabled for UI_16 type)
// Pressure2 flow synthesis
// (output disabled for UI_16 type)
// Pressure2 getter
UI_16 get_Pressure2(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Pressure2==TRUE) {
return diag.Pressure2;
} else {
#endif
return dre.Pressure2;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Pressure2 setter
// (setter disabled for UI_16 type)

  
// VolumeAccumulated flow acquisition
// (input disabled for UI_16 type)
// VolumeAccumulated flow synthesis
// (output disabled for UI_16 type)
// VolumeAccumulated getter
UI_16 get_VolumeAccumulated(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_VolumeAccumulated==TRUE) {
return diag.VolumeAccumulated;
} else {
#endif
return dre.VolumeAccumulated;
#ifdef _DIAG_ACTIVE
}
#endif
};
// VolumeAccumulated setter
// (setter disabled for UI_16 type)

  
// Flow flow acquisition
// (input disabled for UI_16 type)
// Flow flow synthesis
// (output disabled for UI_16 type)
// Flow getter
UI_16 get_Flow(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Flow==TRUE) {
return diag.Flow;
} else {
#endif
return dre.Flow;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Flow setter
// (setter disabled for UI_16 type)

  
// SystemDateRetVal flow acquisition
// (input disabled for UI_16 type)
// SystemDateRetVal flow synthesis
// (output disabled for UI_16 type)
// SystemDateRetVal getter
UI_16 get_SystemDateRetVal(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_SystemDateRetVal==TRUE) {
return diag.SystemDateRetVal;
} else {
#endif
return dre.SystemDateRetVal;
#ifdef _DIAG_ACTIVE
}
#endif
};
// SystemDateRetVal setter
// (setter disabled for UI_16 type)

  
// SystemDateOut flow acquisition
// (input disabled for DateTime type)
// SystemDateOut flow synthesis
// (output disabled for DateTime type)
// SystemDateOut getter
t_datetime get_SystemDateOut(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_SystemDateOut==TRUE) {
return diag.SystemDateOut;
} else {
#endif
return dre.SystemDateOut;
#ifdef _DIAG_ACTIVE
}
#endif
};
// SystemDateOut setter
// (setter disabled for DateTime type)

  
// LocalDateRetVal flow acquisition
// (input disabled for UI_16 type)
// LocalDateRetVal flow synthesis
// (output disabled for UI_16 type)
// LocalDateRetVal getter
UI_16 get_LocalDateRetVal(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_LocalDateRetVal==TRUE) {
return diag.LocalDateRetVal;
} else {
#endif
return dre.LocalDateRetVal;
#ifdef _DIAG_ACTIVE
}
#endif
};
// LocalDateRetVal setter
// (setter disabled for UI_16 type)

  
// LocalDateOut flow acquisition
// (input disabled for DateTime type)
// LocalDateOut flow synthesis
// (output disabled for DateTime type)
// LocalDateOut getter
t_datetime get_LocalDateOut(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_LocalDateOut==TRUE) {
return diag.LocalDateOut;
} else {
#endif
return dre.LocalDateOut;
#ifdef _DIAG_ACTIVE
}
#endif
};
// LocalDateOut setter
// (setter disabled for DateTime type)

  
// WriteDateTime flow acquisition
// (input disabled for DateTime type)
// WriteDateTime flow synthesis
// (output disabled for DateTime type)
// WriteDateTime getter
t_datetime get_WriteDateTime(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_WriteDateTime==TRUE) {
return diag.WriteDateTime;
} else {
#endif
return dre.WriteDateTime;
#ifdef _DIAG_ACTIVE
}
#endif
};
// WriteDateTime setter
// (setter disabled for DateTime type)

  
// WriteDateOut flow acquisition
// (input disabled for DateTime type)
// WriteDateOut flow synthesis
// (output disabled for DateTime type)
// WriteDateOut getter
t_datetime get_WriteDateOut(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_WriteDateOut==TRUE) {
return diag.WriteDateOut;
} else {
#endif
return dre.WriteDateOut;
#ifdef _DIAG_ACTIVE
}
#endif
};
// WriteDateOut setter
// (setter disabled for DateTime type)

  
// StatuSetTime flow acquisition
// (input disabled for DateTime type)
// StatuSetTime flow synthesis
// (output disabled for DateTime type)
// StatuSetTime getter
t_datetime get_StatuSetTime(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_StatuSetTime==TRUE) {
return diag.StatuSetTime;
} else {
#endif
return dre.StatuSetTime;
#ifdef _DIAG_ACTIVE
}
#endif
};
// StatuSetTime setter
// (setter disabled for DateTime type)

  
// PeakMax flow acquisition
// (input disabled for UI_16 type)
// PeakMax flow synthesis
// (output disabled for UI_16 type)
// PeakMax getter
UI_16 get_PeakMax(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakMax==TRUE) {
return diag.PeakMax;
} else {
#endif
return dre.PeakMax;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakMax setter
// (setter disabled for UI_16 type)

  
// PeakSetpoint flow acquisition
// (input disabled for UI_16 type)
// PeakSetpoint flow synthesis
// (output disabled for UI_16 type)
// PeakSetpoint getter
UI_16 get_PeakSetpoint(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakSetpoint==TRUE) {
return diag.PeakSetpoint;
} else {
#endif
return dre.PeakSetpoint;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakSetpoint setter
// (setter disabled for UI_16 type)

  
// PeakMin flow acquisition
// (input disabled for UI_16 type)
// PeakMin flow synthesis
// (output disabled for UI_16 type)
// PeakMin getter
UI_16 get_PeakMin(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakMin==TRUE) {
return diag.PeakMin;
} else {
#endif
return dre.PeakMin;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakMin setter
// (setter disabled for UI_16 type)

  
// PlateauMax flow acquisition
// (input disabled for UI_16 type)
// PlateauMax flow synthesis
// (output disabled for UI_16 type)
// PlateauMax getter
UI_16 get_PlateauMax(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauMax==TRUE) {
return diag.PlateauMax;
} else {
#endif
return dre.PlateauMax;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauMax setter
// (setter disabled for UI_16 type)

  
// PlateauSetpoint flow acquisition
// (input disabled for UI_16 type)
// PlateauSetpoint flow synthesis
// (output disabled for UI_16 type)
// PlateauSetpoint getter
UI_16 get_PlateauSetpoint(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauSetpoint==TRUE) {
return diag.PlateauSetpoint;
} else {
#endif
return dre.PlateauSetpoint;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauSetpoint setter
// (setter disabled for UI_16 type)

  
// PlateauMin flow acquisition
// (input disabled for UI_16 type)
// PlateauMin flow synthesis
// (output disabled for UI_16 type)
// PlateauMin getter
UI_16 get_PlateauMin(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauMin==TRUE) {
return diag.PlateauMin;
} else {
#endif
return dre.PlateauMin;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauMin setter
// (setter disabled for UI_16 type)

  
// PeepMax flow acquisition
// (input disabled for UI_16 type)
// PeepMax flow synthesis
// (output disabled for UI_16 type)
// PeepMax getter
UI_16 get_PeepMax(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepMax==TRUE) {
return diag.PeepMax;
} else {
#endif
return dre.PeepMax;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepMax setter
// (setter disabled for UI_16 type)

  
// PeepSetpoint flow acquisition
// (input disabled for UI_16 type)
// PeepSetpoint flow synthesis
// (output disabled for UI_16 type)
// PeepSetpoint getter
UI_16 get_PeepSetpoint(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepSetpoint==TRUE) {
return diag.PeepSetpoint;
} else {
#endif
return dre.PeepSetpoint;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepSetpoint setter
// (setter disabled for UI_16 type)

  
// PeepMin flow acquisition
// (input disabled for UI_16 type)
// PeepMin flow synthesis
// (output disabled for UI_16 type)
// PeepMin getter
UI_16 get_PeepMin(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepMin==TRUE) {
return diag.PeepMin;
} else {
#endif
return dre.PeepMin;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepMin setter
// (setter disabled for UI_16 type)

  
// FrequencyMax flow acquisition
// (input disabled for UI_16 type)
// FrequencyMax flow synthesis
// (output disabled for UI_16 type)
// FrequencyMax getter
UI_16 get_FrequencyMax(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyMax==TRUE) {
return diag.FrequencyMax;
} else {
#endif
return dre.FrequencyMax;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyMax setter
// (setter disabled for UI_16 type)

  
// FrequencySetpoint flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpoint flow synthesis
// (output disabled for UI_16 type)
// FrequencySetpoint getter
UI_16 get_FrequencySetpoint(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencySetpoint==TRUE) {
return diag.FrequencySetpoint;
} else {
#endif
return dre.FrequencySetpoint;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencySetpoint setter
// (setter disabled for UI_16 type)

  
// FrequencyMin flow acquisition
// (input disabled for UI_16 type)
// FrequencyMin flow synthesis
// (output disabled for UI_16 type)
// FrequencyMin getter
UI_16 get_FrequencyMin(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencyMin==TRUE) {
return diag.FrequencyMin;
} else {
#endif
return dre.FrequencyMin;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencyMin setter
// (setter disabled for UI_16 type)

  
// TidalVolumenMax flow acquisition
// (input disabled for UI_16 type)
// TidalVolumenMax flow synthesis
// (output disabled for UI_16 type)
// TidalVolumenMax getter
UI_16 get_TidalVolumenMax(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumenMax==TRUE) {
return diag.TidalVolumenMax;
} else {
#endif
return dre.TidalVolumenMax;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumenMax setter
// (setter disabled for UI_16 type)

  
// TidalVolumenSetpoint flow acquisition
// (input disabled for UI_16 type)
// TidalVolumenSetpoint flow synthesis
// (output disabled for UI_16 type)
// TidalVolumenSetpoint getter
UI_16 get_TidalVolumenSetpoint(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumenSetpoint==TRUE) {
return diag.TidalVolumenSetpoint;
} else {
#endif
return dre.TidalVolumenSetpoint;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumenSetpoint setter
// (setter disabled for UI_16 type)

  
// TidalVolumenMin flow acquisition
// (input disabled for UI_16 type)
// TidalVolumenMin flow synthesis
// (output disabled for UI_16 type)
// TidalVolumenMin getter
UI_16 get_TidalVolumenMin(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumenMin==TRUE) {
return diag.TidalVolumenMin;
} else {
#endif
return dre.TidalVolumenMin;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumenMin setter
// (setter disabled for UI_16 type)

  
// MinimumVolumeMax flow acquisition
// (input disabled for UI_16 type)
// MinimumVolumeMax flow synthesis
// (output disabled for UI_16 type)
// MinimumVolumeMax getter
UI_16 get_MinimumVolumeMax(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinimumVolumeMax==TRUE) {
return diag.MinimumVolumeMax;
} else {
#endif
return dre.MinimumVolumeMax;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinimumVolumeMax setter
// (setter disabled for UI_16 type)

  
// MinimumVolumeSetpoint flow acquisition
// (input disabled for UI_16 type)
// MinimumVolumeSetpoint flow synthesis
// (output disabled for UI_16 type)
// MinimumVolumeSetpoint getter
UI_16 get_MinimumVolumeSetpoint(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinimumVolumeSetpoint==TRUE) {
return diag.MinimumVolumeSetpoint;
} else {
#endif
return dre.MinimumVolumeSetpoint;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinimumVolumeSetpoint setter
// (setter disabled for UI_16 type)

  
// MinimumVolumeMin flow acquisition
// (input disabled for UI_16 type)
// MinimumVolumeMin flow synthesis
// (output disabled for UI_16 type)
// MinimumVolumeMin getter
UI_16 get_MinimumVolumeMin(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinimumVolumeMin==TRUE) {
return diag.MinimumVolumeMin;
} else {
#endif
return dre.MinimumVolumeMin;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinimumVolumeMin setter
// (setter disabled for UI_16 type)

  
// TriggerSetpoint flow acquisition
// (input disabled for UI_16 type)
// TriggerSetpoint flow synthesis
// (output disabled for UI_16 type)
// TriggerSetpoint getter
UI_16 get_TriggerSetpoint(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerSetpoint==TRUE) {
return diag.TriggerSetpoint;
} else {
#endif
return dre.TriggerSetpoint;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerSetpoint setter
// (setter disabled for UI_16 type)

  
// RecruitmentTimer flow acquisition
// (input disabled for TimeUI_16 type)
// RecruitmentTimer flow synthesis
// (output disabled for TimeUI_16 type)
// RecruitmentTimer getter
UI_16 get_RecruitmentTimer(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RecruitmentTimer==TRUE) {
return diag.RecruitmentTimer;
} else {
#endif
return dre.RecruitmentTimer;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RecruitmentTimer setter
// (setter disabled for TimeUI_16 type)

  
// RecruitmentElap flow acquisition
// (input disabled for TimeUI_16 type)
// RecruitmentElap flow synthesis
// (output disabled for TimeUI_16 type)
// RecruitmentElap getter
UI_16 get_RecruitmentElap(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RecruitmentElap==TRUE) {
return diag.RecruitmentElap;
} else {
#endif
return dre.RecruitmentElap;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RecruitmentElap setter
// (setter disabled for TimeUI_16 type)

  
// TimeStart flow acquisition
// (input disabled for DateTime type)
// TimeStart flow synthesis
// (output disabled for DateTime type)
// TimeStart getter
t_datetime get_TimeStart(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TimeStart==TRUE) {
return diag.TimeStart;
} else {
#endif
return dre.TimeStart;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TimeStart setter
// (setter disabled for DateTime type)

  
// TimeEnd flow acquisition
// (input disabled for DateTime type)
// TimeEnd flow synthesis
// (output disabled for DateTime type)
// TimeEnd getter
t_datetime get_TimeEnd(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TimeEnd==TRUE) {
return diag.TimeEnd;
} else {
#endif
return dre.TimeEnd;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TimeEnd setter
// (setter disabled for DateTime type)

  
// FlowSetpoint flow acquisition
// (input disabled for UI_16 type)
// FlowSetpoint flow synthesis
// (output disabled for UI_16 type)
// FlowSetpoint getter
UI_16 get_FlowSetpoint(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FlowSetpoint==TRUE) {
return diag.FlowSetpoint;
} else {
#endif
return dre.FlowSetpoint;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FlowSetpoint setter
// (setter disabled for UI_16 type)

  
// PIDOutReal flow acquisition
// (input disabled for FL_16 type)
// PIDOutReal flow synthesis
// (output disabled for FL_16 type)
// PIDOutReal getter
FL_16 get_PIDOutReal(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PIDOutReal==TRUE) {
return diag.PIDOutReal;
} else {
#endif
return dre.PIDOutReal;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PIDOutReal setter
// (setter disabled for FL_16 type)

  
// PIDOutInt flow acquisition
// (input disabled for UI_16 type)
// PIDOutInt flow synthesis
// (output disabled for UI_16 type)
// PIDOutInt getter
UI_16 get_PIDOutInt(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PIDOutInt==TRUE) {
return diag.PIDOutInt;
} else {
#endif
return dre.PIDOutInt;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PIDOutInt setter
// (setter disabled for UI_16 type)

  
// PIDOutPWM flow acquisition
// (input disabled for UI_16 type)
// PIDOutPWM flow synthesis
// (output disabled for UI_16 type)
// PIDOutPWM getter
UI_16 get_PIDOutPWM(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PIDOutPWM==TRUE) {
return diag.PIDOutPWM;
} else {
#endif
return dre.PIDOutPWM;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PIDOutPWM setter
// (setter disabled for UI_16 type)

  
// PIDOutMotor flow acquisition
// (input disabled for UI_16 type)
// PIDOutMotor flow synthesis
// (output disabled for UI_16 type)
// PIDOutMotor getter
UI_16 get_PIDOutMotor(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PIDOutMotor==TRUE) {
return diag.PIDOutMotor;
} else {
#endif
return dre.PIDOutMotor;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PIDOutMotor setter
// (setter disabled for UI_16 type)

  
// PeakSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)
// PeakSetpointPreAlarmHI getter
UI_16 get_PeakSetpointPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakSetpointPreAlarmHI==TRUE) {
return diag.PeakSetpointPreAlarmHI;
} else {
#endif
return dre.PeakSetpointPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakSetpointPreAlarmHI setter
// (setter disabled for UI_16 type)

  
// PeakSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)
// PeakSetpointAlarmHI getter
UI_16 get_PeakSetpointAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakSetpointAlarmHI==TRUE) {
return diag.PeakSetpointAlarmHI;
} else {
#endif
return dre.PeakSetpointAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakSetpointAlarmHI setter
// (setter disabled for UI_16 type)

  
// PeakSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// PeakSetpointPreAlarmLOW getter
UI_16 get_PeakSetpointPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakSetpointPreAlarmLOW==TRUE) {
return diag.PeakSetpointPreAlarmLOW;
} else {
#endif
return dre.PeakSetpointPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type)

  
// PeakSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// PeakSetpointAlarmLOW getter
UI_16 get_PeakSetpointAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakSetpointAlarmLOW==TRUE) {
return diag.PeakSetpointAlarmLOW;
} else {
#endif
return dre.PeakSetpointAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakSetpointAlarmLOW setter
// (setter disabled for UI_16 type)

  
// PeakSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)
// PeakSetpointHysteresisHI getter
UI_16 get_PeakSetpointHysteresisHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakSetpointHysteresisHI==TRUE) {
return diag.PeakSetpointHysteresisHI;
} else {
#endif
return dre.PeakSetpointHysteresisHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakSetpointHysteresisHI setter
// (setter disabled for UI_16 type)

  
// PeakSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// PeakSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)
// PeakSetpointHysteresisLOW getter
UI_16 get_PeakSetpointHysteresisLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeakSetpointHysteresisLOW==TRUE) {
return diag.PeakSetpointHysteresisLOW;
} else {
#endif
return dre.PeakSetpointHysteresisLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeakSetpointHysteresisLOW setter
// (setter disabled for UI_16 type)

  
// PlateauSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PlateauSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)
// PlateauSetpointPreAlarmHI getter
UI_16 get_PlateauSetpointPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauSetpointPreAlarmHI==TRUE) {
return diag.PlateauSetpointPreAlarmHI;
} else {
#endif
return dre.PlateauSetpointPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauSetpointPreAlarmHI setter
// (setter disabled for UI_16 type)

  
// PlateauSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PlateauSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)
// PlateauSetpointAlarmHI getter
UI_16 get_PlateauSetpointAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauSetpointAlarmHI==TRUE) {
return diag.PlateauSetpointAlarmHI;
} else {
#endif
return dre.PlateauSetpointAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauSetpointAlarmHI setter
// (setter disabled for UI_16 type)

  
// PlateauSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PlateauSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// PlateauSetpointPreAlarmLOW getter
UI_16 get_PlateauSetpointPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateauSetpointPreAlarmLOW==TRUE) {
return diag.PlateauSetpointPreAlarmLOW;
} else {
#endif
return dre.PlateauSetpointPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateauSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type)

  
// PlateaukSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PlateaukSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// PlateaukSetpointAlarmLOW getter
UI_16 get_PlateaukSetpointAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateaukSetpointAlarmLOW==TRUE) {
return diag.PlateaukSetpointAlarmLOW;
} else {
#endif
return dre.PlateaukSetpointAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateaukSetpointAlarmLOW setter
// (setter disabled for UI_16 type)

  
// PlateaukSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// PlateaukSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)
// PlateaukSetpointHysteresisHI getter
UI_16 get_PlateaukSetpointHysteresisHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateaukSetpointHysteresisHI==TRUE) {
return diag.PlateaukSetpointHysteresisHI;
} else {
#endif
return dre.PlateaukSetpointHysteresisHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateaukSetpointHysteresisHI setter
// (setter disabled for UI_16 type)

  
// PlateaukSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// PlateaukSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)
// PlateaukSetpointHysteresisLOW getter
UI_16 get_PlateaukSetpointHysteresisLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PlateaukSetpointHysteresisLOW==TRUE) {
return diag.PlateaukSetpointHysteresisLOW;
} else {
#endif
return dre.PlateaukSetpointHysteresisLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PlateaukSetpointHysteresisLOW setter
// (setter disabled for UI_16 type)

  
// PeepSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)
// PeepSetpointPreAlarmHI getter
UI_16 get_PeepSetpointPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepSetpointPreAlarmHI==TRUE) {
return diag.PeepSetpointPreAlarmHI;
} else {
#endif
return dre.PeepSetpointPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepSetpointPreAlarmHI setter
// (setter disabled for UI_16 type)

  
// PeepSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)
// PeepSetpointAlarmHI getter
UI_16 get_PeepSetpointAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepSetpointAlarmHI==TRUE) {
return diag.PeepSetpointAlarmHI;
} else {
#endif
return dre.PeepSetpointAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepSetpointAlarmHI setter
// (setter disabled for UI_16 type)

  
// PeepSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// PeepSetpointPreAlarmLOW getter
UI_16 get_PeepSetpointPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepSetpointPreAlarmLOW==TRUE) {
return diag.PeepSetpointPreAlarmLOW;
} else {
#endif
return dre.PeepSetpointPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type)

  
// PeepSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// PeepSetpointAlarmLOW getter
UI_16 get_PeepSetpointAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepSetpointAlarmLOW==TRUE) {
return diag.PeepSetpointAlarmLOW;
} else {
#endif
return dre.PeepSetpointAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepSetpointAlarmLOW setter
// (setter disabled for UI_16 type)

  
// PeepSetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)
// PeepSetpointHysteresisHI getter
UI_16 get_PeepSetpointHysteresisHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepSetpointHysteresisHI==TRUE) {
return diag.PeepSetpointHysteresisHI;
} else {
#endif
return dre.PeepSetpointHysteresisHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepSetpointHysteresisHI setter
// (setter disabled for UI_16 type)

  
// PeepSetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// PeepSetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)
// PeepSetpointHysteresisLOW getter
UI_16 get_PeepSetpointHysteresisLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_PeepSetpointHysteresisLOW==TRUE) {
return diag.PeepSetpointHysteresisLOW;
} else {
#endif
return dre.PeepSetpointHysteresisLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// PeepSetpointHysteresisLOW setter
// (setter disabled for UI_16 type)

  
// FrequencySetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)
// FrequencySetpointPreAlarmHI getter
UI_16 get_FrequencySetpointPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencySetpointPreAlarmHI==TRUE) {
return diag.FrequencySetpointPreAlarmHI;
} else {
#endif
return dre.FrequencySetpointPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencySetpointPreAlarmHI setter
// (setter disabled for UI_16 type)

  
// FrequencySetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)
// FrequencySetpointAlarmHI getter
UI_16 get_FrequencySetpointAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencySetpointAlarmHI==TRUE) {
return diag.FrequencySetpointAlarmHI;
} else {
#endif
return dre.FrequencySetpointAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencySetpointAlarmHI setter
// (setter disabled for UI_16 type)

  
// FrequencySetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// FrequencySetpointPreAlarmLOW getter
UI_16 get_FrequencySetpointPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencySetpointPreAlarmLOW==TRUE) {
return diag.FrequencySetpointPreAlarmLOW;
} else {
#endif
return dre.FrequencySetpointPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencySetpointPreAlarmLOW setter
// (setter disabled for UI_16 type)

  
// FrequencySetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// FrequencySetpointAlarmLOW getter
UI_16 get_FrequencySetpointAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencySetpointAlarmLOW==TRUE) {
return diag.FrequencySetpointAlarmLOW;
} else {
#endif
return dre.FrequencySetpointAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencySetpointAlarmLOW setter
// (setter disabled for UI_16 type)

  
// FrequencySetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)
// FrequencySetpointHysteresisHI getter
UI_16 get_FrequencySetpointHysteresisHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencySetpointHysteresisHI==TRUE) {
return diag.FrequencySetpointHysteresisHI;
} else {
#endif
return dre.FrequencySetpointHysteresisHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencySetpointHysteresisHI setter
// (setter disabled for UI_16 type)

  
// FrequencySetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// FrequencySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)
// FrequencySetpointHysteresisLOW getter
UI_16 get_FrequencySetpointHysteresisLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FrequencySetpointHysteresisLOW==TRUE) {
return diag.FrequencySetpointHysteresisLOW;
} else {
#endif
return dre.FrequencySetpointHysteresisLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FrequencySetpointHysteresisLOW setter
// (setter disabled for UI_16 type)

  
// TidalVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)
// TidalVolumeSetpointPreAlarmHI getter
UI_16 get_TidalVolumeSetpointPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeSetpointPreAlarmHI==TRUE) {
return diag.TidalVolumeSetpointPreAlarmHI;
} else {
#endif
return dre.TidalVolumeSetpointPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeSetpointPreAlarmHI setter
// (setter disabled for UI_16 type)

  
// TidalVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)
// TidalVolumeSetpointAlarmHI getter
UI_16 get_TidalVolumeSetpointAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeSetpointAlarmHI==TRUE) {
return diag.TidalVolumeSetpointAlarmHI;
} else {
#endif
return dre.TidalVolumeSetpointAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeSetpointAlarmHI setter
// (setter disabled for UI_16 type)

  
// TidalVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// TidalVolumeSetpointPreAlarmLOW getter
UI_16 get_TidalVolumeSetpointPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeSetpointPreAlarmLOW==TRUE) {
return diag.TidalVolumeSetpointPreAlarmLOW;
} else {
#endif
return dre.TidalVolumeSetpointPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type)

  
// TidalVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// TidalVolumeSetpointAlarmLOW getter
UI_16 get_TidalVolumeSetpointAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeSetpointAlarmLOW==TRUE) {
return diag.TidalVolumeSetpointAlarmLOW;
} else {
#endif
return dre.TidalVolumeSetpointAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeSetpointAlarmLOW setter
// (setter disabled for UI_16 type)

  
// TidalVolumeHystersisHI flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeHystersisHI flow synthesis
// (output disabled for UI_16 type)
// TidalVolumeHystersisHI getter
UI_16 get_TidalVolumeHystersisHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeHystersisHI==TRUE) {
return diag.TidalVolumeHystersisHI;
} else {
#endif
return dre.TidalVolumeHystersisHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeHystersisHI setter
// (setter disabled for UI_16 type)

  
// TidalVolumeHystersisLOW flow acquisition
// (input disabled for UI_16 type)
// TidalVolumeHystersisLOW flow synthesis
// (output disabled for UI_16 type)
// TidalVolumeHystersisLOW getter
UI_16 get_TidalVolumeHystersisLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TidalVolumeHystersisLOW==TRUE) {
return diag.TidalVolumeHystersisLOW;
} else {
#endif
return dre.TidalVolumeHystersisLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TidalVolumeHystersisLOW setter
// (setter disabled for UI_16 type)

  
// MinuteVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)
// MinuteVolumeSetpointPreAlarmHI getter
UI_16 get_MinuteVolumeSetpointPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeSetpointPreAlarmHI==TRUE) {
return diag.MinuteVolumeSetpointPreAlarmHI;
} else {
#endif
return dre.MinuteVolumeSetpointPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeSetpointPreAlarmHI setter
// (setter disabled for UI_16 type)

  
// MinuteVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)
// MinuteVolumeSetpointAlarmHI getter
UI_16 get_MinuteVolumeSetpointAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeSetpointAlarmHI==TRUE) {
return diag.MinuteVolumeSetpointAlarmHI;
} else {
#endif
return dre.MinuteVolumeSetpointAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeSetpointAlarmHI setter
// (setter disabled for UI_16 type)

  
// MinuteVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// MinuteVolumeSetpointPreAlarmLOW getter
UI_16 get_MinuteVolumeSetpointPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeSetpointPreAlarmLOW==TRUE) {
return diag.MinuteVolumeSetpointPreAlarmLOW;
} else {
#endif
return dre.MinuteVolumeSetpointPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type)

  
// MinuteVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// MinuteVolumeSetpointAlarmLOW getter
UI_16 get_MinuteVolumeSetpointAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeSetpointAlarmLOW==TRUE) {
return diag.MinuteVolumeSetpointAlarmLOW;
} else {
#endif
return dre.MinuteVolumeSetpointAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeSetpointAlarmLOW setter
// (setter disabled for UI_16 type)

  
// MinuteVolumeHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type)
// MinuteVolumeHysteresisHI getter
UI_16 get_MinuteVolumeHysteresisHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeHysteresisHI==TRUE) {
return diag.MinuteVolumeHysteresisHI;
} else {
#endif
return dre.MinuteVolumeHysteresisHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeHysteresisHI setter
// (setter disabled for UI_16 type)

  
// MinuteVolumeHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// MinuteVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type)
// MinuteVolumeHysteresisLOW getter
UI_16 get_MinuteVolumeHysteresisLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MinuteVolumeHysteresisLOW==TRUE) {
return diag.MinuteVolumeHysteresisLOW;
} else {
#endif
return dre.MinuteVolumeHysteresisLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MinuteVolumeHysteresisLOW setter
// (setter disabled for UI_16 type)

  
// TriggeVolumeSetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// TriggeVolumeSetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)
// TriggeVolumeSetpointPreAlarmHI getter
UI_16 get_TriggeVolumeSetpointPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggeVolumeSetpointPreAlarmHI==TRUE) {
return diag.TriggeVolumeSetpointPreAlarmHI;
} else {
#endif
return dre.TriggeVolumeSetpointPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggeVolumeSetpointPreAlarmHI setter
// (setter disabled for UI_16 type)

  
// TriggerVolumeSetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// TriggerVolumeSetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)
// TriggerVolumeSetpointAlarmHI getter
UI_16 get_TriggerVolumeSetpointAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerVolumeSetpointAlarmHI==TRUE) {
return diag.TriggerVolumeSetpointAlarmHI;
} else {
#endif
return dre.TriggerVolumeSetpointAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerVolumeSetpointAlarmHI setter
// (setter disabled for UI_16 type)

  
// TriggerVolumeSetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// TriggerVolumeSetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// TriggerVolumeSetpointPreAlarmLOW getter
UI_16 get_TriggerVolumeSetpointPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerVolumeSetpointPreAlarmLOW==TRUE) {
return diag.TriggerVolumeSetpointPreAlarmLOW;
} else {
#endif
return dre.TriggerVolumeSetpointPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerVolumeSetpointPreAlarmLOW setter
// (setter disabled for UI_16 type)

  
// TriggerVolumeSetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// TriggerVolumeSetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// TriggerVolumeSetpointAlarmLOW getter
UI_16 get_TriggerVolumeSetpointAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerVolumeSetpointAlarmLOW==TRUE) {
return diag.TriggerVolumeSetpointAlarmLOW;
} else {
#endif
return dre.TriggerVolumeSetpointAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerVolumeSetpointAlarmLOW setter
// (setter disabled for UI_16 type)

  
// TriggerVolumeHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// TriggerVolumeHysteresisHI flow synthesis
// (output disabled for UI_16 type)
// TriggerVolumeHysteresisHI getter
UI_16 get_TriggerVolumeHysteresisHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerVolumeHysteresisHI==TRUE) {
return diag.TriggerVolumeHysteresisHI;
} else {
#endif
return dre.TriggerVolumeHysteresisHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerVolumeHysteresisHI setter
// (setter disabled for UI_16 type)

  
// TriggerVolumeHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// TriggerVolumeHysteresisLOW flow synthesis
// (output disabled for UI_16 type)
// TriggerVolumeHysteresisLOW getter
UI_16 get_TriggerVolumeHysteresisLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_TriggerVolumeHysteresisLOW==TRUE) {
return diag.TriggerVolumeHysteresisLOW;
} else {
#endif
return dre.TriggerVolumeHysteresisLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// TriggerVolumeHysteresisLOW setter
// (setter disabled for UI_16 type)

  
// BatterySetpointPreAlarmHI flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointPreAlarmHI flow synthesis
// (output disabled for UI_16 type)
// BatterySetpointPreAlarmHI getter
UI_16 get_BatterySetpointPreAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatterySetpointPreAlarmHI==TRUE) {
return diag.BatterySetpointPreAlarmHI;
} else {
#endif
return dre.BatterySetpointPreAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatterySetpointPreAlarmHI setter
// (setter disabled for UI_16 type)

  
// BatterySetpointAlarmHI flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointAlarmHI flow synthesis
// (output disabled for UI_16 type)
// BatterySetpointAlarmHI getter
UI_16 get_BatterySetpointAlarmHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatterySetpointAlarmHI==TRUE) {
return diag.BatterySetpointAlarmHI;
} else {
#endif
return dre.BatterySetpointAlarmHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatterySetpointAlarmHI setter
// (setter disabled for UI_16 type)

  
// BatterySetpointPreAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointPreAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// BatterySetpointPreAlarmLOW getter
UI_16 get_BatterySetpointPreAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatterySetpointPreAlarmLOW==TRUE) {
return diag.BatterySetpointPreAlarmLOW;
} else {
#endif
return dre.BatterySetpointPreAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatterySetpointPreAlarmLOW setter
// (setter disabled for UI_16 type)

  
// BatterySetpointAlarmLOW flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointAlarmLOW flow synthesis
// (output disabled for UI_16 type)
// BatterySetpointAlarmLOW getter
UI_16 get_BatterySetpointAlarmLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatterySetpointAlarmLOW==TRUE) {
return diag.BatterySetpointAlarmLOW;
} else {
#endif
return dre.BatterySetpointAlarmLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatterySetpointAlarmLOW setter
// (setter disabled for UI_16 type)

  
// BatterySetpointHysteresisHI flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointHysteresisHI flow synthesis
// (output disabled for UI_16 type)
// BatterySetpointHysteresisHI getter
UI_16 get_BatterySetpointHysteresisHI(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatterySetpointHysteresisHI==TRUE) {
return diag.BatterySetpointHysteresisHI;
} else {
#endif
return dre.BatterySetpointHysteresisHI;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatterySetpointHysteresisHI setter
// (setter disabled for UI_16 type)

  
// BatterySetpointHysteresisLOW flow acquisition
// (input disabled for UI_16 type)
// BatterySetpointHysteresisLOW flow synthesis
// (output disabled for UI_16 type)
// BatterySetpointHysteresisLOW getter
UI_16 get_BatterySetpointHysteresisLOW(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_BatterySetpointHysteresisLOW==TRUE) {
return diag.BatterySetpointHysteresisLOW;
} else {
#endif
return dre.BatterySetpointHysteresisLOW;
#ifdef _DIAG_ACTIVE
}
#endif
};
// BatterySetpointHysteresisLOW setter
// (setter disabled for UI_16 type)

  
// Frecuency flow acquisition
// (input disabled for UI_16 type)
// Frecuency flow synthesis
// (output disabled for UI_16 type)
// Frecuency getter
UI_16 get_Frecuency(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Frecuency==TRUE) {
return diag.Frecuency;
} else {
#endif
return dre.Frecuency;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Frecuency setter
// (setter disabled for UI_16 type)

  
// Duty_Cycle flow acquisition
// (input disabled for UI_16 type)
// Duty_Cycle flow synthesis
// (output disabled for UI_16 type)
// Duty_Cycle getter
UI_16 get_Duty_Cycle(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Duty_Cycle==TRUE) {
return diag.Duty_Cycle;
} else {
#endif
return dre.Duty_Cycle;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Duty_Cycle setter
// (setter disabled for UI_16 type)

  
// HOME_Position flow acquisition
// (input disabled for UI_16 type)
// HOME_Position flow synthesis
// (output disabled for UI_16 type)
// HOME_Position getter
UI_16 get_HOME_Position(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_HOME_Position==TRUE) {
return diag.HOME_Position;
} else {
#endif
return dre.HOME_Position;
#ifdef _DIAG_ACTIVE
}
#endif
};
// HOME_Position setter
// (setter disabled for UI_16 type)

  
// ABSOLUTE_Position flow acquisition
// (input disabled for UI_16 type)
// ABSOLUTE_Position flow synthesis
// (output disabled for UI_16 type)
// ABSOLUTE_Position getter
UI_16 get_ABSOLUTE_Position(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_ABSOLUTE_Position==TRUE) {
return diag.ABSOLUTE_Position;
} else {
#endif
return dre.ABSOLUTE_Position;
#ifdef _DIAG_ACTIVE
}
#endif
};
// ABSOLUTE_Position setter
// (setter disabled for UI_16 type)

  
// ABSOLUTE_Velocity flow acquisition
// (input disabled for UI_16 type)
// ABSOLUTE_Velocity flow synthesis
// (output disabled for UI_16 type)
// ABSOLUTE_Velocity getter
UI_16 get_ABSOLUTE_Velocity(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_ABSOLUTE_Velocity==TRUE) {
return diag.ABSOLUTE_Velocity;
} else {
#endif
return dre.ABSOLUTE_Velocity;
#ifdef _DIAG_ACTIVE
}
#endif
};
// ABSOLUTE_Velocity setter
// (setter disabled for UI_16 type)

  
// RELATIVE_Distance flow acquisition
// (input disabled for UI_16 type)
// RELATIVE_Distance flow synthesis
// (output disabled for UI_16 type)
// RELATIVE_Distance getter
UI_16 get_RELATIVE_Distance(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RELATIVE_Distance==TRUE) {
return diag.RELATIVE_Distance;
} else {
#endif
return dre.RELATIVE_Distance;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RELATIVE_Distance setter
// (setter disabled for UI_16 type)

  
// RELATIVE_Velocity flow acquisition
// (input disabled for UI_16 type)
// RELATIVE_Velocity flow synthesis
// (output disabled for UI_16 type)
// RELATIVE_Velocity getter
UI_16 get_RELATIVE_Velocity(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RELATIVE_Velocity==TRUE) {
return diag.RELATIVE_Velocity;
} else {
#endif
return dre.RELATIVE_Velocity;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RELATIVE_Velocity setter
// (setter disabled for UI_16 type)

  
// Velocity_Velocity flow acquisition
// (input disabled for UI_16 type)
// Velocity_Velocity flow synthesis
// (output disabled for UI_16 type)
// Velocity_Velocity getter
UI_16 get_Velocity_Velocity(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Velocity_Velocity==TRUE) {
return diag.Velocity_Velocity;
} else {
#endif
return dre.Velocity_Velocity;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Velocity_Velocity setter
// (setter disabled for UI_16 type)

  
// JOG_Velocity flow acquisition
// (input disabled for UI_16 type)
// JOG_Velocity flow synthesis
// (output disabled for UI_16 type)
// JOG_Velocity getter
UI_16 get_JOG_Velocity(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_JOG_Velocity==TRUE) {
return diag.JOG_Velocity;
} else {
#endif
return dre.JOG_Velocity;
#ifdef _DIAG_ACTIVE
}
#endif
};
// JOG_Velocity setter
// (setter disabled for UI_16 type)

  
// EmergencyStop flow acquisition
void acquire_EmergencyStop(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_EmergencyStop==TRUE) { 
dre.EmergencyStop = diag.EmergencyStop; 
} else { 
#endif 
dre.EmergencyStop = digitalRead(PIN_EmergencyStop); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

// EmergencyStop flow synthesis
// (output disabled for DI type)
// EmergencyStop getter

// EmergencyStop setter
// (setter disabled for DI type)

  
// TurnAxisHwHi flow acquisition
void acquire_TurnAxisHwHi(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_TurnAxisHwHi==TRUE) { 
dre.TurnAxisHwHi = diag.TurnAxisHwHi; 
} else { 
#endif 
dre.TurnAxisHwHi = digitalRead(PIN_TurnAxisHwHi); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// TurnAxisHwHi flow synthesis
// (output disabled for DI_pu type)
// TurnAxisHwHi getter
// (getter disabled for DI_pu type)
// TurnAxisHwHi setter
// (setter disabled for DI_pu type)

  
// BeaconLightRed flow acquisition
// (input disabled for DO type)
// BeaconLightRed flow synthesis
void synthesize_BeaconLightRed(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_BeaconLightRed==TRUE) { 
digitalWrite(PIN_BeaconLightRed,diag.BeaconLightRed); 
} else { 
#endif 
digitalWrite(PIN_BeaconLightRed,dre.BeaconLightRed); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// BeaconLightRed getter
// (getter disabled for DO type)
// BeaconLightRed setter


  
// BeaconLightOrange flow acquisition
// (input disabled for DO type)
// BeaconLightOrange flow synthesis
void synthesize_BeaconLightOrange(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_BeaconLightOrange==TRUE) { 
digitalWrite(PIN_BeaconLightOrange,diag.BeaconLightOrange); 
} else { 
#endif 
digitalWrite(PIN_BeaconLightOrange,dre.BeaconLightOrange); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// BeaconLightOrange getter
// (getter disabled for DO type)
// BeaconLightOrange setter


  
// TurnAxisPulse flow acquisition
// (input disabled for DO type)
// TurnAxisPulse flow synthesis
void synthesize_TurnAxisPulse(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_TurnAxisPulse==TRUE) { 
digitalWrite(PIN_TurnAxisPulse,diag.TurnAxisPulse); 
} else { 
#endif 
digitalWrite(PIN_TurnAxisPulse,dre.TurnAxisPulse); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// TurnAxisPulse getter
// (getter disabled for DO type)
// TurnAxisPulse setter


  
// TurnAxisDir flow acquisition
// (input disabled for DO type)
// TurnAxisDir flow synthesis
void synthesize_TurnAxisDir(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_TurnAxisDir==TRUE) { 
digitalWrite(PIN_TurnAxisDir,diag.TurnAxisDir); 
} else { 
#endif 
digitalWrite(PIN_TurnAxisDir,dre.TurnAxisDir); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// TurnAxisDir getter
// (getter disabled for DO type)
// TurnAxisDir setter


  
// TurnAxisEnable flow acquisition
// (input disabled for DO type)
// TurnAxisEnable flow synthesis
void synthesize_TurnAxisEnable(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_TurnAxisEnable==TRUE) { 
digitalWrite(PIN_TurnAxisEnable,diag.TurnAxisEnable); 
} else { 
#endif 
digitalWrite(PIN_TurnAxisEnable,dre.TurnAxisEnable); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// TurnAxisEnable getter
// (getter disabled for DO type)
// TurnAxisEnable setter


  
// BeaconLightGreen flow acquisition
// (input disabled for DO type)
// BeaconLightGreen flow synthesis
void synthesize_BeaconLightGreen(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_BeaconLightGreen==TRUE) { 
digitalWrite(PIN_BeaconLightGreen,diag.BeaconLightGreen); 
} else { 
#endif 
digitalWrite(PIN_BeaconLightGreen,dre.BeaconLightGreen); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// BeaconLightGreen getter
// (getter disabled for DO type)
// BeaconLightGreen setter


  
// Buzzer flow acquisition
// (input disabled for DO type)
// Buzzer flow synthesis
void synthesize_Buzzer(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_Buzzer==TRUE) { 
digitalWrite(PIN_Buzzer,diag.Buzzer); 
} else { 
#endif 
digitalWrite(PIN_Buzzer,dre.Buzzer); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// Buzzer getter
// (getter disabled for DO type)
// Buzzer setter


  
// PeepValve flow acquisition
// (input disabled for PWM type)
// PeepValve flow synthesis
void synthesize_PeepValve(void){ 
analogWrite(PIN_PeepValve, dre.PeepValve); 
};
// PeepValve getter
// (getter not implemented for PWM type)
// PeepValve setter
// (setter not implemented for PWM type)

  
// TurnAxisHwLow flow acquisition
void acquire_TurnAxisHwLow(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_TurnAxisHwLow==TRUE) { 
dre.TurnAxisHwLow = diag.TurnAxisHwLow; 
} else { 
#endif 
dre.TurnAxisHwLow = digitalRead(PIN_TurnAxisHwLow); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// TurnAxisHwLow flow synthesis
// (output disabled for DI_pu type)
// TurnAxisHwLow getter
// (getter disabled for DI_pu type)
// TurnAxisHwLow setter
// (setter disabled for DI_pu type)

  
// ServoPend flow acquisition
void acquire_ServoPend(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_ServoPend==TRUE) { 
dre.ServoPend = diag.ServoPend; 
} else { 
#endif 
dre.ServoPend = digitalRead(PIN_ServoPend); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};

// ServoPend flow synthesis
// (output disabled for DI type)
// ServoPend getter

// ServoPend setter
// (setter disabled for DI type)

  
// ServoAlarm flow acquisition
void acquire_ServoAlarm(void){ 
#ifdef _DIAG_ACTIVE 
if (diag.enable_ServoAlarm==TRUE) { 
dre.ServoAlarm = diag.ServoAlarm; 
} else { 
#endif 
dre.ServoAlarm = digitalRead(PIN_ServoAlarm); 
#ifdef _DIAG_ACTIVE 
} 
#endif 
};
// ServoAlarm flow synthesis
// (output disabled for DI_pu type)
// ServoAlarm getter
// (getter disabled for DI_pu type)
// ServoAlarm setter
// (setter disabled for DI_pu type)

  
// AtmosfericPressure flow acquisition
void acquire_AtmosfericPressure(void){ 
#ifdef _DIAG_ACTIVE
  if (diag.enable_AtmosfericPressure==TRUE) {
    dre.AtmosfericPressure = diag.AtmosfericPressure;
  } else {
#endif
    dre.AtmosfericPressure = analogRead(PIN_AtmosfericPressure); 
#ifdef _DIAG_ACTIVE
}
#endif
};
// AtmosfericPressure flow synthesis
// (output disabled for ADC type)
// AtmosfericPressure getter

// AtmosfericPressure setter
// (setter disabled for ADC type)

  
// DiferentialPressure flow acquisition
void acquire_DiferentialPressure(void){ 
#ifdef _DIAG_ACTIVE
  if (diag.enable_DiferentialPressure==TRUE) {
    dre.DiferentialPressure = diag.DiferentialPressure;
  } else {
#endif
    dre.DiferentialPressure = analogRead(PIN_DiferentialPressure); 
#ifdef _DIAG_ACTIVE
}
#endif
};
// DiferentialPressure flow synthesis
// (output disabled for ADC type)
// DiferentialPressure getter

// DiferentialPressure setter
// (setter disabled for ADC type)

  
// SerTX flow acquisition
// (input disabled for Bus type)
// SerTX flow synthesis
// (output disabled for Bus type)
// SerTX getter
// (getter disabled for Bus type)
// SerTX setter
// (setter disabled for Bus type)

  
// SerRX flow acquisition
// (input disabled for Bus type)
// SerRX flow synthesis
// (output disabled for Bus type)
// SerRX getter
// (getter disabled for Bus type)
// SerRX setter
// (setter disabled for Bus type)

  
// EmgcyButton flow acquisition
// (input disabled for TBD type)
// EmgcyButton flow synthesis
// (output disabled for TBD type)
// EmgcyButton getter
// (getter disabled for TBD type)
// EmgcyButton setter
// (setter disabled for TBD type)

  
// ApiVersion flow acquisition
// (input disabled for UI_8 type)
// ApiVersion flow synthesis
// (output disabled for UI_8 type)
// ApiVersion getter
UI_8 get_ApiVersion(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_ApiVersion==TRUE) {
return diag.ApiVersion;
} else {
#endif
return dre.ApiVersion;
#ifdef _DIAG_ACTIVE
}
#endif
};
// ApiVersion setter
// (setter disabled for UI_8 type)

  
// MachineUuid flow acquisition
// (input disabled for Variable type)
// MachineUuid flow synthesis
// (output disabled for Variable type)
// MachineUuid getter
t_machineuuid get_MachineUuid(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_MachineUuid==TRUE) {
		return diag.MachineUuid;
	} else {
#endif
		return dre.MachineUuid;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// MachineUuid setter
// (setter disabled for Variable type)

  
// FirmwareVersion flow acquisition
// (input disabled for UI_16 type)
// FirmwareVersion flow synthesis
// (output disabled for UI_16 type)
// FirmwareVersion getter
UI_16 get_FirmwareVersion(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FirmwareVersion==TRUE) {
return diag.FirmwareVersion;
} else {
#endif
return dre.FirmwareVersion;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FirmwareVersion setter
// (setter disabled for UI_16 type)

  
// Uptime15mCounter flow acquisition
// (input disabled for UI_16 type)
// Uptime15mCounter flow synthesis
// (output disabled for UI_16 type)
// Uptime15mCounter getter
UI_16 get_Uptime15mCounter(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Uptime15mCounter==TRUE) {
return diag.Uptime15mCounter;
} else {
#endif
return dre.Uptime15mCounter;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Uptime15mCounter setter
// (setter disabled for UI_16 type)

  
// MutedAlarmSeconds flow acquisition
// (input disabled for UI_8 type)
// MutedAlarmSeconds flow synthesis
// (output disabled for UI_8 type)
// MutedAlarmSeconds getter
UI_8 get_MutedAlarmSeconds(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_MutedAlarmSeconds==TRUE) {
return diag.MutedAlarmSeconds;
} else {
#endif
return dre.MutedAlarmSeconds;
#ifdef _DIAG_ACTIVE
}
#endif
};
// MutedAlarmSeconds setter
// (setter disabled for UI_8 type)

  
// CycleRpmLast30s flow acquisition
// (input disabled for UI_16 type)
// CycleRpmLast30s flow synthesis
// (output disabled for UI_16 type)
// CycleRpmLast30s getter
UI_16 get_CycleRpmLast30s(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_CycleRpmLast30s==TRUE) {
return diag.CycleRpmLast30s;
} else {
#endif
return dre.CycleRpmLast30s;
#ifdef _DIAG_ACTIVE
}
#endif
};
// CycleRpmLast30s setter
// (setter disabled for UI_16 type)

  
// CycleIndications flow acquisition
// (input disabled for Variable type)
// CycleIndications flow synthesis
// (output disabled for Variable type)
// CycleIndications getter
t_cycleindications get_CycleIndications(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_CycleIndications==TRUE) {
		return diag.CycleIndications;
	} else {
#endif
		return dre.CycleIndications;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// CycleIndications setter
// (setter disabled for Variable type)

  
// CycleDuration flow acquisition
// (input disabled for TimeUI_16 type)
// CycleDuration flow synthesis
// (output disabled for TimeUI_16 type)
// CycleDuration getter
UI_16 get_CycleDuration(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_CycleDuration==TRUE) {
return diag.CycleDuration;
} else {
#endif
return dre.CycleDuration;
#ifdef _DIAG_ACTIVE
}
#endif
};
// CycleDuration setter
// (setter disabled for TimeUI_16 type)

  
// VentilationFlags flow acquisition
// (input disabled for Variable type)
// VentilationFlags flow synthesis
// (output disabled for Variable type)
// VentilationFlags getter
t_ventilationflags get_VentilationFlags(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_VentilationFlags==TRUE) {
		return diag.VentilationFlags;
	} else {
#endif
		return dre.VentilationFlags;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// VentilationFlags setter
// (setter disabled for Variable type)

  
// VolumeSetting flow acquisition
// (input disabled for UI_16 type)
// VolumeSetting flow synthesis
// (output disabled for UI_16 type)
// VolumeSetting getter
UI_16 get_VolumeSetting(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_VolumeSetting==TRUE) {
return diag.VolumeSetting;
} else {
#endif
return dre.VolumeSetting;
#ifdef _DIAG_ACTIVE
}
#endif
};
// VolumeSetting setter
// (setter disabled for UI_16 type)

  
// RpmSetting flow acquisition
// (input disabled for UI_16 type)
// RpmSetting flow synthesis
// (output disabled for UI_16 type)
// RpmSetting getter
UI_16 get_RpmSetting(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RpmSetting==TRUE) {
return diag.RpmSetting;
} else {
#endif
return dre.RpmSetting;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RpmSetting setter
// (setter disabled for UI_16 type)

  
// RampDegreesSetting flow acquisition
// (input disabled for UI_8 type)
// RampDegreesSetting flow synthesis
// (output disabled for UI_8 type)
// RampDegreesSetting getter
UI_8 get_RampDegreesSetting(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RampDegreesSetting==TRUE) {
return diag.RampDegreesSetting;
} else {
#endif
return dre.RampDegreesSetting;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RampDegreesSetting setter
// (setter disabled for UI_8 type)

  
// EiRatioSetting flow acquisition
// (input disabled for UI_8 type)
// EiRatioSetting flow synthesis
// (output disabled for UI_8 type)
// EiRatioSetting getter
UI_8 get_EiRatioSetting(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_EiRatioSetting==TRUE) {
return diag.EiRatioSetting;
} else {
#endif
return dre.EiRatioSetting;
#ifdef _DIAG_ACTIVE
}
#endif
};
// EiRatioSetting setter
// (setter disabled for UI_8 type)

  
// RecruitTimer flow acquisition
// (input disabled for UI_8 type)
// RecruitTimer flow synthesis
// (output disabled for UI_8 type)
// RecruitTimer getter
UI_8 get_RecruitTimer(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_RecruitTimer==TRUE) {
return diag.RecruitTimer;
} else {
#endif
return dre.RecruitTimer;
#ifdef _DIAG_ACTIVE
}
#endif
};
// RecruitTimer setter
// (setter disabled for UI_8 type)

  
// IncomingFrame flow acquisition
// (input disabled for Variable type)
// IncomingFrame flow synthesis
// (output disabled for Variable type)
// IncomingFrame getter
t_incomingframe get_IncomingFrame(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_IncomingFrame==TRUE) {
		return diag.IncomingFrame;
	} else {
#endif
		return dre.IncomingFrame;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// IncomingFrame setter
// (setter disabled for Variable type)

  
// OutgoingFrame flow acquisition
// (input disabled for Variable type)
// OutgoingFrame flow synthesis
// (output disabled for Variable type)
// OutgoingFrame getter
t_outgoingframe get_OutgoingFrame(void){ 
#ifdef _DIAG_ACTIVE
	if (diag.enable_OutgoingFrame==TRUE) {
		return diag.OutgoingFrame;
	} else {
#endif
		return dre.OutgoingFrame;
#ifdef _DIAG_ACTIVE
	}
#endif
};
// OutgoingFrame setter
// (setter disabled for Variable type)

  
// Display flow acquisition
// (input disabled for Display type)
// Display flow synthesis
// (output disabled for Display type)
// Display getter
// (getter disabled for Display type)
// Display setter
// (setter disabled for Display type)

  
// DisplayPlus flow acquisition
// (input disabled for Display type)
// DisplayPlus flow synthesis
// (output disabled for Display type)
// DisplayPlus getter
// (getter disabled for Display type)
// DisplayPlus setter
// (setter disabled for Display type)

  
// DisplayButtons flow acquisition
// (input disabled for Keyboard type)
// DisplayButtons flow synthesis
// (output disabled for Keyboard type)
// DisplayButtons getter
// (getter disabled for Keyboard type)
// DisplayButtons setter
// (setter disabled for Keyboard type)

  
// FlowPublic flow acquisition
// (input disabled for UI_16 type)
// FlowPublic flow synthesis
// (output disabled for UI_16 type)
// FlowPublic getter
UI_16 get_FlowPublic(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_FlowPublic==TRUE) {
return diag.FlowPublic;
} else {
#endif
return dre.FlowPublic;
#ifdef _DIAG_ACTIVE
}
#endif
};
// FlowPublic setter
// (setter disabled for UI_16 type)

  
// Power_EN flow acquisition
// (input disabled for BOOL type)
// Power_EN flow synthesis
// (output disabled for BOOL type)
// Power_EN getter
BOOL  get_Power_EN(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Power_EN==TRUE) {
return diag.Power_EN;
} else {
#endif
return dre.Power_EN;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Power_EN setter
// (setter disabled for BOOL type)

  
// Power_Axis flow acquisition
// (input disabled for BOOL type)
// Power_Axis flow synthesis
// (output disabled for BOOL type)
// Power_Axis getter
BOOL  get_Power_Axis(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Power_Axis==TRUE) {
return diag.Power_Axis;
} else {
#endif
return dre.Power_Axis;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Power_Axis setter
// (setter disabled for BOOL type)

  
// Power_ENO flow acquisition
// (input disabled for BOOL type)
// Power_ENO flow synthesis
// (output disabled for BOOL type)
// Power_ENO getter
BOOL  get_Power_ENO(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Power_ENO==TRUE) {
return diag.Power_ENO;
} else {
#endif
return dre.Power_ENO;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Power_ENO setter
// (setter disabled for BOOL type)

  
// Reset_EN flow acquisition
// (input disabled for BOOL type)
// Reset_EN flow synthesis
// (output disabled for BOOL type)
// Reset_EN getter
BOOL  get_Reset_EN(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Reset_EN==TRUE) {
return diag.Reset_EN;
} else {
#endif
return dre.Reset_EN;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Reset_EN setter
// (setter disabled for BOOL type)

  
// Reset_Axis flow acquisition
// (input disabled for BOOL type)
// Reset_Axis flow synthesis
// (output disabled for BOOL type)
// Reset_Axis getter
BOOL  get_Reset_Axis(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Reset_Axis==TRUE) {
return diag.Reset_Axis;
} else {
#endif
return dre.Reset_Axis;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Reset_Axis setter
// (setter disabled for BOOL type)

  
// Reset_ENO flow acquisition
// (input disabled for BOOL type)
// Reset_ENO flow synthesis
// (output disabled for BOOL type)
// Reset_ENO getter
BOOL  get_Reset_ENO(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Reset_ENO==TRUE) {
return diag.Reset_ENO;
} else {
#endif
return dre.Reset_ENO;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Reset_ENO setter
// (setter disabled for BOOL type)

  
// Home_EN flow acquisition
// (input disabled for BOOL type)
// Home_EN flow synthesis
// (output disabled for BOOL type)
// Home_EN getter
BOOL  get_Home_EN(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Home_EN==TRUE) {
return diag.Home_EN;
} else {
#endif
return dre.Home_EN;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Home_EN setter
// (setter disabled for BOOL type)

  
// Home_Axis flow acquisition
// (input disabled for BOOL type)
// Home_Axis flow synthesis
// (output disabled for BOOL type)
// Home_Axis getter
BOOL  get_Home_Axis(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Home_Axis==TRUE) {
return diag.Home_Axis;
} else {
#endif
return dre.Home_Axis;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Home_Axis setter
// (setter disabled for BOOL type)

  
// Home_ENO flow acquisition
// (input disabled for BOOL type)
// Home_ENO flow synthesis
// (output disabled for BOOL type)
// Home_ENO getter
BOOL  get_Home_ENO(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Home_ENO==TRUE) {
return diag.Home_ENO;
} else {
#endif
return dre.Home_ENO;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Home_ENO setter
// (setter disabled for BOOL type)

  
// Halt_EN flow acquisition
// (input disabled for BOOL type)
// Halt_EN flow synthesis
// (output disabled for BOOL type)
// Halt_EN getter
BOOL  get_Halt_EN(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Halt_EN==TRUE) {
return diag.Halt_EN;
} else {
#endif
return dre.Halt_EN;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Halt_EN setter
// (setter disabled for BOOL type)

  
// Halt_Axis flow acquisition
// (input disabled for BOOL type)
// Halt_Axis flow synthesis
// (output disabled for BOOL type)
// Halt_Axis getter
BOOL  get_Halt_Axis(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Halt_Axis==TRUE) {
return diag.Halt_Axis;
} else {
#endif
return dre.Halt_Axis;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Halt_Axis setter
// (setter disabled for BOOL type)

  
// Halt_ENO flow acquisition
// (input disabled for BOOL type)
// Halt_ENO flow synthesis
// (output disabled for BOOL type)
// Halt_ENO getter
BOOL  get_Halt_ENO(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Halt_ENO==TRUE) {
return diag.Halt_ENO;
} else {
#endif
return dre.Halt_ENO;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Halt_ENO setter
// (setter disabled for BOOL type)

  
// Absolute_EN flow acquisition
// (input disabled for BOOL type)
// Absolute_EN flow synthesis
// (output disabled for BOOL type)
// Absolute_EN getter
BOOL  get_Absolute_EN(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Absolute_EN==TRUE) {
return diag.Absolute_EN;
} else {
#endif
return dre.Absolute_EN;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Absolute_EN setter
// (setter disabled for BOOL type)

  
// Absolute_Axis flow acquisition
// (input disabled for BOOL type)
// Absolute_Axis flow synthesis
// (output disabled for BOOL type)
// Absolute_Axis getter
BOOL  get_Absolute_Axis(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Absolute_Axis==TRUE) {
return diag.Absolute_Axis;
} else {
#endif
return dre.Absolute_Axis;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Absolute_Axis setter
// (setter disabled for BOOL type)

  
// Absolute_ENO flow acquisition
// (input disabled for BOOL type)
// Absolute_ENO flow synthesis
// (output disabled for BOOL type)
// Absolute_ENO getter
BOOL  get_Absolute_ENO(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Absolute_ENO==TRUE) {
return diag.Absolute_ENO;
} else {
#endif
return dre.Absolute_ENO;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Absolute_ENO setter
// (setter disabled for BOOL type)

  
// Relative_EN flow acquisition
// (input disabled for BOOL type)
// Relative_EN flow synthesis
// (output disabled for BOOL type)
// Relative_EN getter
BOOL  get_Relative_EN(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Relative_EN==TRUE) {
return diag.Relative_EN;
} else {
#endif
return dre.Relative_EN;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Relative_EN setter
// (setter disabled for BOOL type)

  
// Relative_Axis flow acquisition
// (input disabled for BOOL type)
// Relative_Axis flow synthesis
// (output disabled for BOOL type)
// Relative_Axis getter
BOOL  get_Relative_Axis(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Relative_Axis==TRUE) {
return diag.Relative_Axis;
} else {
#endif
return dre.Relative_Axis;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Relative_Axis setter
// (setter disabled for BOOL type)

  
// Relative_ENO flow acquisition
// (input disabled for BOOL type)
// Relative_ENO flow synthesis
// (output disabled for BOOL type)
// Relative_ENO getter
BOOL  get_Relative_ENO(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Relative_ENO==TRUE) {
return diag.Relative_ENO;
} else {
#endif
return dre.Relative_ENO;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Relative_ENO setter
// (setter disabled for BOOL type)

  
// Velocity_EN flow acquisition
// (input disabled for BOOL type)
// Velocity_EN flow synthesis
// (output disabled for BOOL type)
// Velocity_EN getter
BOOL  get_Velocity_EN(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Velocity_EN==TRUE) {
return diag.Velocity_EN;
} else {
#endif
return dre.Velocity_EN;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Velocity_EN setter
// (setter disabled for BOOL type)

  
// Velocity_Axis flow acquisition
// (input disabled for BOOL type)
// Velocity_Axis flow synthesis
// (output disabled for BOOL type)
// Velocity_Axis getter
BOOL  get_Velocity_Axis(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Velocity_Axis==TRUE) {
return diag.Velocity_Axis;
} else {
#endif
return dre.Velocity_Axis;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Velocity_Axis setter
// (setter disabled for BOOL type)

  
// Velocity_ENO flow acquisition
// (input disabled for BOOL type)
// Velocity_ENO flow synthesis
// (output disabled for BOOL type)
// Velocity_ENO getter
BOOL  get_Velocity_ENO(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Velocity_ENO==TRUE) {
return diag.Velocity_ENO;
} else {
#endif
return dre.Velocity_ENO;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Velocity_ENO setter
// (setter disabled for BOOL type)

  
// Jog_EN flow acquisition
// (input disabled for BOOL type)
// Jog_EN flow synthesis
// (output disabled for BOOL type)
// Jog_EN getter
BOOL  get_Jog_EN(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Jog_EN==TRUE) {
return diag.Jog_EN;
} else {
#endif
return dre.Jog_EN;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Jog_EN setter
// (setter disabled for BOOL type)

  
// Jog_Axis flow acquisition
// (input disabled for BOOL type)
// Jog_Axis flow synthesis
// (output disabled for BOOL type)
// Jog_Axis getter
BOOL  get_Jog_Axis(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Jog_Axis==TRUE) {
return diag.Jog_Axis;
} else {
#endif
return dre.Jog_Axis;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Jog_Axis setter
// (setter disabled for BOOL type)

  
// Jog_ENO flow acquisition
// (input disabled for BOOL type)
// Jog_ENO flow synthesis
// (output disabled for BOOL type)
// Jog_ENO getter
BOOL  get_Jog_ENO(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_Jog_ENO==TRUE) {
return diag.Jog_ENO;
} else {
#endif
return dre.Jog_ENO;
#ifdef _DIAG_ACTIVE
}
#endif
};
// Jog_ENO setter
// (setter disabled for BOOL type)

  
// EN flow acquisition
// (input disabled for BOOL type)
// EN flow synthesis
// (output disabled for BOOL type)
// EN getter
BOOL  get_EN(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_EN==TRUE) {
return diag.EN;
} else {
#endif
return dre.EN;
#ifdef _DIAG_ACTIVE
}
#endif
};
// EN setter
// (setter disabled for BOOL type)

  
// EN0 flow acquisition
// (input disabled for BOOL type)
// EN0 flow synthesis
// (output disabled for BOOL type)
// EN0 getter
BOOL  get_EN0(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_EN0==TRUE) {
return diag.EN0;
} else {
#endif
return dre.EN0;
#ifdef _DIAG_ACTIVE
}
#endif
};
// EN0 setter
// (setter disabled for BOOL type)

  
// CycleStartTime flow acquisition

// CycleStartTime flow synthesis

// CycleStartTime getter
UI_32 get_CycleStartTime(void){
#ifdef _DIAG_ACTIVE
if (diag.enable_CycleStartTime==TRUE) {
return diag.CycleStartTime;
} else {
#endif
return dre.CycleStartTime;
#ifdef _DIAG_ACTIVE
}
#endif
};
// CycleStartTime setter
// (setter disabled for TimeUI_32 type)