#ifndef _CORE_OUTPUT_H
#define _CORE_OUTPUT_H

#include "DRE.h"

void core_output_init(){
    setup_statusLED_output();
    setup_PeepValve();
    setup_ResetRelay_output();
    setup_TurnAxisDir_output();
    setup_TurnAxisEnable_output();
    setup_TurnAxisPulse_output();
    setup_BeaconLightGreen_output();
    setup_BeaconLightOrange_output();
    setup_BeaconLightRed_output();
    setup_Buzzer_output();
#ifdef CFG_USE_LCD
    setup_LCD_DB0_output();
    setup_LCD_DB1_output();
    setup_LCD_DB2_output();
    setup_LCD_DB3_output();
    setup_LCD_DB4_output();
    setup_LCD_DB5_output();
    setup_LCD_DB6_output();
    setup_LCD_DB7_output();
#endif
}

void core_output(){
    // Executes the outputs
    synthesize_statusLED();
    synthesize_PeepValve();
    synthesize_ResetRelay();
    synthesize_TurnAxisDir();
    synthesize_TurnAxisEnable();
    synthesize_TurnAxisPulse();
    synthesize_BeaconLightGreen();
    synthesize_BeaconLightOrange();
    synthesize_BeaconLightRed();
    synthesize_Buzzer();
#ifdef CFG_USE_LCD
    synthesize_LCD_DB0();
    synthesize_LCD_DB1();
    synthesize_LCD_DB2();
    synthesize_LCD_DB3();
    synthesize_LCD_DB5();
    synthesize_LCD_DB6();
    synthesize_LCD_DB7();
#endif
}

#endif