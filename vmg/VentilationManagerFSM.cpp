/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "VentilationManagerFSM.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['Cycle' begin (DON'T REMOVE THIS LINE!)] */
/** ***************************************************
This state machine simply models the insufflation-exsufflation loop.
If a stop request is commanded, the current cycle is finished, before finishing (going to final state).
*************************************************** */
/* set initial state */
STATE_T Cyclestate = ID_CYCLE_INITIAL;
STATE_T Cycle(  )
{

    switch( Cyclestate )
    {
        /* State ID: ID_CYCLE_INITIAL */
        case ID_CYCLE_INITIAL:
        {
            if( edre.vmgStop == true )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doCycleStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Cycle.
                // ..
                /* ['<global>::doCycleStop' end] */
                Cyclestate = ID_CYCLE_STOPPED;
            }
            else
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doStep1' begin] */
                edre.vmgStep1Done = false;
                // Acciones que lanzan Step1
                // ...
                /* ['<global>::doStep1' end] */
                Cyclestate = ID_CYCLE_STEP1;
            }
            return ID_CYCLE_INITIAL;
        }
        /* State ID: ID_CYCLE_STOPPED */
        case ID_CYCLE_STOPPED:
        {
            Cyclestate = ID_CYCLE_INITIAL;
            return ID_CYCLE_STOPPED;
        }
        /* State ID: ID_CYCLE_STEP1 */
        case ID_CYCLE_STEP1:
        {
            if( edre.vmgStop == true )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doCycleStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Cycle.
                // ..
                /* ['<global>::doCycleStop' end] */
                Cyclestate = ID_CYCLE_STOPPED;
            }
            else if( edre.vmgStep1Done )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doStep2' begin] */
                edre.vmgStep2Done = false;
                // Acciones que lanzan Step2
                // ...
                /* ['<global>::doStep2' end] */
                Cyclestate = ID_CYCLE_STEP2;
            }
            return ID_CYCLE_STEP1;
        }
        /* State ID: ID_CYCLE_STEP2 */
        case ID_CYCLE_STEP2:
        {
            if( edre.vmgStop == true )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doCycleStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Cycle.
                // ..
                /* ['<global>::doCycleStop' end] */
                Cyclestate = ID_CYCLE_STOPPED;
            }
            else if( (edre.vmgEnable == false) && (edre.vmgStep2Done) )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doCycleDisable' begin] */
                // Acciones de deshabilitaci_n a nivel de subm_quina Cycle.
                // ..
                /* ['<global>::doCycleDisable' end] */
                Cyclestate = ID_CYCLE_DISABLED;
            }
            else if( edre.vmgStep2Done )
            {
                /* Transition ID: ID_CYCLE_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doStep1' begin] */
                edre.vmgStep1Done = false;
                // Acciones que lanzan Step1
                // ...
                /* ['<global>::doStep1' end] */
                Cyclestate = ID_CYCLE_STEP1;
            }
            return ID_CYCLE_STEP2;
        }
        /* State ID: ID_CYCLE_DISABLED */
        case ID_CYCLE_DISABLED:
        {
            Cyclestate = ID_CYCLE_INITIAL;
            return ID_CYCLE_DISABLED;
        }
    }
}
/* ['Cycle' end (DON'T REMOVE THIS LINE!)] */

/* ['Run' begin (DON'T REMOVE THIS LINE!)] */
/** ***************************************************
VentilationSession models the normal working cycle of the ventilator.  At the beginning of each session (when the machine is connected to a new patient, or when the machine has been stopped for any reason) the machine shall execute a homing operation for reference, and just after the homing, it shall begin the ventilation cycle loop.  The ventilation cycle loop is modeled in a sub-machine called VentilationCycle.  If an stop is requested, this machine will wait for VentilationCycle to end, and then will return the control to the parent machine (MechVentilation).
*************************************************** */
/* set initial state */
STATE_T Runstate = ID_RUN_INITIAL;
STATE_T Run(  )
{

    switch( Runstate )
    {
        /* State ID: ID_RUN_INITIAL */
        case ID_RUN_INITIAL:
        {
            /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
            /* Actions: */
            /* ['<global>::doSetup' begin] */
            edre.vmgSetupDone = false;
            // Acciones de preparaci_n de VentilationManager
            // que deban ejecutarse antes de comandar
            // el ciclo de trabajo
            // ...
            /* ['<global>::doSetup' end] */
            Runstate = ID_RUN_SETUP;
            return ID_RUN_INITIAL;
        }
        /* State ID: ID_RUN_SETUP */
        case ID_RUN_SETUP:
        {
            if( edre.vmgStop == true )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doRunStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Run.
                // ..
                /* ['<global>::doRunStop' end] */
                Runstate = ID_RUN_STOPPED;
            }
            else if( (edre.vmgEnable == false) && (edre.vmgSetupDone)  )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doRunDisable' begin] */
                // Acciones de deshabilitaci_n a nivel de subm_quina Run.
                // ..
                /* ['<global>::doRunDisable' end] */
                Runstate = ID_RUN_DISABLED;
            }
            else if( edre.vmgSetupDone )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::launchCycle' begin] */
                // Acciones de lanzamiento del ciclo de trabajo
                // ...
                /* ['<global>::launchCycle' end] */
                Runstate = ID_RUN_CYCLE;
            }
            return ID_RUN_SETUP;
        }
        /* State ID: ID_RUN_CYCLE */
        case ID_RUN_CYCLE:
        {
            /* call substate function */
            STATE_T cycle_retval = Cycle(  );
            if( cycle_retval < 0 )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                Runstate = ID_RUN_STOPPRESENT;
            }
            return ID_RUN_CYCLE;
        }
        /* State ID: ID_RUN_STOPPRESENT */
        case ID_RUN_STOPPRESENT:
        {
            if( edre.vmgStop == true )
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doRunStop' begin] */
                // Acciones de parada abrupta a nivel de subm_quina Run.
                // ..
                /* ['<global>::doRunStop' end] */
                Runstate = ID_RUN_STOPPED;
            }
            else
            {
                /* Transition ID: ID_RUN_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doRunDisable' begin] */
                // Acciones de deshabilitaci_n a nivel de subm_quina Run.
                // ..
                /* ['<global>::doRunDisable' end] */
                Runstate = ID_RUN_DISABLED;
            }
            return ID_RUN_STOPPRESENT;
        }
        /* State ID: ID_RUN_DISABLED */
        case ID_RUN_DISABLED:
        {
            Runstate = ID_RUN_INITIAL;
            return ID_RUN_DISABLED;
        }
        /* State ID: ID_RUN_STOPPED */
        case ID_RUN_STOPPED:
        {
            Runstate = ID_RUN_INITIAL;
            return ID_RUN_STOPPED;
        }
    }
}
/* ['Run' end (DON'T REMOVE THIS LINE!)] */

/* ['VentilationManager' begin (DON'T REMOVE THIS LINE!)] */
/** ***************************************************
FSM principal del m_dulo VentilationManager.
Contiene el manejo del los estos Ready, Stop y Run.
*************************************************** */
/* set initial state */
STATE_T VentilationManagerstate = ID_VENTILATIONMANAGER_INITIAL;
STATE_T VentilationManager(  )
{

    switch( VentilationManagerstate )
    {
        /* State ID: ID_VENTILATIONMANAGER_INITIAL */
        case ID_VENTILATIONMANAGER_INITIAL:
        {
            /* Transition ID: ID_VENTILATIONMANAGER_TRANSITION_CONNECTION */
            VentilationManagerstate = ID_VENTILATIONMANAGER_VMGSTOPPRESENT;
            return ID_VENTILATIONMANAGER_INITIAL;
        }
        /* State ID: ID_VENTILATIONMANAGER_VMGSTOPPRESENT */
        case ID_VENTILATIONMANAGER_VMGSTOPPRESENT:
        {
            if( edre.vmgStop == true )
            {
                /* Transition ID: ID_VENTILATIONMANAGER_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doStop' begin] */
                // Acciones de Parada abrupta de VentilationManager
                // ...
                /* ['<global>::doStop' end] */
                VentilationManagerstate = ID_VENTILATIONMANAGER_STOP;
            }
            else
            {
                /* Transition ID: ID_VENTILATIONMANAGER_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doDisable' begin] */
                // Operaciones de deshabilitaci_n de VentilationManager
                // ...
                /* ['<global>::doDisable' end] */
                VentilationManagerstate = ID_VENTILATIONMANAGER_READY;
            }
            return ID_VENTILATIONMANAGER_VMGSTOPPRESENT;
        }
        /* State ID: ID_VENTILATIONMANAGER_STOP */
        case ID_VENTILATIONMANAGER_STOP:
        {
            if( (!edre.vmgStop && edre.vmgRehab) )
            {
                /* Transition ID: ID_VENTILATIONMANAGER_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doRehab' begin] */
                // Acciones de rearme de VentilationManager
                // ...
                /* ['<global>::doRehab' end] */
                /* ['<global>::doDisable' begin] */
                // Operaciones de deshabilitaci_n de VentilationManager
                // ...
                /* ['<global>::doDisable' end] */
                VentilationManagerstate = ID_VENTILATIONMANAGER_READY;
            }
            return ID_VENTILATIONMANAGER_STOP;
        }
        /* State ID: ID_VENTILATIONMANAGER_READY */
        case ID_VENTILATIONMANAGER_READY:
        {
            if( edre.vmgStop == true )
            {
                /* Transition ID: ID_VENTILATIONMANAGER_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::doStop' begin] */
                // Acciones de Parada abrupta de VentilationManager
                // ...
                /* ['<global>::doStop' end] */
                VentilationManagerstate = ID_VENTILATIONMANAGER_STOP;
            }
            else if( false )
            {
                /* Transition ID: ID_VENTILATIONMANAGER_TRANSITION_CONNECTION */
                VentilationManagerstate = ID_VENTILATIONMANAGER_UNREACHABLEFINAL;
            }
            else if( edre.vmgEnable )
            {
                /* Transition ID: ID_VENTILATIONMANAGER_TRANSITION_CONNECTION */
                /* Actions: */
                /* ['<global>::launchRun' begin] */
                // Acciones de lanzamiento de la subm_quina Run
                // ...
                /* ['<global>::launchRun' end] */
                VentilationManagerstate = ID_VENTILATIONMANAGER_RUN;
            }
            return ID_VENTILATIONMANAGER_READY;
        }
        /* State ID: ID_VENTILATIONMANAGER_UNREACHABLEFINAL */
        case ID_VENTILATIONMANAGER_UNREACHABLEFINAL:
        {
            VentilationManagerstate = ID_VENTILATIONMANAGER_INITIAL;
            return ID_VENTILATIONMANAGER_UNREACHABLEFINAL;
        }
        /* State ID: ID_VENTILATIONMANAGER_RUN */
        case ID_VENTILATIONMANAGER_RUN:
        {
            /* call substate function */
            STATE_T run_retval = Run(  );
            if( (run_retval < 0) )
            {
                /* Transition ID: ID_VENTILATIONMANAGER_TRANSITION_CONNECTION */
                VentilationManagerstate = ID_VENTILATIONMANAGER_VMGSTOPPRESENT;
            }
            return ID_VENTILATIONMANAGER_RUN;
        }
    }
}
/* ['VentilationManager' end (DON'T REMOVE THIS LINE!)] */
