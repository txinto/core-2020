/* ['Common headers' begin (DON'T REMOVE THIS LINE!)] */
#include "VentilationManagerFSM_CI.h"
/* ['Common headers' end (DON'T REMOVE THIS LINE!)] */

/* ['Cycle' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_CYCLE_INITIAL 0
#define ID_CYCLE_STEP1 1
#define ID_CYCLE_STEP2 2
#define ID_CYCLE_DISABLED -3
#define ID_CYCLE_STOPPED -4

STATE_T Cycle(  );
/* ['Cycle' end (DON'T REMOVE THIS LINE!)] */

/* ['Run' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_RUN_INITIAL 5
#define ID_RUN_SETUP 6
#define ID_RUN_CYCLE 7
#define ID_RUN_STOPPRESENT 8
#define ID_RUN_DISABLED -9
#define ID_RUN_STOPPED -10

STATE_T Run(  );
/* ['Run' end (DON'T REMOVE THIS LINE!)] */

/* ['VentilationManager' begin (DON'T REMOVE THIS LINE!)] */
/* State IDs */
#define ID_VENTILATIONMANAGER_INITIAL 11
#define ID_VENTILATIONMANAGER_READY 12
#define ID_VENTILATIONMANAGER_STOP 13
#define ID_VENTILATIONMANAGER_RUN 14
#define ID_VENTILATIONMANAGER_VMGSTOPPRESENT 15
#define ID_VENTILATIONMANAGER_UNREACHABLEFINAL -16

STATE_T VentilationManager(  );
/* ['VentilationManager' end (DON'T REMOVE THIS LINE!)] */
