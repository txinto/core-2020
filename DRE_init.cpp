#ifndef _DRE_INIT_H
#define _DRE_INIT_H

#include "DRE.h"
#include "extra_DRE.h"

extern t_dre dre;
extern t_diag diag;
t_extra_DRE edre;


void DRE_init(){
    dre.statusLED = false;
    dre.dbgLedBlinkTimer = 0;
    dre.DBG_LED = false;
    dre.ResetRelay = false;
    dre.EmergencyStop = false;
    dre.TurnAxisHwHi = false;
    dre.BeaconLightRed = false;
    dre.BeaconLightOrange = false;
    dre.TurnAxisPulse = false;
    dre.TurnAxisDir = false;
    dre.TurnAxisEnable = false;
    dre.BeaconLightGreen = false;
    dre.Buzzer = false;
    dre.PeepValve = false;
    dre.TurnAxisHwLow = false;
    dre.ServoPend = false;
    dre.ServoAlarm = false;
    dre.AtmosfericPressure = 0;
    dre.DiferentialPressure = 0;
#ifdef CFG_USE_LCD
    dre.LCD_DB7 = false;
    dre.LCD_DB6 = false;
    dre.LCD_DB5 = false;
    dre.LCD_DB4 = false;
    dre.LCD_DB3 = false;
    dre.LCD_DB2 = false;
    dre.LCD_DB1 = false;
    dre.LCD_DB0 = false;
#endif

    /********** External parameters *********/
    edre.PeakSetpoint = 5000;     // TBD
    edre.PeepSetpoint = 2000;     // TBD
    edre.RecruitmentSetpoint = 2000;          // TBD
    edre.RecruitmentTimer = (uint32_t)(40 * (1000/CTE_CYCLE_TIME_IN_MS)); // TBD

    /*********** MV: MechVentilation ***********/
    /* MV Parameters */
    edre.mvExsufflationMode = VM_MODE_EXSUFFLATION;
    edre.mvInsufflationMode = VM_MODE_INSUFFLATION;
    edre.mvRecruitMode = VM_MODE_RECRUIT;
    /* MV Commands */
    edre.mvStop = false;
    edre.mvEnable = false;
    edre.mvRehab = false;
    edre.mvRecruit = false;
    /* MV Responses */

    /*********** VM: VentilationManager ***********/
    /* MC Parameters */
    edre.vmMode = VM_MODE_IDLE;
    /* MC Commands */
    edre.vmStop = false;
    edre.vmEnable = false;
    edre.vmRehab = false;
     /* MV Responses */   
    edre.vmActionFinished = false;


    /*********** MC: MotorControl ***********/
    /* MC Parameters */
    edre.mcSetpoint = 0;
    edre.mcMode = MC_MODE_IDLE;
    /* MC Commands */
    edre.mcStop = false;
    edre.mcEnable = false;
    edre.mcRehab = false;
     /* MV Responses */   
    edre.mcSetPointAchieved = false;

    /*********** MD: MotorDriving ***********/
    /* MD Parameters */
    edre.mdSetpoint = 0;
    edre.mdMode = MD_MODE_IDLE;
    /* MD Commands */
    edre.mdStop = false;
    edre.mdEnable = false;
    edre.mdRehab = false;
    edre.mdHoming = false;
     /* MD Responses */   
    edre.mdSetPointAchieved = false;
    edre.mdHomingAchieved = false;

#include "vmg/VentilationManagerDRE_init_c.h"
}

void Diag_init(){
    diag.enable_statusLED = false;
    diag.enable_dbgLedBlinkTimer = false;
    diag.enable_DBG_LED = false;
    diag.enable_ResetRelay = false;
    diag.enable_EmergencyStop = false;
    diag.enable_TurnAxisHwHi = false;
    diag.enable_BeaconLightRed = false;
    diag.enable_BeaconLightOrange = false;
    diag.enable_TurnAxisPulse = false;
    diag.enable_TurnAxisDir = false;
    diag.enable_TurnAxisEnable = false;
    diag.enable_BeaconLightGreen = false;
    diag.enable_Buzzer = false;
    diag.enable_PeepValve = false;
    diag.enable_TurnAxisHwLow = false;
    diag.enable_ServoPend = false;
    diag.enable_ServoAlarm = false;
    diag.enable_AtmosfericPressure = false;
    diag.enable_DiferentialPressure = false;
#ifdef CFG_USE_LCD
    diag.enable_LCD_DB7 = false;
    diag.enable_LCD_DB6 = false;
    diag.enable_LCD_DB5 = false;
    diag.enable_LCD_DB4 = false;
    diag.enable_LCD_DB3 = false;
    diag.enable_LCD_DB2 = false;
    diag.enable_LCD_DB1 = false;
    diag.enable_LCD_DB0 = false;
#endif
}

#endif